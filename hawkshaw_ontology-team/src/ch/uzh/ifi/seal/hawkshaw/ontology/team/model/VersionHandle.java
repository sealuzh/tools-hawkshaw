package ch.uzh.ifi.seal.hawkshaw.ontology.team.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.team
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Calendar;

import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractModelEntityHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.owl.vocabulary.IHawkshawHistory;

import com.hp.hpl.jena.ontology.Individual;

public class VersionHandle extends AbstractModelEntityHandle<HistoryOntModel> {

	public VersionHandle(Individual theEntity, HistoryOntModel model) {
		super(theEntity, model);
	}
	
	public void addCommitter(String name) {
		CommitterHandle developerHandle = model().getCommitterHandleFor(name);
		addRelation(IHawkshawHistory.IS_COMMITTED_BY, developerHandle);
	}

	public void setTimestamp(long timestamp) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);
		
		addLiteral(IHawkshawHistory.COMMITTED_ON, c);
	}

	public void setComment(String comment) {
		addLiteral(IHawkshawHistory.HAS_COMMIT_MESSAGE, comment);
	}

	public void setFilePath(String filePath) {
		addLiteral(IHawkshawHistory.FILE_PATH, filePath);
	}
}
