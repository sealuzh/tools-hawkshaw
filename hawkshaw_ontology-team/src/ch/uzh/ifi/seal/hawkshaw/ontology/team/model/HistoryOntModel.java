package ch.uzh.ifi.seal.hawkshaw.ontology.team.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.team
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractHawkshawModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.owl.vocabulary.IHawkshawHistory;
import ch.uzh.ifi.seal.hawkshaw.support.misc.WordUtil;

import com.hp.hpl.jena.ontology.Individual;

public class HistoryOntModel extends AbstractHawkshawModel{

	public HistoryOntModel(PersistentJenaModel baseModel) {
		super(baseModel);
		init();
	}

	@Override
	public void clear() {
		getBaseModel().clear();
		init();
	}
	
	public FileHandle getFileHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName,  IHawkshawHistory.FILE_UNDER_VERSION_CONTROL);
		return new FileHandle(individual, this);
	}
	
	public VersionHandle getVersionHandleFor(String uniqueName, String identifier) {
		Individual individual = fetchIndividual(uniqueName, identifier, IHawkshawHistory.VERSION);
		return new VersionHandle(individual, this);
	}
	
	public CommitterHandle getCommitterHandleFor(String name) {
		Individual individual = fetchIndividual(UriUtil.encode(WordUtil.convertNonAscii(name.toLowerCase())), name, IHawkshawHistory.COMMITTER);
		return new CommitterHandle(individual, this);
	}

	private void init() {
		getBaseModel().readOntologies("http://se-on.org/ontologies/hawkshaw/2012/02/history.owl",
								      "http://se-on.org/ontologies/hawkshaw/2012/02/history-nl.owl");
	}
}
