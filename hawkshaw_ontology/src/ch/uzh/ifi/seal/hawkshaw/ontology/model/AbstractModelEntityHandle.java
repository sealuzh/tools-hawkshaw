package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Calendar;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Abstract base class for handles of model entities.
 * 
 * Wraps an individual from the ontology and maintains a reference to the
 * {@link AbstractHawkshawModel} it is associated with.
 * 
 * @author wuersch
 * 
 */
public abstract class AbstractModelEntityHandle<T extends AbstractHawkshawModel> {
	/**
	 * The entity this handle refers to.
	 */
	private Individual theEntity;
	/**
	 * The associated model.
	 */
	private T model;
	
	/**
	 * Constructor.
	 * 
	 * @param theEntity
	 *            the entity this handle refers to. Clients need to
	 *            ensure that the individual has the correct ontology class - no
	 *            additional checks are performed to keep the number of model
	 *            accesses low.
	 * @param model
	 *            the associated model.
	 */
	public AbstractModelEntityHandle(Individual theEntity, T model) {
		this.theEntity = theEntity;
		this.model	   = model;
	}
	
	/**
	 * Convenience method to add a literal to the entity.
	 * 
	 * @param propertyUri the datatype property.
	 * @param value the literal value
	 */
	public void addLiteral(String propertyUri, int value) {
		model.addLiteralTo(this, propertyUri, value);
	}
	
	/**
	 * Convenience method to add a literal to the entity.
	 * 
	 * @param propertyUri the datatype property.
	 * @param value the literal value
	 */
	public void addLiteral(String propertyUri, boolean value) {
		model.addLiteralTo(this, propertyUri, value);
	}
	
	/**
	 * Convenience method to add a literal to the entity.
	 * 
	 * @param propertyUri the datatype property.
	 * @param value the literal value.
	 */
	public void addLiteral(String propertyUri, String value) {
		model.addLiteralTo(this, propertyUri, value);
	}
	
	public void addLiteral(String propertyUri, Calendar date) {
		model.addLiteralTo(this, propertyUri, date);
	}
	
	/**
	 * Convenience method to add a relation to the entity.
	 * 
	 * @param propertyUri the object property.
	 * @param otherHandle the object.
	 */
	public void addRelation(String propertyUri, AbstractModelEntityHandle<T> otherHandle) {
		model.addPropertyBetween(this, propertyUri, otherHandle);
	}
	
	public void addRelation(String propertyUri, String individualUri) {
		model.addPropertyBetween(this, propertyUri, individualUri);
	}
	
	/**
	 * Converts the handle to a Jena individual and returns it.
	 * 
	 * @return the individual this handle refers to.
	 */
	public Individual asIndividual() {
		return theEntity;
	}
		
	/**
	 * Returns the associated model.
	 * 
	 * @return the associated model.
	 */
	public T model() {
		return model;
	}
	
	public String getUri() {
		return theEntity.getURI();
	}
}
