package ch.uzh.ifi.seal.hawkshaw.ontology.exceptions;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Unchecked exception that may be raised whenever an encountered Jena resources
 * does not correspond to the expected one.
 * 
 * @author wuersch
 * 
 */
public class InvalidResourceException extends HawkshawException {
	private static final long serialVersionUID = 1234549415136886637L;
	
	private Resource expectedResource;
	private Resource actualResource;
	
	/**
	 * Constructor. 
	 * 
	 * @param expectedResource the expected resource.
	 * @param actualResource the encountered resource.
	 */
	public InvalidResourceException(Resource expectedResource, Resource actualResource) {
		super(OntologyCore.getDefault(), "Expected " + expectedResource.getLocalName() + " but was " + actualResource.getLocalName());
		this.expectedResource = expectedResource;
		this.actualResource   = actualResource;
	}
	
	/**
	 * Returns the resource that was expected.
	 * 
	 * @return the expected Jena resource.
	 */
	public Resource getExpectedResource() {
		return expectedResource;
	}

	/**
	 * Returns the resource that was encountered.
	 * 
	 * @return
	 */
	public Resource getActualResource() {
		return actualResource;
	}
}
