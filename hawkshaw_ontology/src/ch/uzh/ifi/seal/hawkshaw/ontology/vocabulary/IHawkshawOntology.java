package ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


public interface IHawkshawOntology {
	String BASE        = "http://se-on.org/ontologies/hawkshaw/2012/02/generic.owl";
	String LOCAL       = "local_hawkshaw_generic_12_02";
	
	// Classes
	String FILE         = BASE + "#File";
	String STAKEHOLDER  = BASE + "#Stakeholder";
	String DEVELOPER    = BASE + "#Developer";
	
	// Object properties:
	String HAS_PARENT   = BASE + "#hasParent";
	String HAS_CHILD    = BASE + "#hasChild";
	String HAS_SIBLING  = BASE + "#hasSibling";
	
	// Datatype properties:
	String HAS_URL      = BASE + "#hasURL";
}
