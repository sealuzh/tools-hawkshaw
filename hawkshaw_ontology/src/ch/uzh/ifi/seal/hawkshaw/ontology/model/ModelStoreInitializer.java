package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;

public class ModelStoreInitializer extends Job {
	private IPath locationOfStore;
	
	private ModelStore modelStore;

	public ModelStoreInitializer(IPath locationOfStore) {
		super("Initializing Model Store");
		setSystem(true);
		
		this.locationOfStore = locationOfStore;
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor);
		progress.beginTask("Initializing Model Store.", 10000);
		
		Thread init = new Initializer();
		init.start();
		
		while(init.isAlive()) {
			progress.setWorkRemaining(10000);
			progress.worked(1);
		}
		
		return Status.OK_STATUS;
	}
	
	@Override
	public boolean belongsTo(Object family) {
		return family.equals(OntologyCore.FAMILY_INIT);
	}

	public ModelStore getModelStore() {
		return modelStore;
	}
	
	class Initializer extends Thread {
		@Override
		public void run() {
			modelStore = new ModelStore(locationOfStore);
			modelStore.init();
		}
	}
}

