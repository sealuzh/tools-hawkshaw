package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mindswap.pellet.jena.ModelExtractor;
import org.mindswap.pellet.jena.ModelExtractor.StatementType;
import org.mindswap.pellet.jena.PelletReasonerFactory;

import ch.uzh.ifi.seal.hawkshaw.ontology.exceptions.InvalidResourceException;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;

import com.hp.hpl.jena.datatypes.xsd.XSDDateTime;
import com.hp.hpl.jena.ontology.ConversionException;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.ontology.UnionClass;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.XSD;

public class PersistentJenaModel {	
	private HawkshawModelFactory factory;

	private OntModel persistentModel;
	private OntModel infModel;
	
	private OntDocumentManager docManager;
	
	private String base;
	
	private boolean isDirty;
	
	protected PersistentJenaModel(HawkshawModelFactory factory, OntModel persistentModel, String base) {
		this.factory = factory;
		this.persistentModel = persistentModel;
		this.base = base;
		
		docManager = persistentModel.getDocumentManager();
		
		markAsClean();
		
		createInfModel();
	}
	
	public void readOntologies(String... uris) {
		for(String uri : uris) {
			obtainWriteLock();
			try {
				persistentModel.read(uri);
			} finally {
				releaseLock();
			}
		}
		
		markAsDirty();
	}
	
	public void reason() {
		if(isClean()) { return; }
		
		obtainWriteLock();
		try {			
			addInfsToPersistentModel();
		} finally {
			releaseLock();
		}
		
		markAsClean();
	}
	
	public String getBase() {
		return base;
	}
	
	public void clear() {
		obtainWriteLock();
		try {
			persistentModel.removeAll();
		} finally {
			releaseLock();
		}
		
		factory.addAutoLoads(this); // called method is already locked
	}
	
	/**
	 * Returns the estimated size of the underlying model.
	 * 
	 * @return the number of statements in the model.
	 */
	public long size() {
		obtainReadLock();
		try {
			return persistentModel.size();
		} finally {
			releaseLock();
		}
	}
	
	public Set<String> getLiterals(String subjectUri, String predicateUri) {
		Set<String> result = new HashSet<>();
		
		obtainReadLock();
		try {
			Resource subjectResource = persistentModel.getResource(subjectUri);
			Property predicateProperty = persistentModel.getProperty(predicateUri);
			
			StmtIterator iter = persistentModel.listStatements(new SimpleSelector(subjectResource, predicateProperty, (RDFNode) null));
			
			while(iter.hasNext()) {
				Statement stmt = iter.next();
				result.add(stmt.getString());
			}
			
			iter.close();
		} finally {
			releaseLock();
		}
		
		return result;
	}
	
	public Map<String, Set<Object>> getLiterals(String subjectUri) {
		Map<String, Set<Object>> result = new HashMap<>();
		
		obtainReadLock();
		try {
			Individual subjectResource = persistentModel.getIndividual(subjectUri);
			if(subjectResource != null) {
				StmtIterator stmts = subjectResource.listProperties();
				while(stmts.hasNext()) {
					Statement stmt = stmts.next();
					Property prop = stmt.getPredicate();
					RDFNode object = stmt.getObject();
					if(object.isLiteral()) {
						Literal literal = object.asLiteral();
						
						String propUri = prop.getURI();
						Set<Object> values = result.get(propUri);
						
						if(values == null) {
							values = new HashSet<>();
							result.put(propUri, values);
						}
						
						values.add(rawType(literal.getValue()));
					}
				}
				stmts.close();
			}
		} finally {
			releaseLock();
		}
		
		return result;
	}
	
	public String getObject(String individualUri, String propUri) {
		obtainReadLock();
		try { 
			Individual individual = persistentModel.getIndividual(individualUri);
			Property prop = persistentModel.getProperty(propUri);
			
			if(individual != null && prop != null) {
				Statement stmt = individual.getProperty(prop);
				if(stmt != null) {
					RDFNode node = stmt.getObject();
					if(node.canAs(Individual.class)) {
						return node.as(Individual.class).getURI();
					}
				}
			}
		} finally {
			releaseLock();
		}
		
		return null;
	}
	
	/**
	 * Returns all the named classes (i.e. all classes, except blank nodes) in
	 * the underlying ontology model that match the range information.
	 * 
	 * @param range
	 *            a set of range class URIs.
	 * @return a set with the uri's of all matching named classes in the
	 *         underlying ontology model.
	 */
	public Set<String> getNamedClassesThatMatch(final Set<String> range) {
		Set<OntClass> result = new HashSet<>();
		
		obtainReadLock();
		try {
			ExtendedIterator<OntClass> iter;
			
			if(range.isEmpty()) {
				iter = persistentModel.listNamedClasses();
			} else {
				Filter<OntClass> filter = new RangeFilter(range);
				
				iter = persistentModel.listNamedClasses()
									  .filterKeep(filter);
			}
			
			while(iter.hasNext()) {
				result.add(iter.next());
			}
			
			iter.close();

			return uriStringsOf(result);
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Returns all the individuals (instances) in the underlying ontology model
	 * that match the range information. .
	 * 
	 * @param range
	 *            a set of range class URIs.
	 * @return a set with all uri's of the matching individuals in the
	 *         underlying ontology model.
	 */
	public Set<String> getIndividualsThatMatch(Set<String> range) {		
		Set<Individual> result = new HashSet<>();
		
		obtainReadLock();
		try {
			if(range.isEmpty()) {
				ExtendedIterator<Individual> iter = persistentModel.listIndividuals();
				
				while(iter.hasNext()) {
					result.add(iter.next());
				}
				
				iter.close();
			} else {
				for(String uri : range) {
					OntClass ontClass = persistentModel.getOntClass(uri);
					ExtendedIterator<Individual> iter = persistentModel.listIndividuals(ontClass);
					
					while(iter.hasNext()) {
						result.add(iter.next());
					}
					
					iter.close();
				}
			}
			
			return uriStringsOf(result);
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Returns all the object properties in the underlying ontology model that
	 * match the domain information.
	 * 
	 * @param domain
	 *            a set of domain class URIs.
	 * @return A set of uri's of all matching object properties in the
	 *         underlying ontology model.
	 */
	public Set<String> getObjectPropertiesThatMatch(final Set<String> domain) {
		Set<ObjectProperty> result = new HashSet<>();

		obtainReadLock();
		try {
			ExtendedIterator<ObjectProperty> iter = persistentModel.listObjectProperties()
																   .filterKeep(new DomainFilter<ObjectProperty>(domain));
			
			while(iter.hasNext()) {
				result.add(iter.next());
			}
			
			iter.close();
			
			return uriStringsOf(result);
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Returns all the non-boolean data type properties in the underlying
	 * ontology model that match the domain information.
	 * 
	 * @param domain
	 *            a set of domain class URIs.
	 * @return a set with the uri's of all matching data type properties in the
	 *         underlying ontology model.
	 */
	public Set<String> listDatatypePropertiesThatMatch(final Set<String> domain) {
		Set<DatatypeProperty> result = new HashSet<>();
		
		ComboFilter<DatatypeProperty> cf = new ComboFilter<>();
		cf.add(new DomainFilter<DatatypeProperty>(domain));
		cf.add(new NegativeFilter<>(new BooleanFilter()));
		
		obtainReadLock();
		try {
			ExtendedIterator<DatatypeProperty> iter = persistentModel.listDatatypeProperties()
																	 .filterKeep(cf);
			
			while(iter.hasNext()) {
				result.add(iter.next());
			}
			
			iter.close();
			
			return uriStringsOf(result);
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Returns all the boolean data type properties in the underlying ontology
	 * model that match the domain information.
	 * 
	 * @param domain
	 *            a set of domain class URIs.
	 * @return a set with the uri's of all matching data type properties in the
	 *         underlying ontology model.
	 */
	public Set<String> listBooleanDatatypePropertiesThatMatch(Set<String> domain) {
		Set<DatatypeProperty> result = new HashSet<>();
		
		ComboFilter<DatatypeProperty> cf = new ComboFilter<>();
		cf.add(new DomainFilter<DatatypeProperty>(domain));
		cf.add(new BooleanFilter());
		
		obtainReadLock();
		try {
			ExtendedIterator<DatatypeProperty> iter = persistentModel.listDatatypeProperties()
																	 .filterKeep(cf);
			
			while(iter.hasNext()) {
				result.add(iter.next());
			}
			
			iter.close();

			return uriStringsOf(result);
		} finally {
			releaseLock();
		}
	}
	
	public Set<String> getClassesOf(String individualUri) {
		Set<String> result = new HashSet<>();
		
		obtainReadLock();
		try {
			Individual individual = persistentModel.getIndividual(individualUri);
			if(individual != null) {
				Iterator<OntClass> iter = individual.listOntClasses(true);
				
				while(iter.hasNext()) {
					try {
						OntClass cls = iter.next();
						result.add(cls.getURI());
					} catch(ConversionException cex) { // Named individuals hack
						System.err.println("Warning: " + cex.getMessage() + " (" + individual.getLabel(null) + ')');
					}
				}
			}
		} finally {
			releaseLock();
		}
		
		return result;
	}
	
	public void addRelationTo(Individual subject, String predicateUri, Resource object) {
		obtainWriteLock();
		try {
			subject.addProperty(ResourceFactory.createProperty(predicateUri), object);
		} finally {
			releaseLock();
		}
		
	}

	public void addRelationTo(Individual subject, String predicateUri, String objectUri) {
		obtainWriteLock();
		try {
			subject.addProperty(ResourceFactory.createProperty(predicateUri), ResourceFactory.createResource(objectUri));
		} finally {
			releaseLock();
		}
	}
	
	public void addLiteralTo(Individual individual, String propertyUri, int value) {
		obtainWriteLock();
		try {
			internalAddLiteralTo(individual,
								 ResourceFactory.createProperty(propertyUri),
								 persistentModel.createTypedLiteral(value));
		} finally {
			releaseLock();
		}
	}
	
	public void addLiteralTo(Individual individual, String propertyUri, boolean value) {
		obtainWriteLock();
		try {
			internalAddLiteralTo(individual,
								 ResourceFactory.createProperty(propertyUri),
								 persistentModel.createTypedLiteral(value));
		} finally {
			releaseLock();
		}
	}

	public void addLiteralTo(Individual individual, String propertyUri, String value) {
		obtainWriteLock();
		try {
			internalAddLiteralTo(individual,
								 ResourceFactory.createProperty(propertyUri),
								 persistentModel.createTypedLiteral(value));
		} finally {
			releaseLock();
		}
	}
	
	public void addLiteralTo(Individual individual, String propertyUri, Calendar value) {
		obtainWriteLock();
		try {
			internalAddLiteralTo(individual,
								 ResourceFactory.createProperty(propertyUri),
								 persistentModel.createTypedLiteral(value));
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Clients must not modifify the returned individual.
	 * 
	 * @param uniqueName the unique name of the individual.
	 * @param label the label of the individual.
	 * @param ontClassUri the class of the individual.
	 * @return a payload consisting of the individual and a boolean denoting whether the individual was freshly created or not.
	 * @throws InvalidResourceException
	 */
	public Payload<Individual> fetchIndividual(String uniqueName, String label, String ontClassUri) throws InvalidResourceException {
		Payload<Individual> payload;
		
		Individual individual;
		
		obtainReadLock();
		try {
			individual = getIndividual(uniqueName);
		} finally {
			releaseLock();
		}
			
		if(individual == null) {
			obtainWriteLock();
			try {
				individual = createIndividual(uniqueName, ontClassUri);
				individual.addLiteral(RDFS.label, label);
			} finally {
				releaseLock();
			}
			
			payload = new Payload<>(individual, true);
		} else {
			obtainWriteLock();
			try {
				addOntClass(ontClassUri, individual);
			} finally {
				releaseLock();
			}
			payload = new Payload<>(individual);
		}
		
		return payload;
	}
	
	public boolean hasOntClass(String individualUri, String clsUri) {
		boolean result = false;
		
		obtainReadLock();
		try {
			Individual individual = persistentModel.getIndividual(individualUri);
			Resource cls = persistentModel.getResource(clsUri);
			if(individual != null) {
				result = individual.hasOntClass(cls);
			}
		} finally {
			releaseLock();
		}
		
		return result;
	}
	
	public boolean isDirty() {
		return isDirty;
	}
	
	public Set<QrItem> exec(String queryString) {
		Set<QrItem> queryResults = new HashSet<>();
		
		obtainReadLock();
		try {
			Query query = QueryFactory.create(queryString);
			QueryExecution qexec = QueryExecutionFactory.create(query, persistentModel);
			
			try {
				ResultSet results = qexec.execSelect();
				while (results.hasNext()) {
					QuerySolution soln = results.nextSolution();
					
					for(String var : results.getResultVars()) {
						RDFNode result = soln.get(var);
						
						if(result != null) {
							if(result.canAs(Individual.class)) {
								Individual individual = result.as(Individual.class);
								QrItem qr = new QrItem(this, individual.getLabel(null), ResultType.INDIVIDUAL);
								qr.setUri(individual.getURI());
								queryResults.add(qr);
							} else if(result.isLiteral()) {
								Literal literal = result.asLiteral();
								queryResults.add(new QrItem(this, literal.getLexicalForm(), ResultType.LITERAL));
							} else if(result.canAs(OntClass.class)) {
								OntClass cls = result.as(OntClass.class);
								QrItem qr = new QrItem(this, cls.getLocalName(), ResultType.CLASS);
								qr.setUri(cls.getURI());
								queryResults.add(qr);
							} else {
								System.err.println("Warning: ignoring unknown result type: " + result.toString());
							}
						}
					}
				}
			} finally {
				qexec.close();
			}
		} finally {
			releaseLock();
		}
		
		return queryResults;
	}
	
	public void execConstructQuery(String queryString) {
		obtainReadLock();
		try {
			Query query = QueryFactory.create(queryString);
			QueryExecution qexec = QueryExecutionFactory.create(query, persistentModel);
			
			try {
				persistentModel.add(qexec.execConstruct());
			} finally {
				qexec.close();
			}
		} finally {
			releaseLock();
		}
		
		markAsDirty();
	}

	public void export(String path) throws FileNotFoundException {
		if(isDirty) {
			reason();
		}
		
		obtainReadLock();
		try {
			persistentModel.writeAll(new FileOutputStream(path), "RDF/XML", null);
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Returns the domain of a given data type or object property from the
	 * underlying ontology model. Handles union classes by flattening them.
	 * 
	 * @param property
	 *            the property whose domain we want to fetch.
	 * @return a set URI strings describing the domain of the property.
	 */
	public Set<String> domainOf(String propertyUri) {
		obtainReadLock();
		try {
			OntProperty property = persistentModel.getOntProperty(propertyUri);
			return uriStringsOf(flatten(property.listDomain()));
		} finally {
			releaseLock();
		}
	}
	
	/**
	 * Returns the range of a given data type or object property from the
	 * underlying ontology model. Handles union classes by flattening them.
	 * 
	 * @param property
	 *            the property whose range we want to fetch.
	 * @return a set URI strings describing the range of the property.
	 */
	public Set<String> rangeOf(String propertyUri) {
		obtainReadLock();
		try {
			OntProperty property = persistentModel.getOntProperty(propertyUri);
			return uriStringsOf(flatten(property.listRange()));
		} finally {
			releaseLock();
		}
	}
	
	public String labelOf(String uri) {
		obtainReadLock();
		try {
			Individual individual = persistentModel.getIndividual(uri);
			return individual != null ? individual.getLabel(null) : null;
		} finally {
			releaseLock();
		}
	}
	
	public Set<String> labelsOf(String uri) {
		Set<String> result = new HashSet<>();
		
		obtainReadLock();
		try {
			Individual individual = persistentModel.getIndividual(uri);
			if(individual != null) {
				ExtendedIterator<RDFNode> iter = individual.listLabels(null);
				
				while(iter.hasNext()) {
					result.add(iter.next()
								   .asLiteral()
								   .getLexicalForm());
				}
				
				iter.close();
			}
		} finally {
			releaseLock();
		}
		
		return result;
	}

	public void close() {
		infModel.close(); // cascades to persistent model
	}

	/**
	 * Callers must not modify the model.
	 * 
	 * @return the Jena ontology model.
	 */
	protected OntModel getJenaModel() {
		return persistentModel;
	}

	private static Set<String> superClassesOf(OntClass cls) {
		Set<OntClass> classes = new HashSet<>();
		
		// locking done in public method
		ExtendedIterator<OntClass> iter = cls.listSuperClasses(false);
		while(iter.hasNext()) {
			classes.add(iter.next());
		}
		
		return uriStringsOf(classes);
	}
	
	private void obtainWriteLock() {
		persistentModel.enterCriticalSection(Model.WRITE);
	}
	
	private void obtainReadLock() {
		persistentModel.enterCriticalSection(Model.READ);
	}
	
	private void releaseLock() {
		persistentModel.leaveCriticalSection();
	}
	
	private void markAsDirty() {
		isDirty = true;
	}

	private void markAsClean() {
		isDirty = false;
	}

	private boolean isClean() {
		return !isDirty;
	}

	private Object rawType(Object in) {
		Object out;
		
		if(in instanceof XSDDateTime) {
			out = ((XSDDateTime) in).asCalendar();
		} else {
			out = in;
		}
		
		return out;
	}

	private Individual getIndividual(String uniqueName) {
		// locking done in public method
		return persistentModel.getIndividual(convertToUri(uniqueName));
	}

	private Individual createIndividual(String uniqueName, String ontClass) {
		markAsDirty();
		// locking done in public method
		return persistentModel.createIndividual(convertToUri(uniqueName), ResourceFactory.createResource(ontClass));
	}
	
	private String convertToUri(String uniqueName) {
		String uri;
		if(uniqueName.startsWith("http://") || uniqueName.startsWith("https://")) {
			uri = uniqueName;
		} else {
			uri = UriUtil.makeAbsolute(base, uniqueName);
		}
		return uri;
	}
	
	private void internalAddLiteralTo(Individual individual, Property property, Literal literal) {
		// locking done in public method
		individual.addLiteral(property, literal);
		
		markAsDirty();
	}
	
	private void createInfModel() {
		OntModelSpec infSpec = PelletReasonerFactory.THE_SPEC;
		infSpec.setDocumentManager(docManager);
		infModel = ModelFactory.createOntologyModel(infSpec, persistentModel);
	}
	
	private void addInfsToPersistentModel() {
		// locking done in public method
		persistentModel.add(extractInferences());
	}
	
	private Model extractInferences() {
		infModel.rebind(); // makes the reasoner aware of the tmp model changes
		
		// locking done in public method
		ModelExtractor extractor = new ModelExtractor(infModel);
		extractor.setSelector(
							EnumSet.of(
									StatementType.ALL_INSTANCE,
									StatementType.ALL_SUBCLASS,
									StatementType.ALL_SUBPROPERTY,
									StatementType.INVERSE_PROPERTY,
									StatementType.OBJECT_PROPERTY_VALUE
									)
							);
		return extractor.extractModel();
	}
	
	/**
	 * Flattens union classes by extracting the normal (atomic) classes from
	 * their collection, adding their URI strings to the result set. The URI
	 * strings of normal classes are added without any further processing.
	 * 
	 * @param iterator
	 *            the iterator that ranges over a set of classes.
	 * @return the URI strings of the normal classes.
	 */
	private static Set<OntClass> flatten(Iterator<? extends OntResource> iterator) {
		// locking done in public method
		Set<OntClass> result = new HashSet<>();
		
		while(iterator.hasNext()) {
			OntClass ontClass = iterator.next().asClass();
			if(ontClass.isUnionClass()) {
				UnionClass unionClass = ontClass.asUnionClass();
				
				ExtendedIterator<? extends OntClass> unionIterator = unionClass.listOperands();
				while(unionIterator.hasNext()) {
					result.add(unionIterator.next());
				}
			} else {
				result.add(ontClass);
			}
		}
		return result;
	}
	
	private static Set<String> uriStringsOf(Set<? extends OntResource> resources) {
		Set<String> result = new HashSet<>();
		
		for(OntResource resource : resources) {
			// locking done in public method
			if(resource.isURIResource()) {
				result.add(resource.getURI());
			}
		}
		
		return result;
	}
	
	private static void addOntClass(String classUri, Individual individual) throws InvalidResourceException {
		Resource cls = ResourceFactory.createResource(classUri);
		
		// locking done in public method
		if(!individual.hasOntClass(cls)) {
//			throw new InvalidResourceException(ontClass, individual.getOntClass(true));
			individual.addOntClass(cls);
		}
		
	}
	
	private static final class NegativeFilter<T extends OntProperty> extends Filter<T> {
		private Filter<T> filter;

		private NegativeFilter(Filter<T> filter) {
			this.filter = filter;
		}

		@Override
		public boolean accept(T o) {
			return !filter.accept(o);
		}
	}
	
	private static final class BooleanFilter extends Filter<DatatypeProperty> {
		@Override
		public boolean accept(DatatypeProperty o) {
			OntResource r = o.getRange();
			
			return r != null && r.equals(XSD.xboolean);
		}
		
	}
	
	private static final class ComboFilter<T extends OntProperty> extends Filter<T> {
		private List<Filter<T>> filters;
		
		private ComboFilter() {
			filters = new LinkedList<>();
		}
		
		public void add(Filter<T> filter) {
			filters.add(filter);
		}
		
		@Override
		public boolean accept(T o) {
			boolean accept = true;
			
			for(Filter<T> f : filters) {
				accept = accept && f.accept(o);
			}
			
			return accept;
		}
		
	}
	
	
	private static final class DomainFilter<T extends OntProperty> extends Filter<T> {
		private final Set<String> domain;
	
		private DomainFilter(Set<String> domain) {
			this.domain = domain;
		}
	
		@Override
		public boolean accept(T x) {
			Set<String> otherDomain = uriStringsOf(mergeWithSubClasses(flatten(x.listDomain())));
			
			return domain.isEmpty() || matches(domain, otherDomain);
		}
		
		private static boolean matches(Set<String> first, Set<String> second) {
			Set<String> intersection = new HashSet<>(first);
			intersection.retainAll(second);
			
			return intersection.size() > 0;
		}
		
		private static Set<OntClass> mergeWithSubClasses(Set<OntClass> ontClasses) {
			Set<OntClass> result = new HashSet<>();
			for(OntClass ontClass: ontClasses) {
				result.addAll(mergeWithSubClasses(ontClass));
			}
			
			return result;
		}
		
		private static Set<OntClass> mergeWithSubClasses(OntClass ontClass) {
			return mergeToSet(ontClass, ontClass.listSubClasses());
		}
		
		private static Set<OntClass> mergeToSet(OntClass ontClass, Iterator<OntClass> ontClasses) {
			Set<OntClass> result = new HashSet<>();
			result.add(ontClass);
			
			while(ontClasses.hasNext()) {
				result.add(ontClasses.next());
			}
			
			return result;
		}
	}

	private static final class RangeFilter extends Filter<OntClass> {
		private final Set<String> range;
	
		private RangeFilter(Set<String> range) {
			this.range = range;
		}
	
		@Override
		public boolean accept(OntClass x) {
			if(range.contains(x.getURI())) { // fast return
				return true;
			}
			
			// if range does not contain the exact uri, then we check
			// whether one of the super classes of x matches the range.
			// E.g. range => CodeEntity also covers Classes, Methods, etc.
			Set<String> superClasses = superClassesOf(x);
			superClasses.retainAll(range);
			
			return superClasses.size() > 0;
		}
	}
}
