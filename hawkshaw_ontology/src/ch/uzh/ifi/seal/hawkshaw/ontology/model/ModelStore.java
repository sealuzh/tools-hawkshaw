package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.Hashtable;
import java.util.Map;

import org.eclipse.core.runtime.IPath;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

import com.hp.hpl.jena.query.DataSource;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.store.DatasetGraphTDB;

public class ModelStore {
	private File tripleStoreLocation;
	private DatasetGraphTDB tdb;
	private DataSource src;
	
	private Map<String, Model> spawnedModels;
	
	public ModelStore(IPath tripleStorePath) {
		this(tripleStorePath.toFile());
	}
	
	public ModelStore(File tripleStoreLocation) {
		this.tripleStoreLocation = tripleStoreLocation;
		
		spawnedModels = new Hashtable<>();
	}
	
	public void init() {
		if(tdb == null) {
			tdb = TDBFactory.createDatasetGraph(tripleStoreLocation.getAbsolutePath());
			src = DatasetFactory.create(tdb);
		}
	}
	
	public void dispose() {
		syncAll();
		spawnedModels.clear();
		src.close();
	}

	public void wipe() {
		for(Model model : spawnedModels.values()) {
			model.close();
		}
		
		boolean exists = tripleStoreLocation.exists();
		boolean deleted = false;
		
		if(exists) {
			deleted = tripleStoreLocation.delete();
		}
		
		if(!(exists && deleted)) {
			throw new HawkshawException(OntologyCore.getDefault(), "Could not wipe store."); // TODO own exception
		}
	}
	
	public Model spawnModel(String modelUri) {
		Model model = spawnedModels.get(modelUri);
		
		if(model == null) {
			model = src.getNamedModel(modelUri);
			spawnedModels.put(modelUri, model);
		}
		
		return model;
	}

	public boolean exists(String modelUri) {
		return spawnedModels.containsKey(modelUri) || src.containsNamedModel(modelUri);
	}

	protected void syncAll() {
		for(Model model : spawnedModels.values()) {
			sync(model);
		}
	}

	protected void sync(Model jenaModel) {
		TDB.sync(jenaModel);
	}
}
