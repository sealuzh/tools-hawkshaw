package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.osgi.framework.Bundle;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.support.io.ResourceLocator;

import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class HawkshawModelFactory extends JobChangeAdapter {
	private IPath locationOfStore;
	
	private OntDocumentManager docManager;
	
	private ModelStore modelStore;
	
	private boolean initialized;

	private Map<String, AltEntry> altEntries;
	
	public HawkshawModelFactory(IPath locationOfStore) {
		this.locationOfStore = locationOfStore;
		
		initialized = false;
		
		collectAltEntryMappings();
		initDocManager();
	}
	
	public void startInit() {
		Job storeInitializer = new ModelStoreInitializer(locationOfStore);
		storeInitializer.addJobChangeListener(this);
		storeInitializer.schedule();
	}
	
	public boolean initComplete() {
		return initialized;
	}
	
	public void dispose() {
		if(modelStore != null) {
			modelStore.dispose();
		}
	}
	
	@Override
	public void done(IJobChangeEvent event) {
		ModelStoreInitializer storeInitializer = (ModelStoreInitializer) event.getJob();
		
		modelStore = storeInitializer.getModelStore();
		
		initialized = true;
	}
	
	public PersistentJenaModel createPersistentBaseModel(String modelUri) {
		boolean created = !exists(modelUri);
		
		Model tdbModel = modelStore.spawnModel(modelUri);

		OntModelSpec spec = OntModelSpec.OWL_DL_MEM;
		spec.setDocumentManager(docManager);
		
		OntModel ontModel = ModelFactory.createOntologyModel(spec, tdbModel);
//		ontModel.setStrictMode(false); // Potential named individuals hack - instead, exception is just caught when indiv.listOntClasses(true) is called
		
		PersistentJenaModel pjmodel =  new PersistentJenaModel(this, ontModel, modelUri);
		
		if(created) {
			addAutoLoads(pjmodel);
		}
		
		return pjmodel;
	}

	public void wipeModelStore() {
		modelStore.wipe();
	}

	public boolean exists(String modelUri) {
		return modelStore.exists(modelUri);
	}
	
	protected void addAutoLoads(PersistentJenaModel model) {
		for(AltEntry altEntry : altEntries.values()) {
			Set<String> uris = new HashSet<>();
			if(altEntry.isAutoLoad()) {
				uris.add(altEntry.getUri());
			}
			
			model.readOntologies(uris.toArray(new String[uris.size()]));
		}
	}

	protected void sync(PersistentJenaModel model) {
		modelStore.sync(model.getJenaModel());
	}
	
	private void initDocManager() {
		docManager = new OntDocumentManager();
		docManager.setProcessImports(false); // handled by app
		
		addAltEntries();
	}

	private void addAltEntries() {
		for(AltEntry altEntry : altEntries.values()) {
			docManager.addAltEntry(altEntry.getUri(), altEntry.getLocalPath());
		}
	}
	
	private void collectAltEntryMappings() {
		altEntries = new HashMap<>();
		
		IConfigurationElement[] config = Platform.getExtensionRegistry()
												 .getConfigurationElementsFor(OntologyCore.EXTENSION_ID_ALTENTRY);
		
		for(IConfigurationElement configElement : config) {
			String contributorName = configElement.getContributor().getName();
			
			String uri = configElement.getAttribute("uri");
			String altEntryPath = configElement.getAttribute("file");
			boolean autoLoad = Boolean.parseBoolean(configElement.getAttribute("autoLoad"));
			
			Bundle bundle = Platform.getBundle(contributorName);
			URL url = ResourceLocator.resolveUrl(bundle, altEntryPath);
			try {
				altEntries.put(uri, new AltEntry(uri, FileLocator.toFileURL(url).toString(), autoLoad));
			} catch (IOException ex) {
				throw new HawkshawException(OntologyCore.getDefault(), "Error while collecting ontology alt entries.", ex);
			}
		}
	}
	
	private static final class AltEntry {
		private String uri;
		private String localPath;
		
		private boolean autoLoad;
		
		public AltEntry(String uri, String localPath, boolean autoLoad) {
			this.uri       = uri;
			this.localPath = localPath;
			this.autoLoad  = autoLoad;
		}

		public String getUri() {
			return uri;
		}

		public String getLocalPath() {
			return localPath;
		}

		public boolean isAutoLoad() {
			return autoLoad;
		}
	}
}