package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.NonTerminalSymbol;

public class TestQueryStatement {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Tests whether the query statement is correctly represented as a <code>String</code>.
	 */
	@Test
	public void testToString() {
		QueryStatement stmt = new QueryStatement("LANGUAGE", SentenceType.UNDEF, "part1", "part2", "part3");
		assertThat(stmt.toString(), is(equalTo("part1 part2 part3")));
	}
	
	/**
	 * Tests whether cloning of a query statement works.
	 */
	@Test
	public void testDuplicate() {
		fail("Write a test!");
	}

	/**
	 * Tests whether {@link QueryStatement#merge(QueryStatement)} correctly
	 * replaces the wild cards of the left-hand-side query statement with the
	 * information from the right-hand-side query statement.
	 */
	@Test
	public void testMerge() {
		QueryStatement upper = new QueryStatement("LANGUAGE", SentenceType.UNDEF, "SELECT <<lower>>", "WHERE { <<lower:1>> <<lower>> . }", "<<lower>>");
		upper.setRule(new NonTerminalSymbol("<upper>", null));
		QueryStatement lower = new QueryStatement("", SentenceType.UNDEF, "?var", "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ont/java#Method>", "");
		lower.setRule(new NonTerminalSymbol("<lower>", null));
		
		QueryStatement merged = upper.merge(lower);
		assertThat(merged.toString(), is(equalTo("SELECT ?var WHERE { ?var <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ont/java#Method> . }")));
	}
	
	/**
	 * Tests whether {@link QueryStatement#merge(QueryStatement)} correctly
	 * replaces the wild cards of the left-hand-side query statement with the
	 * information from the right-hand-side query statement when both query statements are of a different size.
	 */
	@Test
	public void testMergeDifferntNumberOfParts() {
		QueryStatement upper = new QueryStatement("LANGUAGE", SentenceType.UNDEF, "", "<<lower>>", "more");
		upper.setRule(new NonTerminalSymbol("<upper>", null));
		QueryStatement lower = new QueryStatement("", SentenceType.UNDEF, "?var", "foo");
		lower.setRule(new NonTerminalSymbol("<lower>", null));
		
		QueryStatement merged = upper.merge(lower);
		assertThat(merged.toString(), is(equalTo("foo more")));
	}
}
