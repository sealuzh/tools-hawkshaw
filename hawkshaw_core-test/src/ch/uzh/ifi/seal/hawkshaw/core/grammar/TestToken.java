package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Queue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;

public class TestToken {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testTokenize() {
		Queue<Token> tokens = Token.tokenize("Hello [cruel] {world} !");
		
		assertThat(tokens, is(notNullValue()));
		assertThat(tokens.size(), is(4));
		
		Token next = tokens.remove();
		assertThat(next.getValue(), is(equalTo("Hello")));
		assertThat(next.isSubject(), is(false));
		assertThat(next.isOptional(), is(false));
		
		next = tokens.remove();
		assertThat(next.getValue(), is(equalTo("cruel")));
		assertThat(next.isSubject(), is(false));
		assertThat(next.isOptional(), is(true));
		
		next = tokens.remove();
		assertThat(next.getValue(), is(equalTo("world")));
		assertThat(next.isSubject(), is(true));
		assertThat(next.isOptional(), is(false));
		
		next = tokens.remove();
		assertThat(next.getValue(), is(equalTo("!")));
		assertThat(next.isSubject(), is(false));
		assertThat(next.isOptional(), is(false));
		
		assertThat(tokens.isEmpty(), is(true));
	}
	
	@Test
	public void testEquals() {
		Token t1 = new Token("word");
		Token t2 = new Token("word");
		Token t3 = new Token("wort");
		Token t4 = new Token("word");
		t4.markAsOptional();
		Token t5 = new Token("word");
		t5.markAsSubject();
		
		assertThat(t1, is(equalTo(t2)));
		assertThat(t1, is(not(equalTo(t3))));
		assertThat(t1, is(not(equalTo(t4))));
		assertThat(t1, is(not(equalTo(t5))));
	}

	@Test
	public void testHashCode() {
		Token t1 = new Token("word");
		Token t2 = new Token("word");
		Token t3 = new Token("wort");
		Token t4 = new Token("word");
		t4.markAsOptional();
		Token t5 = new Token("word");
		t5.markAsSubject();
		
		assertThat(t1.hashCode(), is(equalTo(t2.hashCode())));
		assertThat(t1.hashCode(), is(not(equalTo(t3.hashCode()))));
		assertThat(t1.hashCode(), is(not(equalTo(t4.hashCode()))));
		assertThat(t1.hashCode(), is(not(equalTo(t5.hashCode()))));
	}
}
