package ch.uzh.ifi.seal.hawkshaw.core;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.TestChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.TestGrammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.TestQueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.TestRuleLoader;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.TestSentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.TestToken;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.owl.TestModelWrapper;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.TestAbstractSymbol;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.TestDynamicSymbol;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.TestNonTerminalSymbol;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.TestSymbolContainer;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.TestSymbolFactory;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.TestTerminalSymbol;
import ch.uzh.ifi.seal.hawkshaw.core.query.TestAnswer;
import ch.uzh.ifi.seal.hawkshaw.core.query.TestElement;
import ch.uzh.ifi.seal.hawkshaw.core.query.TestPath;
import ch.uzh.ifi.seal.hawkshaw.core.query.TestProposal;
import ch.uzh.ifi.seal.hawkshaw.core.query.TestQuestion;
import ch.uzh.ifi.seal.hawkshaw.core.query.TestSession;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestAbstractSymbol.class,
	TestAnswer.class,
	TestChainingRestriction.class,
	TestDynamicSymbol.class,
	TestElement.class,
	TestGrammar.class,
	TestModelWrapper.class,
	TestNonTerminalSymbol.class,
	TestPath.class,
	TestProposal.class,
	TestQueryStatement.class,
	TestQuestion.class,
	TestRuleLoader.class,
	TestSentenceType.class,
	TestSession.class,
	TestSymbolContainer.class,
	TestSymbolFactory.class,
	TestTerminalSymbol.class,
	TestToken.class
})
public class AllTests {

}
