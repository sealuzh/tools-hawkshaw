package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static ch.uzh.ifi.seal.hawkshaw.core.query.IsComplete.complete;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;

import ch.uzh.ifi.seal.hawkshaw.core.exceptions.UnexpectedWordException;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;

@SuppressWarnings("restriction")
public class TestQuestion {	
	private static PersistentJenaModel jenaModel;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		OntologyCore.waitForInit(null);
		
		jenaModel = OntologyCore.createPersistentBaseModel("http://seal.ifi.uzh.ch/hawkshaw/test/question");
		jenaModel.readOntologies("http://evolizer.org/ontologies/evolizer-nl/testing");		
		
		jenaModel.reason();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		jenaModel.clear();
		jenaModel.close(); // will cascade to base model.
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBasicComposition() {
		Grammar grammar = new Grammar();
		
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, ""));
		grammar.addRule("<START>", Token.asTokens("<SPO_QENUM>", "?"), new QueryStatement("SPARQL", SentenceType.ENUM_QUESTION, ""));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("Where", "is", "<person>"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<SPO_QENUM>", Token.asTokens("How", "old", "is", "<person>"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<person>", Token.asTokens("Gigs"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<person>", Token.asTokens("Michael", "Wuersch"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<person>", Token.asTokens("Michael", "Knight"), new QueryStatement("", SentenceType.UNDEF, ""));
		
		Question question = new Question(grammar);
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItems("How", "Where"));
		
		question.add("Where");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		question.add("is");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(2));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Gigs", "Michael"));
		question.add("Michael");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(3));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Knight", "Wuersch"));
		question.add("Wuersch");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(4));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(5));
		question.isComplete();
		assertThat(question, is(complete()));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(0));
	}
	
	@Test
	public void testCompopositionWithOverlappingRules() {
		Grammar grammar = new Grammar();
		grammar.addRule("<START>", Token.asTokens("Where", "is", "<name>", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, ""));
		grammar.addRule("<name>", Token.asTokens("Michael", "Wuersch"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<name>", Token.asTokens("Michael"), new QueryStatement("", SentenceType.UNDEF, ""));
		
		Question question = new Question(grammar);
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Where"));
		question.add("Where");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("is"));
		question.add("is");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(2));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Michael"));
		question.add("Michael");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(3));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Wuersch", "?"));
		question.add("?");
		assertThat(question, is(complete()));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(4));
	}

	@Test
	public void testUnexpectedWordException() {
		Grammar grammar = new Grammar();
		
		grammar.addRule("<START>", Token.asTokens("What", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, ""));
	
		Question question = new Question(grammar);
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItem("What"));
		
		try {
			question.add("foobie");
			fail("Expected an UnexpectedWordException.");
		} catch(UnexpectedWordException ex) {
			assertThat(ex.getUnexpectedWord(), is("foobie"));
			assertThat(ex.getAlternates(), hasItem("What"));
		}
	}
	
	@Test
	public void testCompositionWithBranches() {
		Grammar grammar = new Grammar();
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, ""));
		grammar.addRule("<START>", Token.asTokens("<SPO_QSPAT>", "?"), new QueryStatement("SPARQL", SentenceType.SPAT_QUESTION, ""));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("What", "is", "the", "age", "of", "michael"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<SPO_QSPAT>", Token.asTokens("What", "region", "will", "michael", "visit", "in", "september"), new QueryStatement("", SentenceType.UNDEF, ""));
		
		Question question = new Question(grammar);
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItems("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(2));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("is", "region"));
		question.add("is");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(2));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("the"));
		question.add("the");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(3));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("age"));
		question.add("age");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(4));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("of"));
		question.add("of");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(5));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("michael"));
		question.add("michael");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(6));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("?"));
		question.add("?");
		assertThat(question, is(complete()));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(7));
	}
	
	@Test
	public void testBasicRemovalOfLastWord() {
		Grammar grammar = new Grammar();
		
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, ""));
		grammar.addRule("<START>", Token.asTokens("<SPO_QENUM>", "?"), new QueryStatement("SPARQL", SentenceType.ENUM_QUESTION, ""));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("Where", "is", "<person>"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<SPO_QENUM>", Token.asTokens("How", "old", "is", "<person>"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<person>", Token.asTokens("Gigs"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<person>", Token.asTokens("Michael", "Wuersch"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<person>", Token.asTokens("Michael", "Knight"), new QueryStatement("", SentenceType.UNDEF, ""));
	
		Question question = new Question(grammar);
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		// Add 'Where'
		Set<String> nextWords = question.nextWords();		
		assertThat(nextWords, hasItems("How", "Where"));
		
		question.add("Where");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		
		// Remove 'Where'
		question.removeLastWord();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		// Add 'Where'
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("How", "Where"));
		
		question.add("Where");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		// Add 'is?
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		
		question.add("is");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(2));
		
		// Remove 'is', 'Where'
		question.removeLastWord();
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		
		question.removeLastWord();
		
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		// Add 'Where is Michael'
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("How", "Where"));
		
		question.add("Where");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		
		question.add("is");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(2));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Gigs", "Michael"));
		
		question.add("Michael");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(3));
		
		// Remove 'Michael', 'is', 'Where'
		question.removeLastWord();
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Gigs", "Michael"));
		
		question.removeLastWord();
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		
		question.removeLastWord();
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("How", "Where"));
		
		// Add 'Where is Michael Wuersch?'
		question.add("Where");
		question.add("is");
		question.add("Michael");
		question.add("Wuersch");
		question.add("?");
				
		assertThat(question, is(complete()));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(0));
		
		// Remove '?', 'Wuersch'
		question.removeLastWord();
		question.removeLastWord();
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("Wuersch", "Knight"));
		
		question.add("Knight");
		question.add("?");
		
		assertThat(question, is(complete()));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(0));
		
		// Remove all: '?', 'Knight', 'Michael', 'is', 'Where'
		question.removeLastWord();
		question.removeLastWord();
		question.removeLastWord();
		question.removeLastWord();
		question.removeLastWord();
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("How", "Where"));
		assertThat(question, is(not(complete())));
	}
	
	@Test
	public void testRemovalOfLastWordWithBranches() {
		Grammar grammar = new Grammar();
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, ""));
		grammar.addRule("<START>", Token.asTokens("<SPO_QSPAT>", "?"), new QueryStatement("SPARQL", SentenceType.SPAT_QUESTION, ""));
		grammar.addRule("<START>", Token.asTokens("<SPO_QTEMP>", "?"), new QueryStatement("SPARQL", SentenceType.TEMP_QUESTION, ""));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("What", "is", "the", "age", "of", "michael"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<SPO_QSPAT>", Token.asTokens("What", "region", "will", "michael", "visit", "in", "september"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<SPO_QTEMP>", Token.asTokens("What", "is", "the", "time"), new QueryStatement("", SentenceType.UNDEF, ""));
		
		Question question = new Question(grammar);
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItems("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(3));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		// Remove 'What'
		question.removeLastWord();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		
		// Add 'What is the time'
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(3));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("is", "region"));
		
		question.add("is");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(2));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(2));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("the"));
		
		question.add("the");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(2));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(3));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("time", "age"));
		
		question.add("time");
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(2));
		assertThat(question.getWordCount(), is(4));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("?"));
		
		question.add("?");
		assertThat(question, is(complete()));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(2));
		assertThat(question.getWordCount(), is(5));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(0));
		
		// Remove '?', 'time', 'the', 'is'
		question.removeLastWord();
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(2));
		assertThat(question.getWordCount(), is(4));
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("?"));
		
		question.removeLastWord();
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(2));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(3));
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("time", "age"));
		
		question.removeLastWord();
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(2));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(2));
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("the"));
		
		question.removeLastWord();
		assertThat(question, is(not(complete())));
		assertThat(question.getNrOfActivePaths(), is(3));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("is", "region"));
		
		question.removeLastWord();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("What"));
		
		// Try to remove non-existing word - to check whether initial path survives.
		question.removeLastWord();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		nextWords = question.nextWords();
		assertThat(nextWords, hasItems("What"));
	}
	
	@Test
	public void testClear() {
		Grammar grammar = new Grammar();
		grammar.addRule("<START>", Token.tokenize("word1 <branch1> word4"), new QueryStatement("SPARQL", SentenceType.UNDEF, ""));
		grammar.addRule("<START>", Token.tokenize("word1 <branch2> word4"), new QueryStatement("SPARQL", SentenceType.UNDEF, ""));
		grammar.addRule("<branch1>", Token.tokenize("word2 word3a"), new QueryStatement("", SentenceType.UNDEF, ""));
		grammar.addRule("<branch2>", Token.tokenize("word2 word3b"), new QueryStatement("", SentenceType.UNDEF, ""));
		
		Question question = new Question(grammar);
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		question.clear();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		question.add("word1");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		assertThat(question, is(not(complete())));
		
		question.clear();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		question.add("word1");
		question.add("word2");
		assertThat(question.getNrOfActivePaths(), is(2));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(2));
		assertThat(question, is(not(complete())));
		
		question.clear();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		question.add("word1");
		question.add("word2");
		question.add("word3a");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(3));
		assertThat(question, is(not(complete())));
		
		question.clear();
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		question.add("word1");
		question.add("word2");
		question.add("word3a");
		question.add("word4");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(1));
		assertThat(question.getWordCount(), is(4));
		assertThat(question, is(complete()));
	}

	@Test
	public void testGetQueryStatements() {
		Grammar grammar = new Grammar();
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.UNDEF, "SELECT <<SPO_QOBJ>>", "WHERE { <<SPO_QOBJ>> }"));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("What", "<subj>", "<verb_obj>"), new QueryStatement(null, SentenceType.OPEN_QUESTION, "<<subj>>", "<<subj:1>> <<subj>> . <<subj:1>> <<verb_obj>>"));
		grammar.addRule("<subj>", Token.asTokens("<clazz>"), new QueryStatement(null, SentenceType.UNDEF, "<<clazz>>", "<<clazz>>"));
		grammar.addRule("<clazz>", Token.asTokens("<noun>"), new QueryStatement(null, SentenceType.UNDEF, "<<noun>>", "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<noun>>"));
		grammar.addRule("<noun>", Token.asTokens("method"), new QueryStatement(null, SentenceType.UNDEF, "?method", "<http://evolizer.org/ontologies/java#Method>"));
		grammar.addRule("<verb_obj>", Token.asTokens("calls", "<obj>"), new QueryStatement(null, SentenceType.UNDEF, "", "<http://evolizer.org/ontologies/java#invokeMethod> <<obj:1>> . <<obj:1>> <<obj>>"));
		grammar.addRule("<obj>", Token.asTokens("<instance>"), new QueryStatement(null, SentenceType.UNDEF, "<<instance>>", "<http://www.w3.org/2002/07/owl#sameAs> <<instance>>"));
		grammar.addRule("<instance>", Token.asTokens("toString"), new QueryStatement(null, SentenceType.UNDEF, "?toString", "<http://evolizer.org/ontologies/projects/jmonkey#toString>"));
		
		Question question = new Question(grammar);

		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(0));
		assertThat(question, is(not(complete())));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItem("What"));
		
		question.add("What");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(1));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("method"));
		question.add("method");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(2));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("calls"));
		question.add("calls");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(3));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("toString"));
		question.add("toString");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(4));
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question.getNrOfActivePaths(), is(1));
		assertThat(question.getNrOfResolvedPaths(), is(0));
		assertThat(question.getWordCount(), is(5));
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
										   			"?method " +
										   "WHERE { " +
										   			"?method <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/java#Method> . " +
										   			"?method <http://evolizer.org/ontologies/java#invokeMethod> ?toString . " +
										   			"?toString <http://www.w3.org/2002/07/owl#sameAs> <http://evolizer.org/ontologies/projects/jmonkey#toString> " +
										   		  "}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}

	@Test
	public void testCompileWithDynamicNouns() throws IOException {	
		Grammar grammar = new Grammar();
		grammar.addRule("<START>",
						Token.asTokens("What", "{<class>}", "is", "there", "?"),
						new QueryStatement("SPARQL",
										   SentenceType.OPEN_QUESTION,
										   "SELECT <<class>>",
										   "WHERE { <<class>> . }"
						)
		);
		grammar.addRule("<class>",
						Token.asTokens("<NC-s>"),
						new QueryStatement(null,
										   SentenceType.UNDEF,
										   "<<NC-s>>",
										   "<<NC-s:1>> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>"
						)
		);
			
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItem("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("Dog"));
		question.add("Dog");
		assertThat(question, is(not(complete())));

		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("Food"));
		question.add("Food");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("is"));
		question.add("is");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("there"));
		question.add("there");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question, is(complete()));
		
		assertThat(question.compile(), hasItem("SELECT " +
													  "?petfood " +
											   "WHERE { " +
											   		  "?petfood <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#PetFood> . " +
											   "}"));
	}
	
	@Test
	public void testCompileWithDynamicVerbs() throws IOException {	
		Grammar grammar = new Grammar();
		grammar.addRule("<START>",
						Token.asTokens("What", "<C>", "<V-s>", "<I>", "?"),
						new QueryStatement("SPARQL",
										   SentenceType.OPEN_QUESTION,
										   "SELECT <<C>>",
										   "WHERE { " +
										   		    "<<C:1>> <<V-s>> ?michael . " +
										   		    "<<C:1>> <<C>> . " +
										   		    "<<I:1>> <<I>> . " +
										   		 "}"
						)
		);
		
		grammar.addRule("<C>",
				Token.asTokens("Dog"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "?dog",
								   "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#Dog>"
				)
		);
		
		grammar.addRule("<I>",
				Token.asTokens("Michael"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "?michael",
								   "<http://www.w3.org/2002/07/owl#sameAs> <http://evolizer.org/ontologies/evolizer-nl/testing#michael>"
				)
		);
			
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItem("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("Dog"));
		question.add("Dog");
		assertThat(question, is(not(complete())));

		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("lives"));
		question.add("lives");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("with"));
		question.add("with");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("Michael"));
		question.add("Michael");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
											  	    "?dog " +
										   "WHERE { " +
											   		"?dog <http://evolizer.org/ontologies/evolizer-nl/testing#livesWith> ?michael . " +
											   		"?dog <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#Dog> . " +
											   		"?michael <http://www.w3.org/2002/07/owl#sameAs> <http://evolizer.org/ontologies/evolizer-nl/testing#michael> . " +
											     "}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}
	
	@Test
	public void testCompileWithRestriction() throws IOException {	
		Grammar grammar = new Grammar();
		grammar.addRule("<START>",
						Token.asTokens("What", "{<C>}", "<V-s>", "<I>", "?"),
						new QueryStatement("SPARQL",
										   SentenceType.OPEN_QUESTION,
										   "SELECT <<C>>",
										   "WHERE { " +
										   		    "<<C:1>> <<V-s>> ?instance . " +
										   		    "<<C:1>> <<C>> . " +
										   		    "?instance <<I>> . " +
										   		 "}"
						)
		);
		
		grammar.addRule("<C>",
				Token.asTokens("<NC-s>"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "<<NC-s>>",
								   "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>"
				)
		);
		
		grammar.addRule("<I>",
				Token.asTokens("<NI>"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "<<NI>>",
								   "<http://www.w3.org/2002/07/owl#sameAs> <<NI>>"
				)
		);
			
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItem("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		// size check does not make sense - auto loads!
		assertThat(nextWords, hasItems("Car", "Best", "Dog", "Pet", "Human", "Master", "Person", "Commodity", "Good", "Product", "Plaything", "Toy", "Shop", "Store"));
		question.add("Store");
		assertThat(question, is(not(complete())));

		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(3));
		assertThat(nextWords, hasItems("sells", "provides", "offers"));
		question.add("sells");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(2));
		assertThat(nextWords, hasItems("Caesar", "Pedigree"));
		question.add("Caesar");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(1));
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
											  	    "?supermarket " +
										   "WHERE { " +
											   		"?supermarket <http://evolizer.org/ontologies/evolizer-nl/testing#sellsPetFood> ?instance . " +
											   		"?supermarket <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#SuperMarket> . " +
											   		"?instance <http://www.w3.org/2002/07/owl#sameAs> <http://evolizer.org/ontologies/evolizer-nl/testing#caesar> . " +
											     "}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}
	
	@Test
	public void testCompileWithRestrictionAndConjunction() throws IOException {	
		Grammar grammar = new Grammar();
		grammar.addRule("<START>",
						Token.asTokens("What", "{<C>}", "<V-s>", "<I>", "?"),
						new QueryStatement("SPARQL",
										   SentenceType.OPEN_QUESTION,
										   "SELECT <<C>>",
										   "WHERE { " +
										   		    "<<C:1>> <<V-s>> ?instance . " +
										   		    "<<C:1>> <<C>> . " +
										   		    "?instance <<I>> . " +
										   		 "}"
						)
		);
		
		grammar.addRule("<C>",
				Token.asTokens("<NC-s>"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "<<NC-s>>",
								   "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>"
				)
		);
		
		grammar.addRule("<I>",
				Token.asTokens("<NI>"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "<<NI>>",
								   "<http://www.w3.org/2002/07/owl#sameAs> <<NI>>"
				)
		);
			
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords, hasItem("What"));
		question.add("What");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
//		assertThat(nextWords.size(), is(14));
		assertThat(nextWords, hasItems("Car", "Best", "Dog", "Pet", "Human", "Master", "Person", "Commodity", "Good", "Product", "Plaything", "Toy", "Shop", "Store"));
		question.add("Store");
		assertThat(question, is(not(complete())));

		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(3));
		assertThat(nextWords, hasItems("sells", "provides", "offers"));
		question.add("sells");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(2));
		assertThat(nextWords, hasItems("Caesar", "Pedigree"));
		question.add("Caesar");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(1));
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
											  	    "?supermarket " +
										   "WHERE { " +
											   		"?supermarket <http://evolizer.org/ontologies/evolizer-nl/testing#sellsPetFood> ?instance . " +
											   		"?supermarket <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#SuperMarket> . " +
											   		"?instance <http://www.w3.org/2002/07/owl#sameAs> <http://evolizer.org/ontologies/evolizer-nl/testing#caesar> . " +
											     "}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}
	
	@Test
	public void testCompileWithInterrogatives() throws IOException {	
		Grammar grammar = new Grammar();
		grammar.addRule("<START>",
						Token.asTokens("<VInter>", "is", "<NI>", "?"),
						new QueryStatement("SPARQL",
										   SentenceType.OPEN_QUESTION,
										   "SELECT <<VInter>>",
										   "WHERE { " +
										   			"<<NI>> <<VInter>> <<VInter:1>> . " +
										   		 "}"
						)
		);
		
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords.size(), is(1));
		assertThat(nextWords, hasItem("How"));
		question.add("How");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(2));
		assertThat(nextWords, hasItems("old", "much"));
		question.add("old");
		assertThat(question, is(not(complete())));

		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(1));
		assertThat(nextWords, hasItem("is"));
		question.add("is");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(4));
		assertThat(nextWords, hasItems("Doggy", "Pentium", "Stupid", "Wuffi"));
		question.add("Doggy");
		assertThat(question, is(not(complete())));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(1));
		assertThat(nextWords, hasItem("?"));
		question.add("?");
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
													"?hasage " +
										   "WHERE { " +
													"<http://evolizer.org/ontologies/evolizer-nl/testing#doggy> <http://evolizer.org/ontologies/evolizer-nl/testing#hasAge> ?hasage . " +
												 "}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}
	
	@Test
	public void testPhrase() {
		Grammar grammar = new Grammar();
		// SPARQL <START> ::= <SPO_QOBJ> ? | SELECT <<SPO_QOBJ>> | WHERE { <<SPO_QOBJ>> . }
		grammar.addRule("<START>", Token.tokenize("<SPO_QOBJ> ?"), new QueryStatement("SPARQL", SentenceType.CLOSED_QUESTION, "SELECT <<SPO_QOBJ>>", "WHERE { <<SPO_QOBJ>> . }"));
		// <SPO_QOBJ> ::= <INT_DET> <class> are there | <<class>> | <<class:1>> <<class>>
		grammar.addRule("<SPO_QOBJ>", Token.tokenize("<INT_DET> <class> are there"), new QueryStatement("", SentenceType.UNDEF, "<<class>>", "<<class:1>> <<class>>"));
		// <class> ::= <NC-s> | <<NC-s>> | <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>
		grammar.addRule("<class>", Token.tokenize("<NCx>"), new QueryStatement("", SentenceType.UNDEF, "<<NCx>>", "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NCx>>"));
		grammar.addRule("<NCx>", Token.tokenize("Foo"), new QueryStatement("", SentenceType.UNDEF, "?foo", "<http://dummy.org#foo>"));
		grammar.addRule("<NCx>", Token.tokenize("Bar"), new QueryStatement("", SentenceType.UNDEF, "?bar", "<http://dummy.org#bar>"));
		grammar.addRule("<INT_DET>", Token.tokenize("What"), new QueryStatement("", SentenceType.UNDEF, "", ""));
		grammar.addRule("<INT_DET>", Token.tokenize("Which"), new QueryStatement("", SentenceType.UNDEF, "", ""));
		
		Question question = new Question(grammar);
		assertThat(question.toString(), is(""));
		
		question.add("Which");
		assertThat(question.toString(), is("Which"));
		
		question.add("Foo");
		assertThat(question.toString(), is("Which Foo"));
		
		question.add("are");
		assertThat(question.toString(), is("Which Foo are"));
		
		question.add("there");
		assertThat(question.toString(), is("Which Foo are there"));
		
		question.add("?");
		assertThat(question.toString(), is("Which Foo are there ?"));
		
		assertThat(question, is(complete()));
	}
	
	@Test
	public void testConjunctionType1() {
		Grammar grammar = new Grammar();
		// SPARQL <START> ::= <SPO_QOBJ> ? | SELECT <<SPO_QOBJ>> | WHERE { <<SPO_QOBJ>> . }
		grammar.addRule("<START>", Token.tokenize("<SPO_QOBJ> ?"), new QueryStatement("SPARQL", SentenceType.CLOSED_QUESTION, "SELECT <<SPO_QOBJ>>", "WHERE { <<SPO_QOBJ>> . }"));
		// <SPO_QOBJ> ::= <INT_DET> <class> <V-s> <NI> | ?result | ?result <<V-s>> <<NI>> . ?result <<class>>
		grammar.addRule("<SPO_QOBJ>", Token.tokenize("What <class> <V-s> <NI>"), new QueryStatement("", SentenceType.UNDEF, "?result", "?result <<V-s>> <<NI>> . ?result <<class>>"));
		// <SPO_QOBJ> ::= What <class> <V-s> <NI> and <NI_2> | ?result | ?result <<V-s>> <<NI>> . ?result <<V-s>> <<NI_2>> . ?result <<class>>
		grammar.addRule("<SPO_QOBJ>", Token.tokenize("What <class> <V-s> <NI> and <NI_2>"), new QueryStatement("", SentenceType.UNDEF, "?result", "?result <<V-s>> <<NI>> . ?result <<V-s>> <<NI_2>> . ?result <<class>>"));
		// <NI_2> ::== <NI> | - | <<NI>>
		grammar.addRule("<NI_2>", Token.tokenize("<NI>"), new QueryStatement("", SentenceType.UNDEF, "<<NI>>", "<<NI>>"));
		// <class> ::= <NC-s> | <<NC-s>> | <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>
		grammar.addRule("<class>",
				Token.asTokens("<NC-s>"),
				new QueryStatement(null,
								   SentenceType.UNDEF,
								   "<<NC-s>>",
								   "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>"
				)
		);
		
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		assertThat(question.toString(), is(""));
		
		question.add("What");
		assertThat(question.toString(), is("What"));
		
		question.add("Person");
		assertThat(question.toString(), is("What Person"));
		
		question.add("shops");
		assertThat(question.toString(), is("What Person shops"));
		
		question.add("in");
		assertThat(question.toString(), is("What Person shops in"));
		
		question.add("Qualipet");
		assertThat(question.toString(), is("What Person shops in Qualipet"));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords.size(), is(2));
		assertThat(nextWords, hasItems("and", "?"));
		
		question.add("and");
		assertThat(question.toString(), is("What Person shops in Qualipet and"));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(2));
		assertThat(nextWords, hasItems("Migros", "Qualipet"));
		
		question.add("Migros");
		assertThat(question.toString(), is("What Person shops in Qualipet and Migros"));
		
		question.add("?");
		assertThat(question.toString(), is("What Person shops in Qualipet and Migros ?"));
		
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
													"?result " +
										   "WHERE { " +
													"?result <http://evolizer.org/ontologies/evolizer-nl/testing#shopsIn> <http://evolizer.org/ontologies/evolizer-nl/testing#qualipet> . " +
													"?result <http://evolizer.org/ontologies/evolizer-nl/testing#shopsIn> <http://evolizer.org/ontologies/evolizer-nl/testing#migros> . " +
													"?result <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#PetOwner> . " +
												"}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}
	
	@Test
	public void testConjunctionType2() {
		Grammar grammar = new Grammar();
		// SPARQL <START> ::= <SPO_QOBJ> ? | SELECT <<SPO_QOBJ>> | WHERE { <<SPO_QOBJ>> . }
		grammar.addRule("<START>", Token.tokenize("<SPO_QOBJ> ?"), new QueryStatement("SPARQL", SentenceType.CLOSED_QUESTION, "SELECT <<SPO_QOBJ>>", "WHERE { <<SPO_QOBJ>> . }"));
		// <SPO_QOBJ> ::= <INT_DET> <class> <V-s> <NI> | ?result | ?result <<V-s>> <<NI>> . ?result <<class>>
		grammar.addRule("<SPO_QOBJ>", Token.tokenize("What <class> <V-s> <NI>"), new QueryStatement("", SentenceType.UNDEF, "?result", "?result <<V-s>> <<NI>> . ?result <<class>>"));
		// <SPO_QOBJ> ::= What <class> <V-s> <NI> and <NI_2> | ?result | ?result <<V-s>> <<NI>> . ?result <<V-s>> <<NI_2>> . ?result <<class>>
		grammar.addRule("<SPO_QOBJ>", Token.tokenize("What {<class>} <V-s> <NI> and <V_2> <NI_2>"), new QueryStatement("", SentenceType.UNDEF, "?result", "?result <<V-s>> <<NI>> . ?result <<V_2>> <<NI_2>> . ?result <<class>>"));
		// <V_2> ::== <V-s> | <<V-s>> | <<V-s>>
		grammar.addRule("<V_2>", Token.tokenize("<V-s>"), new QueryStatement("", SentenceType.UNDEF, "<<V-s>>", "<<V-s>>"));
		// <NI_2> ::== <NI> | <<NI>> | <<NI>>
		grammar.addRule("<NI_2>", Token.tokenize("<NI>"), new QueryStatement("", SentenceType.UNDEF, "<<NI>>", "<<NI>>"));
		// <class> ::= <NC-s> | <<NC-s>> | <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>
		grammar.addRule("<class>",
				Token.asTokens("<NC-s>"),
				new QueryStatement(null,
								   SentenceType.UNDEF,
								   "<<NC-s>>",
								   "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>"
				)
		);
		
		grammar.setModel(jenaModel);
		
		Question question = new Question(grammar);
		assertThat(question.toString(), is(""));
		
		question.add("What");
		assertThat(question.toString(), is("What"));
		
		question.add("Person");
		assertThat(question.toString(), is("What Person"));
		
		question.add("shops");
		assertThat(question.toString(), is("What Person shops"));
		
		question.add("in");
		assertThat(question.toString(), is("What Person shops in"));
		
		question.add("Qualipet");
		assertThat(question.toString(), is("What Person shops in Qualipet"));
		
		Set<String> nextWords = question.nextWords();
		assertThat(nextWords.size(), is(2));
		assertThat(nextWords, hasItems("and", "?"));
		
		question.add("and");
		assertThat(question.toString(), is("What Person shops in Qualipet and"));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(5));
		assertThat(nextWords, hasItems("buys", "shops", "is", "lives", "owns"));
		
		question.add("buys");
		assertThat(question.toString(), is("What Person shops in Qualipet and buys"));
		
		nextWords = question.nextWords();
		assertThat(nextWords.size(), is(5));
		assertThat(nextWords, hasItems("Pedigree", "Caesar", "Juicy", "Rubber", "Stuffed"));
		
		question.add("Pedigree");
		assertThat(question.toString(), is("What Person shops in Qualipet and buys Pedigree"));
		
		question.add("?");
		assertThat(question.toString(), is("What Person shops in Qualipet and buys Pedigree ?"));
		
		assertThat(question, is(complete()));
		
		String expectedSparqlQueryString = "SELECT " +
													"?result " +
										   "WHERE { " +
													"?result <http://evolizer.org/ontologies/evolizer-nl/testing#shopsIn> <http://evolizer.org/ontologies/evolizer-nl/testing#qualipet> . " +
													"?result <http://evolizer.org/ontologies/evolizer-nl/testing#buys> <http://evolizer.org/ontologies/evolizer-nl/testing#pedigree> . " +
													"?result <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#PetOwner> . " +
												"}";
		
		assertThat(question.compile(), hasItem(expectedSparqlQueryString));
	}
	
	@Test
	public void testAsk() {
		Grammar grammar = new Grammar();
		grammar.addRule("<START>",
						Token.asTokens("<SPO_OPEN>", "?"),
						new QueryStatement("SPARQL",
										   SentenceType.OPEN_QUESTION,
										   "SELECT <<SPO_OPEN>>",
										   "WHERE { " +
										   			"<<SPO_OPEN>> . " +
										   		 "}"
						)
		);
		
		grammar.addRule("<SPO_OPEN>",
				Token.asTokens("<VInter>", "is", "<NI>"),
				new QueryStatement("",
								   SentenceType.OPEN_QUESTION,
								   "<<VInter>>",
								   "<<NI>> <<VInter>> <<VInter:1>>"
				)
		);

		grammar.addRule("<SPO_OPEN>",
				Token.asTokens("What", "<C>", "<V-s>", "<I>"),
				new QueryStatement("",
								   SentenceType.OPEN_QUESTION,
								   "<<C>>",
								   "<<C:1>> <<V-s>> <<I>> . " +
								   "<<C:1>> <<C>>"
				)
		);
		
		grammar.addRule("<C>",
				Token.asTokens("<NC-s>"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "<<NC-s>>",
								   "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <<NC-s>>"
				)
		);
		
		grammar.addRule("<I>",
				Token.asTokens("<NI>"),
				new QueryStatement("",
								   SentenceType.UNDEF,
								   "<<NI>>",
								   "<<NI>>"
				)
		);
		
		grammar.setModel(jenaModel);
		
		Question firstQuestion = new Question(grammar);
		firstQuestion.add("How")
					 .add("old")
					 .add("is")
					 .add("Doggy")
					 .add("?");
		Answer firstAnswer = firstQuestion.ask();
		
		assertThat(firstAnswer.getQuestionPhrase(), is("How old is Doggy ?"));
		Set<String> resultNodes = asStringSet(firstAnswer.getNodes());
		assertThat(resultNodes.size(), is(1));
		assertThat(resultNodes, hasItems("10"));
		
		Set<String> formalQueryStrings = firstAnswer.getFormalQueryStrings();
		assertThat(formalQueryStrings.size(), is(1));
		String expectedQueryString = "SELECT " +
										"?hasage " +
									 "WHERE { " +
									 	"<http://evolizer.org/ontologies/evolizer-nl/testing#doggy> <http://evolizer.org/ontologies/evolizer-nl/testing#hasAge> ?hasage . " +
									 "}";
		assertThat(formalQueryStrings, hasItem(expectedQueryString));
		
		
		Question secondQuestion = new Question(grammar);
		secondQuestion.add("What")
					  .add("Store")
					  .add("sells")
					  .add("Caesar")
					  .add("?");
		Answer secondAnswer = secondQuestion.ask();
		assertThat(secondAnswer.getQuestionPhrase(), is("What Store sells Caesar ?"));
		
		resultNodes = asStringSet(secondAnswer.getNodes());
		assertThat(resultNodes.size(), is(2));
		assertThat(resultNodes, hasItems("http://evolizer.org/ontologies/evolizer-nl/testing#qualipet", "http://evolizer.org/ontologies/evolizer-nl/testing#migros"));
		
		formalQueryStrings = secondAnswer.getFormalQueryStrings();
		assertThat(formalQueryStrings.size(), is(1));
		expectedQueryString = "SELECT " +
								"?supermarket " +
							  "WHERE { " +
								"?supermarket <http://evolizer.org/ontologies/evolizer-nl/testing#sellsPetFood> <http://evolizer.org/ontologies/evolizer-nl/testing#caesar> . ?supermarket <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/evolizer-nl/testing#SuperMarket> . " +
							  "}";
		assertThat(formalQueryStrings, hasItem(expectedQueryString));
	}

	private static Set<String> asStringSet(Set<QrItem> input) {
		Set<String> output = new HashSet<>();
		
		for(QrItem node : input) {
			if(node.isLiteral()) {
				output.add(node.asReadable());
			} else {
				output.add(node.getUri());
			}
		}
		
		return output;
	}
}


@SuppressWarnings("restriction")
class IsComplete extends TypeSafeMatcher<Question> {
	
	@Override
	public boolean matchesSafely(Question question) {
		return question.isComplete();
	}
	
	@Override
	public void describeTo(Description description) {
		description.appendText("complete");
	}
	
	@Factory
	public static <T> Matcher<Question> complete() {
		return new IsComplete();
	}
}
