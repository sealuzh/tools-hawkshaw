package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;

public class TestSession {
	private Grammar grammar;
	private static PersistentJenaModel baseModel;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		OntologyCore.waitForInit(null);
		baseModel = OntologyCore.createPersistentBaseModel("http://seal.ifi.uzh.ch/hawkshaw/test/question");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		baseModel.clear();
		baseModel.close();
		baseModel = null;
	}

	@Before
	public void setUp() throws Exception {
		grammar = new Grammar();
		/*
		 SPARQL <START> ::= <SPO_QOBJ> ? | SELECT <<SPO_QOBJ>> | WHERE { <<SPO_QOBJ>> . }
		 <SPO_QOBJ> ::= <INT_DET> {<class>} <V> <this> | ?result | ?result <<V>> <<this>> . ?result <<class>>
		 <this> ::= this | ?selection | <<selection>>
		 */
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.OPEN_QUESTION, "SELECT <<SPO_QOBJ>>", "WHERE { <<SPO_QOBJ>> . }"));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("What", "methods", "call", "<this>"), new QueryStatement("", SentenceType.UNDEF, "?result", "?result <http://evolizer.org/ontologies/seon/2009/06/java.owl#invokesMethod> <<this>> . ?result <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://evolizer.org/ontologies/seon/2009/06/java.owl#Method>"));
		
		grammar.setModel(baseModel);
	}

	@After
	public void tearDown() throws Exception {
		grammar = null;
	}
	
	@Test
	public void testSession() {
		fail("Not yet implemented");
	}
}
