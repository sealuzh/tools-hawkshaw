package business.entities;
import business.api.AbstractEmployee;
import business.api.IResponsible;


public class Manager extends AbstractEmployee implements IResponsible{
	private double bonus;
	private Assistant personalAssistant;
	
	public Manager(String name, double baseSalary, double bonus) {
		super(name, baseSalary);
		this.bonus = bonus;
	}
	
	public void setAssistant(Assistant personalAssistant) {
		this.personalAssistant = personalAssistant;
	}

	public Assistant getAssistant() {
		return personalAssistant;
	}
	
	@Override
	public double getSalary() {
		double totalSalary = getBaseSalary() + bonus;
		return totalSalary;
	}
	
	public void manage() {
		if(personalAssistant != null) {
			personalAssistant.work();
		}
	}

	public void charge() {
		bonus = 0;		
	}
		
	// used for testing local inner classes.
	public static Manager getCEO() {
		class CEO extends Manager {
			public CEO() {
				super("Ospel", 0, 0);
			}
			@Override
			public double getSalary() {
				return 100000000; 
			}
		}
		
		return new CEO();
	}
	
	// used for testing inner anonymous classes.
	public static Manager getCEO2() {
		Manager ceo = new Manager("Ospel", 0, 0) {		
			@Override
			public double getSalary() {
				return 100000000; 
			}
		};
		
		return ceo;
	}
}
