package ch.uzh.ifi.seal.hawkshaw.ontology.issue_history;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue-history
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;

public class IssueHistoryLinkerPlugin extends Plugin implements IHawkshawPlugin {
	// The plug-in ID
	public static final String PLUGIN_ID = "ch.uzh.ifi.seal.hawkshaw.issue-history";
	
	private static IssueHistoryLinkerPlugin plugin;

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		
		super.stop(context);
	}
	
	@Override
	public String getID() {
		return PLUGIN_ID;
	}
	
	public static IssueHistoryLinkerPlugin getDefault() {
		return plugin;
	}
}
