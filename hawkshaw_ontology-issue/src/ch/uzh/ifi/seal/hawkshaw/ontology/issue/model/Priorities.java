package ch.uzh.ifi.seal.hawkshaw.ontology.issue.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary.IHawkshawIssueOntology;

public enum Priorities {
	BLOCKER (IHawkshawIssueOntology.PRIORITY_BLOCKER,  IHawkshawIssueOntology.IS_BLOCKER),
	CRITICAL(IHawkshawIssueOntology.PRIORITY_CRITICAL, IHawkshawIssueOntology.IS_CRITICAL),
	MAJOR   (IHawkshawIssueOntology.PRIORITY_MAJOR,    IHawkshawIssueOntology.IS_MAJOR),
	MINOR   (IHawkshawIssueOntology.PRIORITY_MINOR,    IHawkshawIssueOntology.IS_MINOR),
	TRIVIAL (IHawkshawIssueOntology.PRIORITY_TRIVIAL,  IHawkshawIssueOntology.IS_TRIVIAL),
	UNKNOWN (IHawkshawIssueOntology.PRIORITY_UNKNOWN,  IHawkshawIssueOntology.IS_UNKNOWN);
	
	private String indiv;
	private String prop;

	private Priorities(String indiv, String prop) {
		this.indiv = indiv;
		this.prop = prop;
	}
	
	public String toIndividual() {
		return indiv;
	}

	public String toProperty() {
		return prop;
	}
	
	public static Priorities parse(String priority) {
		Priorities result;
		
		if(       "Blocker".equalsIgnoreCase(priority)) {
			result = BLOCKER;
		} else if("Critical".equalsIgnoreCase(priority)) {
			result = CRITICAL;
		} else if("Major".equalsIgnoreCase(priority)) {
			result = MAJOR;
		} else if("Minor".equalsIgnoreCase(priority)) {
			result = MINOR;
		} else if("Trivial".equalsIgnoreCase(priority)) {
			result = TRIVIAL;
		} else { // if we don't recognize a specific category, we just use #priority-unknown instead
			System.err.println("Warning: Unknown priority: " + priority); // TODO logging
			result = UNKNOWN;
		}
		
		return result;
	}
}
