package ch.uzh.ifi.seal.hawkshaw.ontology.issue.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary.IHawkshawIssueOntology;

public enum IssueTypes {
	BUG(IHawkshawIssueOntology.BUG),
	IMPROVEMENT(IHawkshawIssueOntology.IMPROVEMENT),
	NEW_FEATURE(IHawkshawIssueOntology.NEW_FEATURE),
	QUESTION(IHawkshawIssueOntology.QUESTION),
	TASK(IHawkshawIssueOntology.TASK),
	TEST(IHawkshawIssueOntology.TEST),
	WISH(IHawkshawIssueOntology.WISH),
	UNKNOWN(IHawkshawIssueOntology.ISSUE);
	
	private String cls;

	private IssueTypes(String cls) {
		this.cls = cls;
	}
	
	public static IssueTypes parse(String type) {
		IssueTypes cls;
		
		if(       "Bug".equalsIgnoreCase(type)) {
			cls = BUG;
		} else if("Improvement".equalsIgnoreCase(type)) {
			cls = IMPROVEMENT;
		} else if("New Feature".equalsIgnoreCase(type)) {
			cls = NEW_FEATURE;
		} else if("Question".equalsIgnoreCase(type)) {
			cls = QUESTION;
		} else if("Task".equalsIgnoreCase(type)) {
			cls = TASK;
		} else if("Test".equalsIgnoreCase(type)) {
			cls = TEST;
		} else if("Wish".equalsIgnoreCase(type)) {
			cls = WISH;
		} else { // if we don't recognize a specific category, we just use #Issue instead
			if(!"unknown".equals(type)) {
				System.err.println("Warning: Unknown issue type: " + type); // TODO logging
			}
			cls = UNKNOWN;
		}
		
		return cls;
	}
	
	public String toOntClass() {
		return cls;
	}
}
