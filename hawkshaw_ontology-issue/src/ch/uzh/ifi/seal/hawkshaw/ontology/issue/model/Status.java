package ch.uzh.ifi.seal.hawkshaw.ontology.issue.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary.IHawkshawIssueOntology;

public enum Status {
	CLOSED(IHawkshawIssueOntology.STATUS_CLOSED),
	OPEN(IHawkshawIssueOntology.STATUS_OPEN),
	IN_PROGRESS(IHawkshawIssueOntology.STATUS_IN_PROGRESS),
	REOPENED(IHawkshawIssueOntology.STATUS_REOPENED),
	RESOLVED(IHawkshawIssueOntology.STATUS_RESOLVED),
	UNKNOWN(IHawkshawIssueOntology.STATUS_UNKNOWN);
	
	private String indiv;

	private Status(String indiv) {
		this.indiv = indiv;
	}
	
	public static Status parse(String status) {
		Status individual;
		
		if(       "Closed".equalsIgnoreCase(status)) {
			individual = CLOSED;
		} else if("Open".equalsIgnoreCase(status)) {
			individual = OPEN;
		} else if("In Progress".equalsIgnoreCase(status)) {
			individual = IN_PROGRESS;
		} else if("Reopened".equalsIgnoreCase(status)) {
			individual = REOPENED;
		} else if("Resolved".equalsIgnoreCase(status)) {
			individual = RESOLVED;
		} else { // if we don't recognize a specific category, we just use #status-unknown instead
			System.err.println("Warning: Unknown status: " + status); // TODO logging
			individual = UNKNOWN;
		}
		
		return individual;
	}
	
	public String toIndividual() {
		return indiv;
	}
}
