package ch.uzh.ifi.seal.hawkshaw.ontology.issue.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.URI;

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary.IHawkshawIssueOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractHawkshawModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IHawkshawOntology;
import ch.uzh.ifi.seal.hawkshaw.support.misc.WordUtil;

import com.hp.hpl.jena.ontology.Individual;

public class IssueOntModel extends AbstractHawkshawModel {

	public static final String UNKNOWN_ISSUE_TYPE = "unknown";

	public IssueOntModel(PersistentJenaModel baseModel) {
		super(baseModel);
		init();
	}

	@Override
	public void clear() {
		getBaseModel().clear();
		init();
	}

	public IssueHandle getIssueHandlerFor(URI uri, String key, String type){
		Individual individual = fetchIndividual(uri.toString(), key, IssueTypes.parse(type).toOntClass());
		return new IssueHandle(individual, key, this);
	}

	public CommentHandle getCommentHandle(URI uri) {
		String uriString = uri.toString();
		Individual individual = fetchIndividual(uriString, "Comment " + uriString.substring(uriString.lastIndexOf('/') + 1), IHawkshawIssueOntology.COMMENT);
		return new CommentHandle(individual, this);
	}

	public StakeholderHandle getStakeholderHandle(String acc, String name) {
		Individual individual = fetchIndividual(uriFromName(acc), name, IHawkshawOntology.STAKEHOLDER);
		return new StakeholderHandle(individual, this);
	}

	public AssigneeHandle getAssigneeHandle(String acc, String name) {
		Individual individual = fetchIndividual(uriFromName(acc), name, IHawkshawIssueOntology.ASSIGNEE);
		return new AssigneeHandle(individual, this);
	}
	
	public ReporterHandle getReporterHandle(String acc, String name) {
		Individual individual = fetchIndividual(uriFromName(acc), name, IHawkshawIssueOntology.REPORTER);
		return new ReporterHandle(individual, this);
	}
	
	private void init() {
			getBaseModel().readOntologies(
				"http://se-on.org/ontologies/hawkshaw/2012/02/issues.owl",
				"http://se-on.org/ontologies/hawkshaw/2012/02/issues-nl.owl"
			);
	}

	private String uriFromName(String name) {
		return UriUtil.encode(WordUtil.convertNonAscii(name.toLowerCase()));
	}
	
	public static String getIssueLinkProperty(String desc) {
		return IHawkshawIssueOntology.BASE + '#'+ WordUtil.toCamelCase(desc);
	}
}
