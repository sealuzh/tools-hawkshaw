package ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface IHawkshawIssueOntology {
	String BASE              = "http://se-on.org/ontologies/hawkshaw/2012/02/issues.owl";
	String LOCAL             = "local_hawkshaw_issues_12_02";
	
	// Classes
	String COMMENT           = BASE + "#Comment";
	
	String ISSUE             = BASE + "#Issue";
	String BUG               = BASE + "#Bug";
	String IMPROVEMENT       = BASE + "#Improvement";
	String NEW_FEATURE       = BASE + "#New_Feature";
	String QUESTION          = BASE + "#Question";
	String TASK              = BASE + "#Task";
	String TEST              = BASE + "#Test";
	String WISH              = BASE + "#Wish";
	
	String PRIORITY          = BASE + "#Priority";
	
	String ASSIGNEE          = BASE + "#Assignee";
	String REPORTER          = BASE + "#Reporter";
	
	String STATUS            = BASE + "#Status";
	
	
	// Object properties
	String HAS_PRIORITY      = BASE + "#hasPriority";
	String HAS_STATUS        = BASE + "#hasStatus";

	String COMMENTS_ON       = BASE + "#commentsOn";
	String HAS_AUTHOR        = BASE + "#hasAuthor";
	
	String HAS_COMMENT       = BASE + "#hasComment";
	String IS_COMMENT_OF     = BASE + "#isCommentOf";
	
	String HAS_ASSIGNEE      = BASE + "#hasAssignee";
	String IS_ASSIGNEE_OF    = BASE + "#isAssigneeOf";

	String REPORTS_ISSUE     = BASE + "#reportsIssue";
	String IS_REPORTED_BY    = BASE + "#isReportedBy";
	
	
	// Datatype properties
	String CREATED_ON        = BASE + "#createdOn";
	String HAS_DESCRIPTION   = BASE + "#hasDescription";
	String HAS_KEY           = BASE + "#hasKey";
	String IS_RESOLVED       = BASE + "#isResolved";
	String IS_OPEN           = BASE + "#isOpen";
	String HAS_COMMENT_TEXT  = BASE + "#hasCommentText";
	String FIX_TIME          = BASE + "#fixTime";

	String IS_BLOCKER        = BASE + "#isBlocker";
	String IS_CRITICAL       = BASE + "#isCritical";
	String IS_MAJOR          = BASE + "#isMajor";
	String IS_MINOR          = BASE + "#isMinor";
	String IS_TRIVIAL        = BASE + "#isTrivial";
	String IS_UNKNOWN        = BASE + "#unknown-prop"; // should never be added to ont
	
	// Individuals
	String PRIORITY_BLOCKER  = BASE + "#blocker";
	String PRIORITY_CRITICAL = BASE + "#critical";
	String PRIORITY_MAJOR    = BASE + "#major";
	String PRIORITY_MINOR    = BASE + "#minor";
	String PRIORITY_TRIVIAL  = BASE + "#trivial";
	String PRIORITY_UNKNOWN  = BASE + "#unknown-priority"; // should never be added to ont
	
	String STATUS_CLOSED      = BASE + "#closed";
	String STATUS_OPEN        = BASE + "#open";
	String STATUS_IN_PROGRESS = BASE + "#in-progress";
	String STATUS_REOPENED    = BASE + "#reopened";
	String STATUS_RESOLVED    = BASE + "#resolved";
	String STATUS_UNKNOWN     = BASE + "#status-unknown";
}
