package ch.uzh.ifi.seal.hawkshaw.ontology.issue.importer;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.joda.time.DateTime;
import org.joda.time.Days;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.IssueOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.model.CommentHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.model.IssueHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.model.IssueOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.model.StakeholderHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

import com.atlassian.jira.rest.client.IssueRestClient;
import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.RestClientException;
import com.atlassian.jira.rest.client.auth.AnonymousAuthenticationHandler;
import com.atlassian.jira.rest.client.domain.BasicResolution;
import com.atlassian.jira.rest.client.domain.BasicUser;
import com.atlassian.jira.rest.client.domain.Comment;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.IssueLink;
import com.atlassian.jira.rest.client.domain.IssueLinkType;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;

public class JiraImporterJob extends Job {
	public static final Object ISSUE_JOB_FAMILY = new Object();
	
	private IProject project;
	
	private String   trackerUrl;
	private String   prefix ="IVY-";
	
	private int      maxErrors = 10;
	private int      sleepAfter = 50;
	private int      issueCounter;
	
	private long     sleepMillis = 5000;
	private long     duration;
	
	private IssueOntModel model;
	
	private IssueRestClient issueClient;
	
	private Issue currentIssue;


	// TODO cancel status
	public JiraImporterJob(IssueImporterConfig config) {
		super("Importing Issues from Jira");
		
		this.project = config.getProject();
		this.trackerUrl = config.getTrackerUrl();
		this.prefix = config.getIssueKey();
		this.maxErrors = config.getMaxErrors();
		this.sleepAfter = config.getSleepAfter();
		this.sleepMillis = config.getSleepMillis();
		
		this.issueCounter = 0;
		this.duration = 0;
		
		setUser(true);
	}

	@Override
	public boolean belongsTo(Object family) {
		return ISSUE_JOB_FAMILY.equals(family);
	}

	public String getProjectName() {
		return project.getName();
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		monitor.beginTask("Importing Issues from Jira", IProgressMonitor.UNKNOWN);
		
		initModel();
		initJiraRestClient();
		
		long start = System.currentTimeMillis();
		IStatus status = processRepository(monitor);
		long end = System.currentTimeMillis();
		
		duration = end - start;
		
		return status;
	}

	private IStatus processRepository(IProgressMonitor monitor) {
		IStatus result = Status.OK_STATUS;
		
		int currentErrors = 0;
		
		int issueNumber = 1;
		
		while(currentErrors < maxErrors ) {
			if(monitor.isCanceled()) {
				result = Status.CANCEL_STATUS;
				break;
			}
			
			String key = prefix + issueNumber;
			
			monitor.setTaskName("Importing " + key + " (" + (maxErrors - currentErrors) + " tries left)");
			
			try {
				initCurrentIssue(key);
				
				IssueHandle issueHandle = getHandleForCurrentIssue();
				setUrl(issueHandle);
				setKeyOf(issueHandle);
				setPriorityOf(issueHandle);
				addDescriptionTo(issueHandle);
				setCreationDateOf(issueHandle);
				setFixTime(issueHandle);
				setStatusOf(issueHandle);
				setResolutionOf(issueHandle);
				
				addAssigneeTo(issueHandle);
				addReporterTo(issueHandle);
				addCommentsTo(issueHandle);
				
				linkIssuesTo(issueHandle);
				
				currentErrors = 0;
				issueCounter++; // total imported issues without errors
			} catch(RestClientException rcex) {
				currentErrors++;
			} finally {
				clearCurrentIssue();
			}
			
			issueNumber++; // total requested issues, including errors
			
			if(issueNumber % sleepAfter == 0) {
				try {
					monitor.setTaskName("Pausing for " + sleepMillis + " ms to let the server rest a bit");
					Thread.sleep(sleepMillis);
				} catch (InterruptedException e) {
					// just continue import
				}
			}
		}
		
		return result;
	}

	private void setUrl(IssueHandle issueHandle) {
		issueHandle.setUrl(trackerUrl + "/browse/" + currentIssue.getKey());
	}

	private void linkIssuesTo(IssueHandle issueHandle) {
		for(IssueLink link : currentIssue.getIssueLinks()) {
			IssueLinkType linkType = link.getIssueLinkType();
			
			IssueHandle targetIssueHandle = model.getIssueHandlerFor(link.getTargetIssueUri(), link.getTargetIssueKey(), IssueOntModel.UNKNOWN_ISSUE_TYPE);
			
			issueHandle.addLink(targetIssueHandle, linkType.getDescription());
		}
	}

	private void setFixTime(IssueHandle issueHandle) {
		BasicResolution r = currentIssue.getResolution();
		if(r != null) {
			DateTime created = currentIssue.getCreationDate();
			DateTime lastUpdated = currentIssue.getUpdateDate(); // TODO heuristic: last update == resolved -> does not hold, needs to be fixed as soon as REST API exposes this information
			
			Days diff = Days.daysBetween(created, lastUpdated);
			int days = diff.getDays();
			
			issueHandle.setFixTime(days);
		}
	}

	private void setKeyOf(IssueHandle issueHandle) {
		issueHandle.setKey(currentIssue.getKey());
	}

	private IssueHandle getHandleForCurrentIssue() {
		IssueHandle issueHandle = model.getIssueHandlerFor(currentIssue.getSelf(), currentIssue.getKey(), currentIssue.getIssueType().getName());
		return issueHandle;
	}

	private void clearCurrentIssue() {
		currentIssue = null;
	}

	private void initCurrentIssue(String key) {
		currentIssue = issueClient.getIssue(key, new NullProgressMonitor());
	}

	private void setStatusOf(IssueHandle issueHandle) {
		issueHandle.setStatus(currentIssue.getStatus().getName());
	}

	private void setPriorityOf(IssueHandle issueHandle) {
		issueHandle.setPriority(currentIssue.getPriority().getName());
	}

	private void addDescriptionTo(IssueHandle issueHandle) {
		String description = currentIssue.getDescription();
		
		if(description == null || description.length() < 1) {
			description = "no description given";
		} 
		
		issueHandle.setDescription(description);
	}

	private void setCreationDateOf(IssueHandle issueHandle) {
		issueHandle.setCreationDate(currentIssue.getCreationDate().toGregorianCalendar());
	}

	private void setResolutionOf(IssueHandle issueHandle) {
		BasicResolution r = currentIssue.getResolution();
		if(r != null) {
			issueHandle.setResolved();
		} else {
			issueHandle.setOpen();
		}
	}

	private void addReporterTo(IssueHandle issueHandle) {
		BasicUser reporter = currentIssue.getReporter();
		issueHandle.addReporter(reporter.getName(), reporter.getDisplayName());
	}

	private void addAssigneeTo(IssueHandle issueHandle) {
		BasicUser assignee = currentIssue.getAssignee();
		if(assignee != null) {
			issueHandle.addAssignee(assignee.getName(), assignee.getDisplayName());
		}
	}

	private void addCommentsTo(IssueHandle issueHandle) {
		for(Comment comment : currentIssue.getComments()) {
			CommentHandle commentHandle = issueHandle.addComment(comment.getSelf(), comment.getBody());
			commentHandle.setCreationDate(comment.getCreationDate().toGregorianCalendar());
			
			BasicUser author = comment.getAuthor();
			StakeholderHandle authorHandle = model.getStakeholderHandle(author.getName(), author.getDisplayName());
			
			commentHandle.setAuthor(authorHandle);
		}
	}

	private void initJiraRestClient() {
		try {
			URI jiraServerUri = new URI(trackerUrl);
			JerseyJiraRestClientFactory factory = new JerseyJiraRestClientFactory();
	
			JiraRestClient restClient = factory.create(jiraServerUri, new AnonymousAuthenticationHandler());
			issueClient = restClient.getIssueClient();
		} catch(URISyntaxException usex) {
			throw new HawkshawException(IssueOntologyCore.getDefault(), "Jira repository URI is invalid", usex);
		}
	}

	private void initModel() {
		OntologyCore.waitForInit(null);
		PersistentJenaModel base = OntologyCore.fetchPersistentModelFor(project);
		model = new IssueOntModel(base);
	}

	public String getDurationString() {
		return String.format("%d min, %d sec", 
				 TimeUnit.MILLISECONDS.toMinutes(duration),
				 TimeUnit.MILLISECONDS.toSeconds(duration) - 
				 TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
		);
	}

	public int getNumberOfImportedIssues() {
		return issueCounter;
	}
}
