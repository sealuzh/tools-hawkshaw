package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author wuersch
 *
 */
public class Token {
	private String value;
	private boolean isOptional;
	private boolean isSubject;
	
	/**
	 * 
	 * @param value
	 * @param isOptional
	 */
	public Token(String value) {
		this.value = value;
	}
	
	public void markAsOptional() {
		isOptional = true;
	}
	
	public void markAsSubject() {
		isSubject = true;
	}

	/**
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isOptional() {
		return isOptional;
	}
	
	public boolean isSubject() {
		return isSubject;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isOptional ? 1231 : 1237);
		result = prime * result + (isSubject ? 1231 : 1237);
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Token other = (Token) obj;
		if (isOptional != other.isOptional) {
			return false;
		}
		
		if (isSubject != other.isSubject) {
			return false;
		}
		
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		
		return true;
	}

	/**
	 * For debugging purpose only.
	 */
	@Override
	public String toString() {
		return isOptional ? "[" + value + "]" : value;
	}
	
	/**
	 * This method tokenizes the natural language part of a rule. Each
	 * token/word can be optional, if it is enclosed by square brackets.
	 * 
	 * The method returns a FIFO queue, in particular a {@link LinkedList}.
	 * 
	 * @param statement
	 *            the {@link String} that should be tokenized.
	 * @return a {@link Queue} of tokens.
	 * 
	 * @see {@link Grammar#addRule(String, LinkedList, QueryStatement)}.
	 * @see {@links #asTokens(String...)}.
	 */
	public static Queue<Token> tokenize(String statement) {
		return asTokens(statement.split("\\s++"));
	}
	
	public static Queue<Token> asTokens(String... words) {
		Queue<Token> result = new LinkedList<>();

		for(String word : words) {
			Token token;
			
			if(word.startsWith("{")) { // subject of the sentence, e.g., "What {person} knows..."
				token = new Token(word.substring(1,word.length() - 1));
				token.markAsSubject();
			} else if(word.startsWith("[")) { // optional words, e.g., "Give me all [the] instances of..."
				token = new Token(word.substring(1,word.length() - 1));
				token.markAsOptional();
			} else {
				token = new Token(word);
			}
			
			result.add(token);
		}
		
		return result;
	}
}
