package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashSet;
import java.util.Set;

/**
 * A chaining restriction assures that only meaningful verbs can follow a given
 * subject and only meaningful objects can follow a given verb, respectively.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 * 
 */
public class ChainingRestriction {
	public static final ChainingRestriction EMPTY = new EmptyChainingRestriction();
	
	private Set<String> domain;
	private Set<String> range;
	
	public ChainingRestriction() {
		this.domain = new HashSet<>();
		this.range  = new HashSet<>();
	}
	
	public Set<String> getDomain() {
		return new HashSet<>(domain);
	}
	
	public Set<String> getRange() {
		return new HashSet<>(range);
	}

	public void addDomain(String domain) {
		this.domain.add(domain);
	}
	
	public void addDomain(Set<String> domain) {
		this.domain.addAll(domain);
	}
	
	public void addRange(String range) {
		this.range.add(range);
	}

	public void addRange(Set<String> range) {
		this.range.addAll(range);		
	}
	
	private static final class EmptyChainingRestriction extends ChainingRestriction {
		@Override
		public void addDomain(String domain) {
			throw new UnsupportedOperationException("Cannot add domain to empty restriction");
		}

		@Override
		public void addDomain(Set<String> domain) {
			throw new UnsupportedOperationException("Cannot add domain to empty restriction");
		}
		
		@Override
		public void addRange(String range) {
			throw new UnsupportedOperationException("Cannot add range to empty restriction");
		}

		@Override
		public void addRange(Set<String> range) {
			throw new UnsupportedOperationException("Cannot add range to empty restriction");
		}
	}
}
