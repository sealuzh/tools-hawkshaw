package ch.uzh.ifi.seal.hawkshaw.core.exceptions;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

public class RuleLoaderException extends HawkshawException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1831774378518326425L;
	private Grammar grammar;
	private String grammarLocation;

	public RuleLoaderException(String message, Grammar grammar, String grammarLocation, Throwable cause) {
		super(QueryCore.getDefault(), message, cause);
		this.grammar = grammar;
		this.grammarLocation = grammarLocation;
	}
	
	public RuleLoaderException(String message, Grammar grammar, String grammarLocation) {
		super(QueryCore.getDefault(), message);
		this.grammar = grammar;
		this.grammarLocation = grammarLocation;
	}

	public Grammar getGrammar() {
		return grammar;
	}

	public String getGrammarLocation() {
		return grammarLocation;
	}

}
