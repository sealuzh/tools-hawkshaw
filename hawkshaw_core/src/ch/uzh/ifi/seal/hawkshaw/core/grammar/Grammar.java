package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Queue;
import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.AbstractSymbol;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.SymbolContainer;
import ch.uzh.ifi.seal.hawkshaw.core.ontology.ModelAdapter;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;

/**
 * Instances of this class store all the grammar rules needed to compose a
 * natural language query.
 * 
 * New rules can be added using the
 * {@link #addRule(String, Queue, QueryStatement)} method. The words that are
 * allowed to start a new question can be retrieved by calling
 * {@link #startWords()}.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 * 
 */
public class Grammar {
	/**
	 * Contains the left-hand-sided symbols.
	 */
	private SymbolContainer rules;

	/**
	 * TODO Where should this really go?
	 */
	private ModelAdapter model;

	private static final String ROOT_SYMBOL_LABEL = "<START>";

	/**
	 * Default constructor. Initializes a new {@link SymbolContainer}.
	 */
	public Grammar() {
		rules = new SymbolContainer();
	}

	/**
	 * Checks whether the grammar is ready for use. This is the case whenever
	 * there are some rules and the model is available. Does not check whether
	 * the rules are complete and valid, though.
	 * 
	 * @return true if the grammar can be used, false otherwise.
	 */
	public boolean ready() {
		return !rules.isEmpty() && model != null;
	}

	/**
	 * Processes a grammar rule and stores it.
	 * 
	 * @param ruleLabel
	 *            the left-hand-sided symbol.
	 * @param tokens
	 *            the natural language part.
	 * @param queryStatement
	 *            the formal language part.
	 */
	public void addRule(String ruleLabel, Queue<Token> tokens,
			QueryStatement queryStatement) {
		// TODO basic syntax checking - where?
		AbstractSymbol rule = rules.fetchOrCreateSymbol(ruleLabel, this);
		queryStatement.setRule(rule);
		rule.addNextSymbolFrom(tokens, queryStatement);
	}

	public void setExternal(String key, String name, String value) {
		String word = key.substring(1, key.length() - 1) + " (" + name + ')';
		QueryStatement queryStatement = new QueryStatement(null,
				SentenceType.UNDEF, '?' + word, '<' + value + '>');

		addRule(key, Token.asTokens(word), queryStatement);
	}

	public void unsetExternal(String key) {
		rules.removeSymbol(key); // TODO rework: grammar shouldn't know about
									// selections, etc. -> goes into session,
									// but symbols should be filterable for
									// domain/range.
	}

	/**
	 * Returns a rule for the given label, or, if no such rule exists, it will
	 * create, store, and return a new one.
	 * 
	 * @param label
	 *            the label of the rule.
	 * @return either an existing rule with the given label or a freshly created
	 *         one, if no such rule was known before.
	 */
	public AbstractSymbol getRuleFor(String label) {
		return rules.fetchOrCreateSymbol(label, this);
	}

	/**
	 * Checks whether there is already a rule with the given label. Mainly used
	 * for testing purposes.
	 * 
	 * @param label
	 *            the label of the rule.
	 * @return <code>true</code> if the rule exists, <code>false</code>
	 *         otherwise.
	 */
	public boolean hasRule(String label) {
		return rules.containsSymbol(label);
	}

	/**
	 * Returns the words that can start a new question.
	 */
	public Set<Proposal> startWords() {
		AbstractSymbol startRule = getRuleFor(ROOT_SYMBOL_LABEL);
		return startRule.nextProposals();
	}

	/**
	 * Sets an ontology model. The model is then wrapped in a
	 * {@link ModelAdapter} instance.
	 * 
	 * @param ontModel
	 *            the Jena ontology model.
	 */
	public void setModel(PersistentJenaModel ontModel) {
		if (model == null) {
			model = new ModelAdapter(ontModel);
		} else {
			model.setOntModel(ontModel);
		}
	}

	/**
	 * Returns the model wrapper.
	 */
	public ModelAdapter getModel() {
		return model;
	}
}
