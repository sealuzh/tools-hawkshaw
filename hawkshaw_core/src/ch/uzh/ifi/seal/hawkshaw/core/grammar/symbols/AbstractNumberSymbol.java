package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;

/**
 * Common superclass of symbols that have a grammatical number.
 * 
 * @author Michael
 * 
 */
public abstract class AbstractNumberSymbol extends AbstractSymbol {
	private boolean singular;

	/**
	 * Constructor.
	 * 
	 * @param label
	 *            the symbol's label
	 * @param singular
	 *            <code>true</code> if, the symbol is singular,
	 *            <code>false</code> else.
	 * @param grammar
	 *            the grammar context.
	 */
	public AbstractNumberSymbol(String label, boolean singular, Grammar grammar) {
		super(label, grammar);
		this.singular = singular;
	}

	/**
	 * Tests whether this symbol is singular or plural.
	 * 
	 * @return <code>true</code> if, the symbol is singular, <code>false</code>
	 *         else.
	 */
	public boolean isSingular() {
		return singular;
	}
}
