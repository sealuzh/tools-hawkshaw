package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Stack;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.AbstractSymbol;

/**
 * A proposal stores a single word then shown to the user to provide guidance
 * during query composition.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 * 
 */
public class Proposal {
	private AbstractSymbol symbol;
	
	private Stack<AbstractSymbol> upperSymbols;
	private Stack<Direction> direction;
	
	public Proposal(AbstractSymbol symbol) {
		this.symbol = symbol;
		upperSymbols = new Stack<>();
		direction = new Stack<>();
	}
	
	public AbstractSymbol getSymbol() {
		return symbol;
	}

	public String getLabel() {
		return symbol.getLabel();
	}
	
	public Direction direction() {
		return direction.pop();
	}

	public void addDirectionInformation(Direction dir) {
		direction.push(dir);
	}

	public void addUpperSymbol(AbstractSymbol symbol) {
		upperSymbols.push(symbol);	
	}

	public Stack<AbstractSymbol> getUpperSymbols() {
		return upperSymbols;
	}

	@Override
	public String toString() {
		return "Proposal [" + symbol.toString() + "]";
	}
}
