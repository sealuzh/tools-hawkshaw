package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * Instances of these class provide status information about a question.
 * 
 * @author wuersch
 * 
 */
public class QuestionStatus {
	private Question question;
	
	protected QuestionStatus(Question question) {
		this.question = question;
	}
	
	public boolean questionIsComplete() {
		return question.isComplete();
	}
	
	public int getWordCount() {
		return question.getWordCount();
	}
	
	public int getNrOfActivePaths() {
		return question.getNrOfActivePaths();
	}
	
	public int getNrOfResolvedPaths() {
		return question.getNrOfResolvedPaths();
	}
	
	public String getQuestionString() {
		return question.toString();
	}
}
