package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.osgi.framework.Bundle;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.support.io.ResourceLocator;

public class GrammarInitializer extends Job {
	private Grammar grammar;

	public GrammarInitializer() {
		super("Initializing Default Grammar");
		setSystem(true);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor);
		progress.beginTask("Initializing Default Grammar.", 10000);
		
		Thread init = new Initializer();
		init.start();
		
		while(init.isAlive()) {
			progress.setWorkRemaining(10000);
			progress.worked(1);
		}
		
		return Status.OK_STATUS;
	}
	
	@Override
	public boolean belongsTo(Object family) {
		return family.equals(QueryCore.FAMILY_INIT);
	}
	
	public Grammar getGrammar() {
		return grammar;
	}
	
	private class Initializer extends Thread {
		@Override
		public void run() {
			grammar = new Grammar();
			
			RuleLoader rl = new RuleLoader(grammar);
			
			for(URL url : collectRegisteredGrammarFileURLs()) {
				rl.loadGrammarDescriptionFile(url);
			}
		}
		
		private List<URL> collectRegisteredGrammarFileURLs() throws HawkshawException {
			try {
				IConfigurationElement[] config = Platform.getExtensionRegistry()
														 .getConfigurationElementsFor(QueryCore.EXTENSION_ID_GRAMMAR);
				
				List<URL> urls = new LinkedList<>();
				
				for(IConfigurationElement configElement : config) {
					String contributorName = configElement.getContributor().getName();
					String grammarFilePath = configElement.getAttribute("file");
					Bundle bundle = Platform.getBundle(contributorName);
					URL url = ResourceLocator.resolveUrl(bundle, grammarFilePath);
					urls.add(FileLocator.toFileURL(url));
				}
				
				return urls;
			} catch (IOException ex) {
				throw new HawkshawException(QueryCore.getDefault(), "Error while collecting registered grammar files.", ex);
			}
		}
	}
}
