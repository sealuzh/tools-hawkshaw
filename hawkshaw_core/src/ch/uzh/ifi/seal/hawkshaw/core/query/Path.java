package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;

/**
 * A path stores each element (word) of a {@link Question}.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 *
 */
public class Path {
	private Grammar grammar;
	private Element firstElement;
	
	private Set<Proposal> currentProposals;
	
	private int createdAt = -10;
	
	/**
	 * Constructor.
	 * 
	 * @param context
	 */
	public Path(Grammar grammar) {
		this.grammar = grammar;
	}
	
	/**
	 * Copy constructor. Creates also a deep copy of the chain of elements
	 * rooted at {@link #firstElement}. Only the initial path needs access to
	 * the grammar context, so the context is not set for any paths created
	 * later.
	 * 
	 * @param otherPath
	 *            the path to copy.
	 * @throws NullPointerException
	 *             if <code>otherPath</code> is <code>null</code>.
	 */
	private Path(Path otherPath) {
		if(otherPath.firstElement != null) {
			firstElement = otherPath.firstElement.duplicate();
			// small performance tweak - no need to calculate the proposals twice:
			currentProposals = otherPath.currentProposals;
		}
	}
	
	/**
	 * Creates a deep copy of the current path.
	 * 
	 * @return a deep copy of the current path.
	 */
	protected Path duplicate() {
		return new Path(this);
	}

	/**
	 * Returns the next words that can be added to the current path. The corresponding proposals
	 * are further cached to speed up the process of adding the next word later.
	 * 
	 * @return the next words for this path.
	 */
	public Set<String> nextWords() {
		Set<String> result = new TreeSet<>();
				
		for(Proposal proposal : proposalCache()) {
			result.add(proposal.getLabel());
		}
		
		return result;
	}
	
	public String phrase() {
		if(firstElement != null) {
			return firstElement.phrase().trim();
		} else {
			return "";
		}
	}

	/**
	 * Tries to add a word to the current path. If there are several
	 * possibilities to do that (e.g., if multiple 'lower' grammar rules
	 * starting with the same word are applicable), this will result in copying
	 * the current path.
	 * 
	 * If a word can not be added to the current path, this does not necessarily
	 * mean that the word could not be added to question itself. If appending
	 * the word was successful for other active paths, this simply means that
	 * the current path will be suspended.
	 * 
	 * @param word
	 *            the word to add.
	 * @return <code>null</code> if <code>word</code> could not be added, an
	 *         empty set if there was only one possibility to add
	 *         <code>word</code> to the current path, and a set of branches of
	 *         the current path if there are several possibilities to add the
	 *         <code>word</code>.
	 */
	public Set<Path> add(String word) { // TODO Caching proposals: where? question or here?
		Set<Proposal> matches = findMatches(word);
		
		discardProposalCache();
		
		switch(matches.size()) {
		case 0:
			// word can not be added to this path
			return null;
		case 1:
			// there is exactly one possibility to add the word to the current path
			add(matches.iterator().next());
			return Collections.emptySet();
		default:					
			// there is more than one possibility to add the word to the current path
			return spawnPathsFor(matches);
		}
	}

	/**
	 * Removes the last element from the current path. This is done for each
	 * active path when the last word of a question is removed.
	 */
	public void removeLastElement() {
		if(firstElement == null) {
			// path contains no words, nothing to remove.
			return;
		}
		
		if(firstElement.isLastElement()) {
			// path contains only one word
			firstElement = null;
		} else {
			firstElement.removeLastElement();
		}
		
		discardProposalCache();
	}

	/**
	 * Returns whether this path forms a valid question.
	 * 
	 * @return <code>true</code> if the current path is complete,
	 *         <code>false</code> otherwise.
	 */
	public boolean isComplete() {
		return firstElement != null && firstElement.isComplete();
	}

	public Set<QueryStatement> compile() {
		Set<QueryStatement> result = new HashSet<>();		
		
		if(firstElement != null) {
			for(QueryStatement stmt : firstElement.mergeQueryStatements()) {
				result.add(stmt);
			}
		}
		
		return result;
	}

	/**
	 * Checks whether this path was created at a given number of words of the
	 * question.
	 * 
	 * @param wordCount
	 *            the number of words of the question that this path belongs to.
	 * @return <code>true</code> if the current path was created at the given
	 *         number of words, <code>false</code> otherwise.
	 */
	public boolean wasCreatedAt(int wordCount) {
		return this.createdAt == wordCount;
	}

	/**
	 * Sets the number of words of the question where the current path was
	 * spawned. This information is necessary to discard invalid paths once
	 * a word was removed from a question.
	 * 
	 * @param wordCount
	 *            the number of words where this path was spawned.
	 */
	public void setCreatedAt(int wordCount) {
		this.createdAt = wordCount;
	}

	/**
	 * Retrieves and caches the next proposals for the current path. If they are
	 * already known, the cache is simply returned.
	 * 
	 * @return a proposal of words that can be added to the current path.
	 */
	private Set<Proposal> proposalCache() {
		if (currentProposals == null) {
			// the initial proposals are provided by the grammar context.
			currentProposals = firstElement == null ? grammar.startWords()
													: firstElement.nextProposals();
		}
		return currentProposals;
	}

	/**
	 * Finds and returns all the proposals that match the given word. If the
	 * word can not be added to the current path, the result will be an empty
	 * set. If there is exactly one possibility to add the word (no branching
	 * necessary), the result contains exactly one proposal. If there are
	 * multiple possibilities to add the word to the current path, the result
	 * will contain multiple proposals. This will result in branching of the
	 * current path, triggered by the caller of this method, namely
	 * {@link #add(String)}.
	 * 
	 * @param word
	 *            the word that should be added to the current path.
	 * @return a set of proposals that match the given word.
	 */
	private Set<Proposal> findMatches(String word) {
		Set<Proposal> matches = new HashSet<>();
		for(Proposal proposal : proposalCache()) {
			if(proposal.getLabel().equals(word)) {
				matches.add(proposal);
			}
		}
		return matches;
	}
	
	/**
	 * Clears the <code>currentProposals</code>.
	 */
	private void discardProposalCache() {
		if(currentProposals != null) {
			currentProposals.clear();
			currentProposals = null;
		}
	}

	/**
	 * If there is more than one matching {@link Proposal}, this method creates
	 * one or several copies of the current path. One match is then added to
	 * each of the resulting paths (including the current one).
	 * 
	 * If there is exactly one matching proposal, it is simply added to the
	 * current path.
	 * 
	 * @param matches
	 *            a set of proposals, of which each could be added to the
	 *            current path. If multiple proposals exist, this will result in
	 *            spawning copies of the current path. <code>matches</code> must at least contain
	 *            the current path to preserve the semantics of the returned set.
	 * @return an empty set if there was only one match (the current path), a set of branches of
	 *         the current path if there were multiple matches.
	 */
	private Set<Path> spawnPathsFor(Set<Proposal> matches) {
		Iterator<Proposal> iter = matches.iterator();
		
		Set<Path> branches = new HashSet<>();
		while(iter.hasNext()) {
			Proposal proposal = iter.next();
			
			if(iter.hasNext()) {
				// add each proposal to a clone of the existing, unmodified path
				Path newPath = this.duplicate();
				newPath.add(proposal);
				branches.add(newPath);
			} else {
				// except for the last proposal, which is added to the current path
				add(proposal);
			}
		}
		return branches;
	}

	/**
	 * Adds a proposal to the current path.
	 * 
	 * @param proposal the proposal to add.
	 */
	private void add(Proposal proposal) {
		if(firstElement == null) {
			firstElement = new Element(proposal);
		} else {
			firstElement.add(proposal);			
		}
	}
}
