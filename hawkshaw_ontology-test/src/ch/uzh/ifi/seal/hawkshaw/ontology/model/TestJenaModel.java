package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.support.io.FileUtil;

public class TestJenaModel {
	private static final String TDB_TEST_FOLDER = "tdb_test";
	
	private File tdbTestFolder;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		IPath pluginStateLocationPath = OntologyCore.getDefault().getStateLocation();
		IPath tdbTestFolderPath = pluginStateLocationPath.append(TDB_TEST_FOLDER);
		tdbTestFolder = tdbTestFolderPath.toFile();
	}

	@After
	public void tearDown() throws Exception {
		if(tdbTestFolder != null && tdbTestFolder.exists()) {
			FileUtil.delete(tdbTestFolder);
			tdbTestFolder = null;
		}
	}
	
	@Test
	public void testCreateEmptyModel() {
		
	}
}
