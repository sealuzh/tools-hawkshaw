package ch.uzh.ifi.seal.hawkshaw.support.exceptions;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;


/**
 * Super class for all unchecked exceptions thrown by classes of this plug-in.
 * 
 * @author wuersch
 * 
 */
public class HawkshawException extends RuntimeException {
	private static final long serialVersionUID = -2011337602157416320L;
	private IHawkshawPlugin context;
	
	/**
	 * Constructor. Allows to specify an error message.
	 * 
	 * @param context the plug-in.
	 * @param message the error message.
	 */
	public HawkshawException(IHawkshawPlugin context, String message) {
		super(message);
		this.context = context;
	}

	/**
	 * Constructor. Allows to specify an error message and the cause for
	 * this exception.
	 * 
	 * @param context the plug-in.
	 * @param message the error message.
	 * @param cause the cause.
	 */
	public HawkshawException(IHawkshawPlugin context, String message, Throwable cause) {
		super(message, cause);
		this.context = context;
	}
	
	public IStatus convertToStatus() {
		return new Status(IStatus.ERROR, context.getID(), getLocalizedMessage(), this);
	}
}
