package ch.uzh.ifi.seal.hawkshaw.support.logging;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public final class Log {
	private Log() {
		super();
	}
	
	public static void trace(Object origin, Object message) {
		// TODO auto-generated method-stub
	}

	public static void trace(Object origin, Object message, Throwable t) {
		// TODO auto-generated method-stub
	}

	public static void debug(Object origin, Object message) {
		// TODO auto-generated method-stub
	}

	public static void debug(Object origin, Object message, Throwable t) {
		// TODO auto-generated method-stub
	}

	public static void info(Object origin, Object message) {
		// TODO auto-generated method-stub
	}

	public static void info(Object origin, Object message, Throwable t) {
		// TODO auto-generated method-stub
	}

	public static void warn(Object origin, Object message) {
		// TODO auto-generated method-stub
	}

	public static void warn(Object origin, Object message, Throwable t) {
		// TODO auto-generated method-stub
	}

	public static void error(Object origin, Object message) {
		// TODO auto-generated method-stub
	}

	public static void error(Object origin, Object message, Throwable t) {
		// TODO auto-generated method-stub
	}

	public static void fatal(Object origin, Object message) {
		// TODO auto-generated method-stub
	}

	public static void fatal(Object origin, Object message, Throwable t) {
		// TODO auto-generated method-stub
	}
}
