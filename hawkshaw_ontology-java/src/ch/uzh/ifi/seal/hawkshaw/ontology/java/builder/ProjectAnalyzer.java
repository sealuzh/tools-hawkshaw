package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model.CompilationUnitVisitor;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model.JavaOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.exceptions.HawkshawBuildException;

public class ProjectAnalyzer {
	private JavaOntModel model;
	
	private int totalCompilationUnits;
	private int totalPackages;

	private int worked;

	public ProjectAnalyzer(JavaOntModel model) {
		this.model = model;
		
		totalPackages         = 0;
		totalCompilationUnits = 0;
		worked                = 0;
	}
	
	public void performFullAnalysis(IJavaProject javaProject, IProgressMonitor monitor) throws HawkshawBuildException {		
		assessAmountOfWork(javaProject);
		
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		subMonitor.subTask("Performing full analysis of " + javaProject.getElementName() + " (" + totalCompilationUnits + " Java files)");
		
		try {
			IPackageFragment[] packageFragments = javaProject.getPackageFragments();
			
			subMonitor.setWorkRemaining(totalPackages);
			
			for(IPackageFragment packageFragment : packageFragments) {
				if(subMonitor.isCanceled()) {
					return;
				}
				
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					processPackage(packageFragment, subMonitor.newChild(1));
				}
			}
		} catch(JavaModelException jmex) {
			throw new HawkshawBuildException("Error while processing Java project. " + jmex.getMessage(), jmex, javaProject);
		}
	}
	
	public void performDeltaAnalysis(IResourceDelta delta, IProgressMonitor monitor) {
		// TODO implement delta analysis
	}
	
	private void assessAmountOfWork(IJavaProject javaProject) {
		int packageCounter = 0;
		int compilationUnitCounter = 0;
		
		try {
			for(IPackageFragment packageFragment :javaProject.getPackageFragments()) {
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					ICompilationUnit[] units = packageFragment.getCompilationUnits();
					
					packageCounter++;
					compilationUnitCounter += units.length;
				}
			}
		} catch(JavaModelException jme) {
			throw new HawkshawBuildException("Error while assessing the amount of work.", javaProject, jme);
		}
		
		totalPackages = packageCounter;
		totalCompilationUnits = compilationUnitCounter;
	}

	private void processPackage(IPackageFragment packageFragment, IProgressMonitor monitor) throws HawkshawBuildException {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		subMonitor.subTask("Processing " + packageFragment.getElementName());
		
		try {
			ICompilationUnit[] compilationUnits = packageFragment.getCompilationUnits();
			
			subMonitor.setWorkRemaining(compilationUnits.length);
			
			for (ICompilationUnit compilationUnit : compilationUnits) {
				if(subMonitor.isCanceled()) {
					return;
				}
				
				processCompilationUnit(compilationUnit, packageFragment.getElementName(), subMonitor.newChild(1));
			}
		} catch(JavaModelException jmex) {
			throw new HawkshawBuildException("Error while processing package '" + packageFragment.getElementName() + "'. " + jmex.getMessage(), jmex, packageFragment); // TODO Build exception?
		}
	}

	private void processCompilationUnit(ICompilationUnit compilationUnit, String packageName, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		subMonitor.subTask("Parsing and RDF-izing (" + (++worked) + '/' + totalCompilationUnits + "): " + compilationUnit.getElementName());
		
		ASTParser lParser = ASTParser.newParser(AST.JLS4); // up to J2SE 1.7
		lParser.setSource(compilationUnit);
		lParser.setResolveBindings(true);
		
		CompilationUnit cu = (CompilationUnit) lParser.createAST(subMonitor.newChild(60));
		cu.accept(new CompilationUnitVisitor(compilationUnit, packageName, model));
		subMonitor.worked(40);
	}
}
