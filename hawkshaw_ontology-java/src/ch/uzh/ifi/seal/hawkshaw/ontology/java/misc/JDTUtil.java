package ch.uzh.ifi.seal.hawkshaw.ontology.java.misc;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;

// TODO write a test.
public final class JDTUtil {
	
	private JDTUtil() {
		// no instances
	}
	
	public static boolean isInterface(TypeDeclaration node) {
		return node.isInterface();
	}
	
	public static boolean isInterface(Type type) {
		return type.resolveBinding().isInterface();
	}
	
	public static boolean isException(Type type) {
		return isException(type.resolveBinding());
	}

	public static boolean isException(TypeDeclaration type) {
		return isException(type.resolveBinding());
	}
	
	public static boolean isException(ITypeBinding type) {
		return findTypeInHierarchy(type, "java.lang.Throwable") != null;
	}
	
	public static boolean isConstructor(MethodDeclaration node) {
		return node.isConstructor();
	}
	
	public static String resolveAndEncode(TypeDeclaration node) {
		return resolveAndEncode(node.resolveBinding());
	}
	
	public static String resolveAndEncode(ITypeBinding node) {
		return transformAndEncode(qualifierFor(node));
	}
	
	public static String resolveAndEncode(Type type) {
		return transformAndEncode(qualifierFor(type.resolveBinding()));
	}
	
	public static String resolveAndEncode(MethodDeclaration node) {
		return resolveAndEncode(node.resolveBinding());
	}
	
	public static String resolveAndEncode(IMethodBinding binding) {
		String methodIdentifier = binding.getName();
		String encodedParentTypeQualifier = transformAndEncode(qualifierFor(binding.getDeclaringClass()));
		
		StringBuilder sb = new StringBuilder(methodIdentifier).append('(');
		
		// add fully qualified param type names, delimited by '+'
		ITypeBinding[] params = binding.getParameterTypes();
		for(int i = 0; i < params.length; i++) {
			if(i > 0) { // separate params
				sb.append(',');
			}
			
			String parameterTypeQualifier = params[i].getQualifiedName();
			int dotpos = parameterTypeQualifier.lastIndexOf('.');
			if(dotpos > -1) {
				sb.append(parameterTypeQualifier.substring(dotpos + 1));
			} else {
				sb.append(parameterTypeQualifier);
			}
		}
		
		sb.append(')');
		
		return encodedParentTypeQualifier + '.' + UriUtil.encode(sb.toString());
	}
	
	public static String resolveAndEncodeCallee(MethodInvocation node) {
		IMethodBinding resolvedBinding = node.resolveMethodBinding();
		IMethodBinding callee = resolvedBinding.getMethodDeclaration();
		
		return resolveAndEncode(callee);
	}
	
	public static String resolveAndEncodeCallee(ConstructorInvocation node) {
		IMethodBinding resolvedBinding = node.resolveConstructorBinding();
		IMethodBinding callee = resolvedBinding.getMethodDeclaration();
		
		return resolveAndEncode(callee);
	}
	
	public static String qualifierFor(ITypeBinding binding) { // if(binding.isPrimitive())
		return binding.getQualifiedName();
	}
	
	public static String identifierFor(AbstractTypeDeclaration node) {
		return node.getName().getIdentifier();
	}
	
	public static String identifierFor(Type type) { // ArrayType, ParameterizedType, PrimitiveType, QualifiedType, SimpleType, WildcardType
		return identifierFor(type.resolveBinding());
	}
	
	public static String identifierFor(ITypeBinding type) {
		return type.getName();
	}
	
	public static String identifierFor(VariableDeclarationFragment fragment) {
		return fragment.getName().getIdentifier();
	}
	
	public static String identifierFor(SingleVariableDeclaration svd) {
		return svd.getName().getIdentifier();
	}
	
	public static String identifierFor(IVariableBinding fieldBinding) {
		return fieldBinding.getName();
	}
	
	public static String identifierFor(MethodDeclaration node) {
		return node.getName().getIdentifier();
	}
	
	public static String identifierFor(MethodInvocation node) {
		return node.getName().getIdentifier();
	}
	
	public static String identifierFor(ConstructorInvocation node) {
		return node.resolveConstructorBinding().getName();
	}
	
	public static String transformAndEncode(String qualifiedTypeName) {
//		int pos = qualifiedTypeName.lastIndexOf('.');
//		if (pos > -1) {
//			return new StringBuilder().append(UriUtil.encode(qualifiedTypeName.substring(0, pos)))
//									  .append('/')
//									  .append(UriUtil.encode(qualifiedTypeName.substring(pos + 1, qualifiedTypeName.length())))
//									  .toString();
//		} else {
			return UriUtil.encode(qualifiedTypeName);
//		}
	}
	
	/**
	 * Finds a type binding for a given fully qualified type in the hierarchy of a type.
	 * Returns <code>null</code> if no type binding is found.
	 * @param hierarchyType the binding representing the hierarchy
	 * @param fullyQualifiedTypeName the fully qualified name to search for
	 * @return the type binding
	 */
	public static ITypeBinding findTypeInHierarchy(ITypeBinding hierarchyType, String fullyQualifiedTypeName) {
		if (hierarchyType.isArray() || hierarchyType.isPrimitive()) {
			return null;
		}
		if (fullyQualifiedTypeName.equals(hierarchyType.getQualifiedName())) {
			return hierarchyType;
		}
		ITypeBinding superClass= hierarchyType.getSuperclass();
		if (superClass != null) {
			ITypeBinding res= findTypeInHierarchy(superClass, fullyQualifiedTypeName);
			if (res != null) {
				return res;
			}
		}
		ITypeBinding[] superInterfaces= hierarchyType.getInterfaces();
		for (int i= 0; i < superInterfaces.length; i++) {
			ITypeBinding res= findTypeInHierarchy(superInterfaces[i], fullyQualifiedTypeName);
			if (res != null) {
				return res;
			}
		}
		return null;
	}
}
