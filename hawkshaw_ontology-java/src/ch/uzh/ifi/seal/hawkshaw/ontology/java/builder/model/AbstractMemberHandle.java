package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.hp.hpl.jena.ontology.Individual;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractModelEntityHandle;

/**
 * 
 * @author wuersch
 *
 */
public class AbstractMemberHandle extends AbstractModelEntityHandle<JavaOntModel>{

	public AbstractMemberHandle(Individual theEntity, JavaOntModel model) {
		super(theEntity, model);
	}

	public void setPublic() {
		addRelation(IHawkshawJavaOntology.HAS_ACCESS_MODIFIER, IHawkshawJavaOntology.PUBLIC);
		addLiteral(IHawkshawJavaOntology.IS_PUBLIC, true);
	}
	
	public void setPrivate() {
		addRelation(IHawkshawJavaOntology.HAS_ACCESS_MODIFIER, IHawkshawJavaOntology.PRIVATE);
		addLiteral(IHawkshawJavaOntology.IS_PRIVATE, true);
	}
	
	public void setProtected() {
		addRelation(IHawkshawJavaOntology.HAS_ACCESS_MODIFIER, IHawkshawJavaOntology.PROTECTED);
		addLiteral(IHawkshawJavaOntology.IS_PROTECTED, true);
	}
	
	public void setDefault() {
		addRelation(IHawkshawJavaOntology.HAS_ACCESS_MODIFIER, IHawkshawJavaOntology.DEFAULT);
	}

	public void setAbstract() {
		addLiteral(IHawkshawJavaOntology.IS_ABSTRACT, true);
	}
	
	public void setStatic() {
		addLiteral(IHawkshawJavaOntology.IS_STATIC, true);
	}
	
	public void setFinal() {
		addLiteral(IHawkshawJavaOntology.IS_FINAL, true);
	}
}
