package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.exceptions.InvalidResourceException;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractHawkshawModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.IPostCreationHook;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.Payload;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IHawkshawOntology;

import com.hp.hpl.jena.ontology.Individual;

public class JavaOntModel extends AbstractHawkshawModel {	
	public JavaOntModel(PersistentJenaModel baseModel) {
		super(baseModel);
		init();
	}
	
	@Override
	public void clear() {
		getBaseModel().clear();
		init();
	}
	
	public FileHandle getFileHandleFor(String uniqueName, String localName) {
		Payload<Individual> individual = getBaseModel().fetchIndividual(uniqueName, localName,  IHawkshawOntology.FILE);
		
		return new FileHandle(individual.getPayload(), this);
	}
	
	public PackageHandle getPackageHandleFor(String uniqueName, String localName) {
		Individual individual = internalFetchIndividual(uniqueName, localName,  IHawkshawJavaOntology.PACKAGE);
		
		return new PackageHandle(individual, this);
	}
	
	public InterfaceHandle getInterfaceHandleFor(String uniqueName, String localName) {
		Individual individual = internalFetchIndividual(uniqueName, localName, IHawkshawJavaOntology.INTERFACE);
		
		return new InterfaceHandle(individual, this);
	}
	
	public ClassHandle getClassHandleFor(String uniqueName, String localName) {
		return getClassOrExceptionHandleFor(uniqueName, localName, IHawkshawJavaOntology.CLASS);
	}
	
	public ClassHandle getExceptionHandleFor(String uniqueName, String localName) {
		return getClassOrExceptionHandleFor(uniqueName, localName, IHawkshawJavaOntology.EXCEPTION);
	}
	
	public FieldHandle getFieldHandleFor(String uniqueName, String localName) {
		Individual individual = internalFetchIndividual(uniqueName, localName, IHawkshawJavaOntology.FIELD);
		
		return new FieldHandle(individual, this);
	}
	
	public MethodHandle getMethodHandleFor(String uniqueName, String localName) {
		Individual individual = internalFetchIndividual(uniqueName, localName + "()", IHawkshawJavaOntology.METHOD);
		
		return new MethodHandle(individual, this);
	}
	
	public ConstructorHandle getConstructorHandleFor(String uniqueName, String localName) {
		Individual individual = internalFetchIndividual(uniqueName, localName + "()", IHawkshawJavaOntology.CONSTRUCTOR);
		
		return new ConstructorHandle(individual, this);
	}
	
	public MethodParameterHandle getMethodParameterHandleFor(String uniqueName, String localName, String typeName) {
		Individual individual = internalFetchIndividual(uniqueName, localName, typeName, IHawkshawJavaOntology.PARAMETER);
		
		return new MethodParameterHandle(individual, this);
	}
	
	public PackageHandle addPackage(String uniqueName, String localName) {
		return getPackageHandleFor(uniqueName, localName);
	}
	
	private void init() {
			getBaseModel().readOntologies(
				"http://se-on.org/ontologies/hawkshaw/2012/02/code.owl",
				"http://se-on.org/ontologies/hawkshaw/2012/02/code-nl.owl"
			);
	}
	
	private ClassHandle getClassOrExceptionHandleFor(String uniqueName, String localName, String kind) {
		Individual individual = internalFetchIndividual(uniqueName, localName, kind);
		
		return new ClassHandle(individual, this);
	}
	
	private Individual internalFetchIndividual(String uniqueName, String label, String identifier, String ontClass) throws InvalidResourceException {
		return fetchIndividual(uniqueName, label, ontClass, new IdentifierAdditionHook(identifier));
	}
	
	private Individual internalFetchIndividual(String uniqueName, String identifier, String ontClass) throws InvalidResourceException {
		return internalFetchIndividual(uniqueName, identifier, identifier, ontClass);
	}
	
	private final class IdentifierAdditionHook implements IPostCreationHook {
		private String identifier;
	
		private IdentifierAdditionHook(String identifier) {
			this.identifier = identifier;
		}
		
		@Override
		public void created(Individual individual) {
			addLiteralTo(individual, IHawkshawJavaOntology.HAS_IDENTIFIER, identifier);
		}
	}
}
