package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractModelEntityHandle;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java packages.
 * 
 * @author wuersch
 *
 */
public class PackageHandle extends AbstractModelEntityHandle<JavaOntModel> {
	/**
	 * Constructor.
	 * 
	 * @param thePackage
	 *            the package to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public PackageHandle(Individual thePackage, JavaOntModel model) {
		super(thePackage, model);
	}
	
	/**
	 * Adds an interface to the underlying package.
	 * 
	 * @param qualifiedInterfaceName
	 *            the qualified interface name
	 * @param localInterfaceName
	 *            the local identifier of the interface, used as human-readable
	 *            label.
	 * @param location
	 *            the source code location of the interface declaration.
	 * 
	 * @return a handle to the newly added interface.
	 */
	public InterfaceHandle addInterface(String qualifiedInterfaceName, String localInterfaceName, SourceCodeLocationHelper location) {
		InterfaceHandle interfaceHandle = model().getInterfaceHandleFor(qualifiedInterfaceName, localInterfaceName);
	
		addType(interfaceHandle, location);
		
		return interfaceHandle;
	}
	
	/**
	 * Adds an interface to the underlying package.
	 * 
	 * @param qualifiedClassName
	 *            the qualified class name
	 * @param localClassName
	 *            the local identifier of the class, used as human-readable
	 *            label.
	 * @param location
	 *            the source code location of the class declaration.
	 * 
	 * @return a handle to the newly added interface.
	 */
	public ClassHandle addClass(String qualifiedClassName, String localClassName, SourceCodeLocationHelper location) {
		ClassHandle classHandle = model().getClassHandleFor(qualifiedClassName, localClassName);
		
		addType(classHandle, location);
		
		return classHandle;
	}
	
	/**
	 * Helper method for
	 * {@link #addInterface(String, String, SourceCodeLocationHelper)} and
	 * {@link #addClass(String, String, SourceCodeLocationHelper)}.
	 * 
	 * @param typeHandle
	 *            the handle of the type that should be added to the underlying
	 *            package.
	 * @param localName
	 *            the local identifier of the type.
	 * @param location
	 *            the source code location of the type declaration.
	 */
	private void addType(AbstractTypeHandle typeHandle, SourceCodeLocationHelper location) {
		typeHandle.addRelation(IHawkshawJavaOntology.IS_PACKAGE_MEMBER_OF , this);
				
		location.applyTo(typeHandle);
	}

	public ClassHandle addException(String qualifiedExceptionName, String typeIdentifier, SourceCodeLocationHelper location) {
		ClassHandle exceptionHandle = model().getExceptionHandleFor(qualifiedExceptionName, typeIdentifier);
		
		addType(exceptionHandle, location);
		
		return exceptionHandle;
	}
}
