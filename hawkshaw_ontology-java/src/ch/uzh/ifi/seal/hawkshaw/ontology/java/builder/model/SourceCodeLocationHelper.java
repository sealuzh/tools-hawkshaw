package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractModelEntityHandle;

/**
 * Parameter object for source code location information.
 * 
 * @author Michael
 *
 */
public class SourceCodeLocationHelper {
	private String path;
	private int startPosition;
	private int length;
	
	public SourceCodeLocationHelper(ICompilationUnit cu, ASTNode node) {
		this(cu.getHandleIdentifier(), node.getStartPosition(), node.getLength());
	}
	
	public SourceCodeLocationHelper(String path, int startPosition, int length) {
		this.path = path;
		this.startPosition = startPosition;
		this.length = length;
	}
	
	public void applyTo(AbstractModelEntityHandle<JavaOntModel> handle) {		
		handle.addLiteral(IHawkshawJavaOntology.HAS_PATH, path);
		handle.addLiteral(IHawkshawJavaOntology.STARTS_AT, startPosition);
		handle.addLiteral(IHawkshawJavaOntology.HAS_LENGTH, length);
	}
}
