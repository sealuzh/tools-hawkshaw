package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java methods.
 * 
 * @author wuersch
 *
 */
public class MethodHandle extends AbstractCallableHandle {
	/**
	 * Constructor.
	 * 
	 * @param theMethod
	 *            the method to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public MethodHandle(Individual theMethod, JavaOntModel model) {
		super(theMethod, model);
	}
	
	/**
	 * Sets the return type of this method.
	 * 
	 * @param returnTypeHandle
	 *            the handle for the return type of this method.
	 */
	public void setReturnType(AbstractTypeHandle returnTypeHandle) { // TODO Interfaces?
		addRelation(IHawkshawJavaOntology.HAS_RETURN_TYPE, returnTypeHandle);
	}
}
