package ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * This interface defines the essentials of the Seon Java Ontology in terms of
 * constants. It therefore bridges the gap between the OWL description of the entities
 * and the Hawkshaw environment.
 * 
 * @author wuersch
 *
 */
public interface IHawkshawJavaOntology {

	String BASE                       = "http://se-on.org/ontologies/hawkshaw/2012/02/code.owl";
	String LOCAL                      = "local_hawkshaw_code_12_02";

	// Classes
	String COMPLEX_TYPE               = BASE + "#ComplexType";
	String PACKAGE                    = BASE + "#Package";
	String CLASS                      = BASE + "#ClassType";
	String EXCEPTION                  = BASE + "#ExceptionType";
	String INTERFACE                  = BASE + "#InterfaceType";
	String FIELD                      = BASE + "#Field";
	String METHOD                     = BASE + "#Method";
	String CONSTRUCTOR                = BASE + "#Constructor";
	String PARAMETER                  = BASE + "#Parameter";
	
	// Object properties:
	String HAS_PACKAGE_MEMBER         = BASE + "#hasPackageMember";
	String IS_PACKAGE_MEMBER_OF       = BASE + "#isPackageMemberOf";
	
	String HAS_DATA_TYPE              = BASE + "#hasDatatype";
	String IS_DATA_TYPE_OF            = BASE + "#isDatatypeOf";

	String HAS_SUPERCLASS             = BASE + "#hasSuperClass";
	String HAS_SUBCLASS               = BASE + "#hasSubClass";

	String HAS_SUPER_INTERFACE        = BASE + "#hasSuperInterface";
	String HAS_SUB_INTERFACE          = BASE + "#hasSubInterface";
	String IMPLEMENTS_INTERFACE       = BASE + "#implementsInterface";
	
	String DECLARES_FIELD             = BASE + "#declaresField";
	String IS_DECLARED_FIELD_OF       = BASE + "#isDeclaredFieldOf";

	String DECLARES_METHOD            = BASE + "#declaresMethod";
	String IS_DECLARED_METHOD_OF      = BASE + "#isDeclaredMethodOf";
	
	String DECLARES_CONSTRUCTOR       = BASE + "#declaresConstructor";
	String IS_DECLARED_CONSTRUCTOR_OF = BASE + "#isDeclaredConstructorOf";

	String HAS_RETURN_TYPE            = BASE + "#hasReturnType";
	String IS_RETURN_TYPE_OF          = BASE + "#isReturnTypeOf";
	String HAS_PARAMETER              = BASE + "#hasParameter";
	String IS_PARAMETER_OF            = BASE + "#isParameterOf";
	String THROWS_EXCEPTION           = BASE + "#throwsException";
	String INVOKES_METHOD             = BASE + "#invokesMethod";
	String IS_INVOKED_BY_METHOD       = BASE + "#isInvokedByMethod";
	String INVOKES_CONSTRUCTOR        = BASE + "#invokesConstructor";
	String IS_INVOKED_BY_CONSTRUCTOR  = BASE + "#isInvokedByConstructor";

	String ACCESSES_FIELD             = BASE + "#accessesField";
	String IS_ACCESSED_BY_METHOD      = BASE + "#isAccessedByMethod";
	
	String HAS_ACCESS_MODIFIER        = BASE + "#hasAccessModifier";

	/* Bridge to file system */
	String CONTAINS_CODE_ENTITY       = BASE + "#containsCodeEntity";
	String IS_CONTAINED_BY            = BASE + "#isContainedBy";

	// Datatype properties:
	String HAS_PATH                   = BASE + "#hasPath";
	String HAS_IDENTIFIER             = BASE + "#hasIdentifier";
	String STARTS_AT                  = BASE + "#startsAt";
	String HAS_LENGTH                 = BASE + "#hasLength";
	String IS_ABSTRACT                = BASE + "#isAbstract";
	String HAS_POSITION               = BASE + "#hasPosition";
	String IS_PUBLIC                  = BASE + "#isPublic"; 
	String IS_PRIVATE                 = BASE + "#isPrivate"; 
	String IS_PROTECTED               = BASE + "#isProtected"; 
	String IS_STATIC                  = BASE + "#isStatic";
	String IS_FINAL                   = BASE + "#isFinal";
	
	// Individuals
	String PUBLIC                     = BASE + "#public"; 
	String PRIVATE                    = BASE + "#private"; 
	String PROTECTED                  = BASE + "#protected"; 
	String DEFAULT                    = BASE + "#public";
}
