package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Abstract base class for type handles, currently class and interface handles.
 * Enums are types too, but are not considered yet.
 * 
 * @author wuersch
 * 
 */
public abstract class AbstractTypeHandle extends AbstractMemberHandle {
	/**
	 * Constructor.
	 * 
	 * @param theType the type to which this handle refers to. 
	 * @param isInterface denotes whether the current type is an interface or not.
	 * @param model the associated Java source code model.
	 */
	protected AbstractTypeHandle(Individual theType, JavaOntModel model) {
		super(theType, model);
	}
	
	/**
	 * Adds a new field to the underlying type and returns a handle for it.
	 * 
	 * @param qualifiedFieldName
	 *            the fully qualified name of the field, i.e., an unique
	 *            identifier composed of the qualified parent name and the local
	 *            identifier of the field.
	 * @param fieldName
	 *            the local identifier of the field, used as a human-readable
	 *            label.
	 * @param location
	 *            the source code location of the field declaration.
	 * 
	 * @return a handle to the newly added field.
	 */
	// TODO override in InterfaceHandle to set constant?
	public FieldHandle addField(String qualifiedFieldName, String fieldName, SourceCodeLocationHelper location) {
		FieldHandle handle = model().getFieldHandleFor(qualifiedFieldName, fieldName);
		
		addRelation(IHawkshawJavaOntology.DECLARES_FIELD, handle);
		location.applyTo(handle);
		
		return handle;
	}
	
	/**
	 * Adds a new method to the underlying type and returns a handle for it.
	 * 
	 * @param qualifiedMethodName
	 *            the fully qualified name of the method, i.e., an unique
	 *            identifier composed of the qualified parent name, the local
	 *            identifier of the method, and the qualified type names of the
	 *            method's parameters.
	 * @param methodName
	 *            the local identifier of the method, used as a human-readable
	 *            label.
	 * @param location
	 *            location the source code location of the method declaration.
	 * 
	 * @return a handle to the newly added method.
	 */
	public MethodHandle addMethod(String qualifiedMethodName, String methodName, SourceCodeLocationHelper location) {
		MethodHandle handle = model().getMethodHandleFor(qualifiedMethodName, methodName);
		
		addRelation(IHawkshawJavaOntology.DECLARES_METHOD, handle);
		location.applyTo(handle);
		
		return handle;
	}

	/**
	 * Adds a new constructor to the underlying type and returns a handle for it.
	 * 
	 * @param qualifiedMethodName
	 *            the fully qualified name of the constructor, i.e., an unique
	 *            identifier composed of the qualified parent name, the local
	 *            identifier of the constructor, and the qualified type names of the
	 *            constructor's parameters.
	 * @param methodName
	 *            the local identifier of the constructor, used as a human-readable
	 *            label.
	 * @param location
	 *            location the source code location of the constructor declaration.
	 * 
	 * @return a handle to the newly added constructor.
	 */
	public ConstructorHandle addConstructor(String qualifiedMethodName, String methodName, SourceCodeLocationHelper location) {
		ConstructorHandle handle = model().getConstructorHandleFor(qualifiedMethodName, methodName);
		
		addRelation(IHawkshawJavaOntology.DECLARES_CONSTRUCTOR, handle);
		location.applyTo(handle);
		
		return handle;
	}
	
	/**
	 * Adds a new interface to the underlying type. For classes, this means that
	 * an 'implements' relationship is established, while interfaces will
	 * receive a 'has super interface' relationship.
	 * 
	 * @param interfaceHandle
	 *            the handle of the interface to add.
	 */
	public abstract void addInterface(InterfaceHandle interfaceHandle);
}
