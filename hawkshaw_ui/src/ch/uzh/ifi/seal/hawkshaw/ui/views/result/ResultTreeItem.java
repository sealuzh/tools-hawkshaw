package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jdt.core.IJavaProject;


public class ResultTreeItem implements ITreeItem<Result> {
	private Result result;
	
	public ResultTreeItem(Result result) {
		this.result = result;
	}
	
	@Override
	public Result getItem() {
		return result;
	}

	@Override
	public Object getParent() {
		return null;
	}
	
	public boolean hasChildren() {
		return result != null;
	}

	@Override
	public IJavaProject getAssociatedJavaProject() {
		return result.getAssociatedProject();
	}
	
	public Result getResult() {
		return result;
	}

	public AnswerTreeItem getChild() {
		return new AnswerTreeItem(this, result.getAnswer());
	}
}
