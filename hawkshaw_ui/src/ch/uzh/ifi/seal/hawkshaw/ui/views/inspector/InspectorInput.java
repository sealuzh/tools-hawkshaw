package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InspectorInput {
	public static final InspectorInput EMPTY = new InspectorInput(new QrItemWrapper[0], DropMode.NONE);
	
	private DropMode dropMode;
	private QrItemWrapper[] newBeans;
	
	public InspectorInput(QrItemWrapper[] beans, DropMode dropMode) {
		this.newBeans = Arrays.copyOf(beans, beans.length);
		this.dropMode =  dropMode;
	}
	
	public QrItemWrapper[] applyDropMode(QrItemWrapper[] oldBeans) {
		QrItemWrapper[] result;
		switch(dropMode) {
		case REPLACE:
			result = newBeans;
			break;
		case UNION:
			result = union(oldBeans);
			break;
		case INTERSECTION:
			result = intersection(oldBeans);
			break;
		case DIFF:
			result = diff(oldBeans);
			break;
		default:
			// DropMode.NONE or unexpected input
			result = new QrItemWrapper[0];
			break;
		}
		
		return result;
	}

	private QrItemWrapper[] diff(QrItemWrapper[] oldBeans) {
		List<QrItemWrapper> diff = new ArrayList<>();
		
		for(QrItemWrapper oldBean : oldBeans) {
			boolean keep = true;
			for(QrItemWrapper newBean : newBeans) {
				if(oldBean.equals(newBean)) {
					keep = false;
					break;
				}
			}
			
			if(keep) {
				diff.add(oldBean);
			}
		}
		
		return diff.toArray(new QrItemWrapper[diff.size()]);
	}

	private QrItemWrapper[] intersection(QrItemWrapper[] oldBeans) {
		List<QrItemWrapper> intersect = new ArrayList<>();
		
		for(QrItemWrapper oldBean : oldBeans) {
			for(QrItemWrapper newBean : newBeans) {
				if(oldBean.equals(newBean)) {
					intersect.add(newBean); // use new bean for the unlikely event that new props are available
				}
			}
		}
		
		return intersect.toArray(new QrItemWrapper[intersect.size()]);
	}

	private QrItemWrapper[] union(QrItemWrapper[] oldBeans) {
		List<QrItemWrapper> union = new ArrayList<>();
		Collections.addAll(union, newBeans);
		
		for(QrItemWrapper oldBean : oldBeans) {
			if(!union.contains(oldBean)) {
				union.add(oldBean);
			}
		}
		
		return union.toArray(new QrItemWrapper[union.size()]);
	}
}
