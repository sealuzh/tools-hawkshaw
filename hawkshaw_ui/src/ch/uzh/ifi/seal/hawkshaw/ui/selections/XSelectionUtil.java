package ch.uzh.ifi.seal.hawkshaw.ui.selections;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;

import ch.uzh.ifi.seal.hawkshaw.ui.views.result.AnswerTreeItem;
import ch.uzh.ifi.seal.hawkshaw.ui.views.result.QrTreeItem;

public final class XSelectionUtil {
	private XSelectionUtil() {
		super();
	}
	
	public static IResourceSelection resolve() {
		IWorkbenchWindow activeWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		IWorkbenchPage activePage = activeWindow.getActivePage();
		IResourceSelection selection = resolve(activePage.getActivePart(), activePage.getSelection());
		
		// no active selection...
		if(selection == null) {
			// ... but perhaps an inactive package explorer selection?
			ISelectionService service = activeWindow.getSelectionService();
			selection = resolve(null, service.getSelection(JavaUI.ID_PACKAGES));
			
			// no inactive package explorer selection, but perhaps there's only one (java) project in the workspace?
			if(selection == null) {
				IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
				IProject[] projects = workspaceRoot.getProjects();
				if(projects.length == 1) {
					IJavaProject javaProject = JavaCore.create(projects[0]);
					if(javaProject != null && javaProject.exists()) {
						selection = new JavaElementSelection(javaProject);
					}
				}
			}
		}
		
		return selection;
	}

	public static IResourceSelection resolve(IWorkbenchPart part, ISelection selection) {
		IResourceSelection context = null;
		
		if (selection instanceof ITreeSelection) {
			context = fromTreeSelection((ITreeSelection) selection);
		} else if (part instanceof ITextEditor && selection instanceof ITextSelection){
			try {
				context = fromEditor((ITextEditor) part, (ITextSelection) selection);
			} catch (JavaModelException e) {
				context = null;
			}
		}
		
		return context;
	}

	private static IResourceSelection fromTreeSelection(ITreeSelection selection) {
		IResourceSelection context = null;
		
		Object firstElement = selection.getFirstElement();
		if (firstElement instanceof IJavaElement) {
			IJavaElement javaElement = (IJavaElement) firstElement;
			context = new JavaElementSelection(javaElement);
		} else if(firstElement instanceof QrTreeItem) {
			QrTreeItem item = (QrTreeItem) firstElement;
			context = new QrItemSelection(item.getItem(), item.getAssociatedJavaProject());
		} else if(firstElement instanceof AnswerTreeItem) {
			AnswerTreeItem item = (AnswerTreeItem) firstElement;
			context = new JavaElementSelection(item.getAssociatedJavaProject());
		}
		
		return context;
	}

	private static JavaElementSelection fromEditor(ITextEditor editor, final ITextSelection selection) throws JavaModelException {
		JavaElementSelection context = null;
		
		ITypeRoot root = (ITypeRoot) JavaUI.getEditorInputJavaElement(editor.getEditorInput());
		if(root != null) {
			IJavaElement[] elements = root.codeSelect(selection.getOffset(), 0);
			if (elements.length > 0) {
				context = new JavaElementSelection(elements[0], selection.getOffset(), editor);
			}
		}
		
		return context;
	}
}
