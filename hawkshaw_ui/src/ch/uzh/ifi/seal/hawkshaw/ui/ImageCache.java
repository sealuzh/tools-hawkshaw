package ch.uzh.ifi.seal.hawkshaw.ui;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

/**
 * Caches SWT images. In SWT images can not be garbage collected automatically.
 * Instances of this class can be used to keep track of used images and be told
 * to dispose them correctly afterwards.
 * 
 * @author wuersch
 * 
 */
public final class ImageCache {
	
	private Map<ImageDescriptor, Image> cache;
	
	/**
	 * Default constructor.
	 */
	public ImageCache() {
		cache = new HashMap<>();
	}
	
	/**
	 * Creates an image from a descriptor, if necessary, and caches it for
	 * future use.
	 * 
	 * @param descriptor
	 *            a handle to the image.
	 * @return an image created from the handle.
	 */
	public Image get(ImageDescriptor descriptor) {
		Image image = cache.get(descriptor);
		
		if(image == null) {
			image = descriptor.createImage();
			cache.put(descriptor, image);
		}
		
		return image;
	}
	
	/**
	 * Disposes all cached images explicitly. Typically called in a plug-in's
	 * {@link org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 * stop()} method.
	 */
	public void dispose() {
		for(Image image : cache.values()) {
			image.dispose();
		}
		
		cache.clear();
	}
}
