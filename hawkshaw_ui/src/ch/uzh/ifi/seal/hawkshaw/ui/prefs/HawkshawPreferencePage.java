package ch.uzh.ifi.seal.hawkshaw.ui.prefs;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class HawkshawPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	private FieldEditor modelTypeEditor;

	public HawkshawPreferencePage()  {
		super("Hawkshaw Queries", IHawkshawImages.HAWK_ICON, FieldEditorPreferencePage.GRID);
	}
	
	@Override
	public void init(IWorkbench workbench) {
		// do nothing
	}

	@Override
	protected void createFieldEditors() {		
		modelTypeEditor = new RadioGroupFieldEditor(IPreferenceConstants.MODEL_TYPE, "Specify the kind of model you want to use to store queryable project information", 1 /* columns */, getRadioButtonLabelsAndValues() , getFieldEditorParent(), true /* use group */);
		addField(modelTypeEditor);
	}
	
	private String[][] getRadioButtonLabelsAndValues() {
		return new String[][] {{"Jena/In-Memory", IPreferenceConstants.MODEL_TYPE_MEM}, { "Jena/TDB", IPreferenceConstants.MODEL_TYPE_FS}};
	}

	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return QueryUI.getDefault().getPreferenceStore();
	}

}
