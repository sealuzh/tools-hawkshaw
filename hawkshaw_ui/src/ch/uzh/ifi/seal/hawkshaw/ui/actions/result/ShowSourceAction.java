package ch.uzh.ifi.seal.hawkshaw.ui.actions.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class ShowSourceAction extends Action {
	private static final String ID = "ch.uzh.ifi.seal.hawkshaw.ui.commands.result.openInEditor";
	
	private final IJavaElement javaElement;

	public ShowSourceAction(IJavaElement javaElement) {
		super("Open in editor...");
		this.javaElement = javaElement;
		
		setActionDefinitionId(ID);
		setImageDescriptor(IHawkshawImages.APP_GO);
	}

	@Override
	public void run() {
		try {
			IEditorPart editor = JavaUI.openInEditor(javaElement);
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			page.activate(editor);
		} catch (PartInitException pie) {
			throw new HawkshawException(QueryUI.getDefault(), "Could not open Java Editor.", pie); // TODO own exception
		} catch (JavaModelException jme) {
			throw new HawkshawException(QueryUI.getDefault(), "Could not open Java Element.", jme);
		}

	}
}