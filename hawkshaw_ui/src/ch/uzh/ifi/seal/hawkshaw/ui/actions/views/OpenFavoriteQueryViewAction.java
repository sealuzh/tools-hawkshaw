package ch.uzh.ifi.seal.hawkshaw.ui.actions.views;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.action.Action;

import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryInterfaceDispatcher;
import ch.uzh.ifi.seal.hawkshaw.ui.views.favorites.FavoriteQueryView;

public class OpenFavoriteQueryViewAction extends Action {
	public OpenFavoriteQueryViewAction() {
		super("Open Favorites...", IHawkshawImages.FAVS);
	}
	
	@Override
	public void run() {
		QueryInterfaceDispatcher.INSTANCE.openView(FavoriteQueryView.VIEW_ID);
	}
}