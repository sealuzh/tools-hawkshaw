package ch.uzh.ifi.seal.hawkshaw.ui.selections;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.ui.texteditor.ITextEditor;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.misc.JDTUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;

public class JavaElementSelection implements IResourceSelection {
	private IJavaElement javaElement;
	private int offset;
	private ITextEditor editor;
	
	public JavaElementSelection(IJavaElement javaElement, int offset, ITextEditor editor) {
		this(javaElement);
		
		this.offset = offset;
		this.editor = editor;
	}

	public JavaElementSelection(IJavaElement javaElement) {
		this.javaElement = javaElement;
	}

	@Override
	public IJavaProject getJavaProject() {
		return javaElement.getJavaProject();
	}
	
	public IJavaElement getJavaElement() {
		return javaElement;
	}
	
	public IProject getProject() {
		return javaElement.getJavaProject().getProject();
	}
	
	@Override
	public String toUriString() {
		String uri = null;
		
		if(!isProject()) {
			IProject project = getProject();
			if(OntologyCore.modelExists(project)) {
				PersistentJenaModel jom = OntologyCore.fetchPersistentModelFor(project);
				String local = toLocalUniqueNameString();
				
				if(local != null) {
					uri = UriUtil.makeAbsolute(jom.getBase(), local);
				}
			}
		}
		
		return uri;
	}
	
	private String toLocalUniqueNameString() {
		IJavaElement parsable = findParsable(javaElement);
		
		String uri = null;
		if(parsable != null) {
			ASTParser parser = ASTParser.newParser(AST.JLS4);
			parser.setProject(parsable.getJavaProject());
			
			IBinding[] bindings = parser.createBindings(new IJavaElement[] { parsable } , null);
			IBinding binding = bindings[0];
			
			
			if(binding instanceof ITypeBinding) {
				uri = toLocalUniqueNameString((ITypeBinding) binding);
			} else if(binding instanceof IVariableBinding) {
				uri = toLocalUniqueNameString((IVariableBinding) binding);
			} else if(binding instanceof IMethodBinding) {
				uri = toLocalUniqueNameString((IMethodBinding) binding);
			}
		}
		
		return uri;
	}
	
	private IJavaElement findParsable(IJavaElement javaElement) {
		IJavaElement parsable;
		
		switch(javaElement.getElementType()) {
		case IJavaElement.TYPE:
			// fall through
		case IJavaElement.FIELD:
			// fall-through
		case IJavaElement.METHOD:
			parsable = javaElement;
			break;
		case IJavaElement.COMPILATION_UNIT:
			ICompilationUnit unit = (ICompilationUnit) javaElement;
			parsable = unit.findPrimaryType();
			break;
		case IJavaElement.JAVA_PROJECT:
			parsable = null;
			break;
		default:
			parsable = findParsable(javaElement.getParent());
		}
		
		return parsable;
	}

	private String toLocalUniqueNameString(ITypeBinding binding) {
		return JDTUtil.resolveAndEncode(binding);
//		return binding.getBinaryName().replaceAll("^(.*)\\.(.*)$","$1/$2");
	}
	
	private String toLocalUniqueNameString(IVariableBinding binding) {
		String uri = null;
		
		if(binding.isField()) {
			String parent = JDTUtil.resolveAndEncode(binding.getDeclaringClass());
			String identifier = binding.getName();
			uri = parent + '/' + identifier;
		}
		
		return uri;
	}
	
	private String toLocalUniqueNameString(IMethodBinding binding) {
		return JDTUtil.resolveAndEncode(binding);
	}
	
	public boolean isProject() {
		return javaElement.getElementType() == IJavaElement.JAVA_PROJECT;
	}

	@Override
	public boolean isJavaElement() {
		return true;
	}

	@Override
	public String getName() {
		return javaElement.getElementName();
	}
}
