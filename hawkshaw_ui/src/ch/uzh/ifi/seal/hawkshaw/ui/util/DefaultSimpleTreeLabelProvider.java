package ch.uzh.ifi.seal.hawkshaw.ui.util;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

public class DefaultSimpleTreeLabelProvider extends ColumnLabelProvider {
	private IField field;
	
	public DefaultSimpleTreeLabelProvider(IField field) {
		this.field = field;
	}
	
	@Override
	public String getText(Object element) {
		return field.getValue(element);
	}
	
	@Override
	public Image getImage(Object element) {
		return field.getImage(element);
	}
	
	@Override
	public String getToolTipText(Object element) {
		return field.getToolTipText(element);
	}

	@Override
	public Image getToolTipImage(Object object) {
		return field.getToolTipImage(object);
	}
}
