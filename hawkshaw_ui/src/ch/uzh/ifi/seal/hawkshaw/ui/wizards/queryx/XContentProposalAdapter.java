package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Text;



/**
 * This class has the normal {@link ContentProposalAdapter} behavior, except
 * that it exposes a means to programmatically open the proposal pop-up by
 * calling {@link #activate()}.
 * 
 * @author Michael
 * 
 */
// TODO I should consider merging this class with EContentProposalAdapter
public class XContentProposalAdapter extends EContentProposalAdapter {
	private Text control;
	private ProposalHandler proposalProvider;
	
	/**
	 * Constructs the extended content proposal adapter that can assist the user with
	 * choosing content for the field.
	 * 
	 * @param control
	 *            the {@link Text} control for which the adapter is providing content assist.
	 *            May not be <code>null</code>.
	 * @param proposalProvider
	 *            the <code>IContentProposalProvider</code> used to obtain
	 *            content proposals for this control, or <code>null</code> if
	 *            no content proposal is available.
	 */
	public XContentProposalAdapter(Text control, ProposalHandler proposalProvider) {
		super(control, new TextContentAdapter(), proposalProvider);
		
		this.control = control;
		this.proposalProvider = proposalProvider;
		
		installListeners();
		
		setPopupSize(new Point(200, 150));
	}

	/**
	 * Sets focus to the control and opens the proposal popup.
	 * 
	 * @see ContentProposalAdapter#openProposalPopup()
	 */
	public void activate() {
		control.setFocus();
		openProposalPopup();
	}

	private void installListeners() {
		control.addListener(SWT.DefaultSelection, proposalProvider);
		control.addKeyListener(proposalProvider);
		control.addVerifyListener(proposalProvider);
		
		addContentProposalListener(proposalProvider);
	}
}
