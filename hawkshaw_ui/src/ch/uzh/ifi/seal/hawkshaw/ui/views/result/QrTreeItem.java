package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.IJavaProject;

import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;
import ch.uzh.ifi.seal.hawkshaw.ui.views.shared.IQrItemContainer;

public class QrTreeItem implements ITreeItem<QrItem>, IQrItemContainer {
	private QrItem item;
	
	private ITreeItem<?> parent;
	private Set<QrTreeItem> children;
	
	public QrTreeItem(QrItem item) {
		this.item = item;
		children = new HashSet<>();
	}
	
	public void setParent(ITreeItem<?> parent) {
		this.parent = parent;
	}

	public void addChild(QrTreeItem child) {
		children.add(child);
	}

	@Override
	public QrItem getItem() {
		return item;
	}

	@Override
	public Object getParent() {
		return parent;
	}
	
	public String getUri() {
		return item.getUri();
	}

	@Override
	public QrItem[] getItems() {
		QrItem[] result;
		
		if(item.isLiteral() && parent instanceof IQrItemContainer) {
			result = ((IQrItemContainer) parent).getItems();
		} else {
			result = new QrItem[] { item };
		}
		
		return result;
	}

	@Override
	public boolean hasChildren() {
		return children.size() > 0;
	}

	public Object[] getChildren() {
		return children.toArray(new QrTreeItem[children.size()]);
	}

	@Override
	public IJavaProject getAssociatedJavaProject() {
		IJavaProject project = null;
		
		if(parent != null) {
			project = parent.getAssociatedJavaProject();
		}
		
		return project;
	}
	
	@Override
	public String toString() {
		return item.asReadable();
	}
}
