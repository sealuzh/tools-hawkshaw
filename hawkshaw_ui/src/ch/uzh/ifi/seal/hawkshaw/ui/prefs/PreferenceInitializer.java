package ch.uzh.ifi.seal.hawkshaw.ui.prefs;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;

import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class PreferenceInitializer extends AbstractPreferenceInitializer {
	private static final String default_dir = getDefaultFolderPath();
	
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences node = DefaultScope.INSTANCE.getNode(QueryUI.PLUGIN_ID);
		node.put(IPreferenceConstants.MODEL_TYPE, IPreferenceConstants.MODEL_TYPE_MEM);
		node.put(IPreferenceConstants.STORAGE_LOCATION, default_dir);
	}

	private static String getDefaultFolderPath() {
		return System.getProperty("user.home") + System.getProperty("file.separator") + "nl_tdb_store";
	}
}
