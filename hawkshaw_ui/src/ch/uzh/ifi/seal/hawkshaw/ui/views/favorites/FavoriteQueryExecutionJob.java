package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import ch.uzh.ifi.seal.hawkshaw.core.ontology.ModelAdapter;
import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.JavaOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryInterfaceDispatcher;

public class FavoriteQueryExecutionJob extends Job {
	private FavoriteQueryItem item;
	
	public FavoriteQueryExecutionJob(FavoriteQueryItem item) {
		super("Executing Favorite Query");
		this.item = item;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		
		IJavaProject javaProject = convertToJavaProject(item);
		
		JavaOntologyCore.waitForInit(subMonitor.newChild(50)); // loads ontology core, TODO ontologies need to be added as extension points
		PersistentJenaModel model = OntologyCore.fetchPersistentModelFor(javaProject.getProject());
		
		Answer globalAnswer = execute(model,  item.getSparqlQueries(), subMonitor.newChild(50));
		globalAnswer.setQuestionPhrase(item.getQuestionPhrase());
		show(javaProject, globalAnswer);
		
		return Status.OK_STATUS;
	}
	

	private IJavaProject convertToJavaProject(FavoriteQueryItem item) {
		String projectHandle = item.getProjectHandle();
		IJavaElement element = JavaCore.create(projectHandle);
		IJavaProject javaProject = element.getJavaProject();
		
		return javaProject;
	}

	private Answer execute(PersistentJenaModel model, Set<String> queries, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		
		Answer globalAnswer = new Answer();	
		globalAnswer.setCompilationTime(0L);

		long start = System.currentTimeMillis();
		
		subMonitor.setWorkRemaining(queries.size());
		ModelAdapter wrapper = new ModelAdapter(model);
		for(String query : queries) {
			globalAnswer.include(wrapper.execute(query));
			subMonitor.worked(1);
		}
		
		long end = System.currentTimeMillis();
		
		globalAnswer.setQueryExecutionTime(end - start);
		
		return globalAnswer;
	}

	private void show(IJavaProject javaProject, Answer globalAnswer) {
		QueryInterfaceDispatcher.INSTANCE.showResult(globalAnswer, javaProject);
	}
}
