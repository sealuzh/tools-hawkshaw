package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.selections.JavaElementSelection;

public class XProjectChooserPage extends AbstractXQueryPage {
	public static final String PAGE_ID = "ch.uzh.ifi.seal.hawkshaw.ui.wizards.query.XQueryWizard.XProjectChooserPage";
	
	private static final String COLUMN_PROJECT = "project";
	
	private String[] columnNames = new String[] { COLUMN_PROJECT };
	
	private Table table;
	private TableViewer viewer;
	
	public XProjectChooserPage() {
		super(PAGE_ID, "Choose a Project", IHawkshawImages.INSP_WIZ);
		setMessage("The current selection does not have an ontology model associated. Choose a project below and a model will be generated if necessary.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.None);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		
		createTable(composite);
		createTableViewer();
	
		setControl(composite);
	}
	
	@Override
	public void setVisible(boolean visible) {
		
		if(visible) {
			viewer.setInput(new String[] { "Loading..." });
			new ProjectLoader(viewer).start();
			setPageComplete(false);
		}
		
		super.setVisible(visible);
	}
	
	@Override
	public boolean hasNextPage() {
		return true;
	}
	
	private void createTable(Composite parent) {
		setUpTable(parent);
		setUpColumns();
		
		setUpListener();
	}

	private void setUpTable(Composite parent) {
		table = new Table(parent, SWT.SINGLE
									  | SWT.BORDER
									  | SWT.H_SCROLL
									  | SWT.V_SCROLL
									  | SWT.SINGLE
									  | SWT.FULL_SELECTION
									  | SWT.HIDE_SELECTION);
		
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
	}

	private void setUpColumns() {
		TableColumn column = new TableColumn(table, SWT.LEFT, 0);
		column.setText("Java Project");
		column.setWidth(200);
		
		column = new TableColumn(table, SWT.LEFT, 1);
		column.setText("Project Status");
		column.setWidth(250);
		column.setToolTipText("Displays whether the project has a model associated.");
		
		column = new TableColumn(table, SWT.CENTER, 2);
		column.setText("#Triples");
		column.setWidth(45);
		column.setAlignment(SWT.CENTER);
	}

	private void setUpListener() {
		table.addSelectionListener(new ProjectSelectionListener());
	}

	private void createTableViewer() {
		viewer = new TableViewer(table);
		viewer.setUseHashlookup(true);
		
		viewer.setColumnProperties(columnNames);
		
		viewer.setLabelProvider(new JavaProjectLabelProvider());
		viewer.setContentProvider(ArrayContentProvider.getInstance()); // ProjectLoader returns an array of IJavaProjects
	}
	
	private class ProjectSelectionListener extends SelectionAdapter {
		@Override
		public void widgetSelected(SelectionEvent e) {
			TableItem[] selection = table.getSelection();
			Object data = selection[0].getData();
			
			if(data instanceof IJavaProject) { // make sure that dummy string when hitting tdb cannot be selected
				JavaElementSelection sel = new JavaElementSelection((IJavaProject) data);
				XQueryWizard queryWizard = ((XQueryWizard) getWizard());
				queryWizard.setSelection(sel);
				
				setPageComplete(true);
			}
		}
	}
	
	private static final class ProjectLoader extends Thread {
		private TableViewer viewer;
		
		public ProjectLoader(TableViewer viewer) {
			this.viewer = viewer;
		}
		
		@Override
		public void run() {
			final List<IJavaProject> javaProjects = new ArrayList<>();
			
			IWorkspaceRoot wsRoot = ResourcesPlugin.getWorkspace().getRoot();
			IProject[] projects = wsRoot.getProjects();
			
			for(IProject project : projects) {
				try {
					if(project.isOpen() && project.isNatureEnabled(JavaCore.NATURE_ID)) {
						IJavaProject javaProject = JavaCore.create(project);
						javaProjects.add(javaProject);
					}
				} catch(CoreException cex) {
					// Should not happen
					throw new HawkshawException(QueryUI.getDefault(), "Unexpected error while accessing workspace projects.", cex); // TODO own exception
				}
			} 
			
			setViewerInput(javaProjects.toArray());
		}

		private void setViewerInput(final Object[] javaProjects) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					viewer.setInput(javaProjects);
				}
			});
		}
	}
}
