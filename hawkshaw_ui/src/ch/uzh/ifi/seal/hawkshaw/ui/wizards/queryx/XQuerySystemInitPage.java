package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class XQuerySystemInitPage extends AbstractXQueryPage {
	public static final String PAGE_ID = "ch.uzh.ifi.seal.hawkshaw.ui.wizards.query.XQueryWizard.XQuerySystemInitPage";
	
	public XQuerySystemInitPage() {
		super(PAGE_ID, "Initializing Query System", IHawkshawImages.INSP_WIZ);
		setMessage("You are invoking the natural language query feature for the first time in this session. Initialization is needed and will take a couple of seconds.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		setControl(composite);
	}
	
	@Override
	public IWizardPage getPreviousPage() {
		return null;
	}
	
	@Override
	public boolean hasNextPage() {
		return true;
	}
	
	@Override
	public void finish() {
		IRunnableWithProgress initializer = new XQuerySystemInitializer();
		try {
			getContainer().run(true, false, initializer);
		} catch (InvocationTargetException e) {
			throw new HawkshawException(QueryUI.getDefault(), "Unexpected error while waiting for query system initialization.", e);
		} catch (InterruptedException e) {
			// should not happen
			throw new HawkshawException(QueryUI.getDefault(), "Interrupted while waiting for query system initialization.", e);
		}
	}
}