package ch.uzh.ifi.seal.hawkshaw.ui.perspectives;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import ch.uzh.ifi.seal.hawkshaw.ui.views.favorites.FavoriteQueryView;
import ch.uzh.ifi.seal.hawkshaw.ui.views.inspector.InspectorView;
import ch.uzh.ifi.seal.hawkshaw.ui.views.result.QueryResultView;

public class PerspectiveFactory implements IPerspectiveFactory {

	private IPageLayout factory;

	@Override
	public void createInitialLayout(IPageLayout factory) {
		this.factory = factory;
		
		addViews();
		addActionSets();
		addNewWizardShortcuts();
		addPerspectiveShortcuts();
		addViewShortcuts();		
	}

	private void addViews() {
		// Top left: JDT Package Explorer
		IFolderLayout topLeft = factory.createFolder("topLeft", IPageLayout.LEFT, 0.25f, factory.getEditorArea());
		topLeft.addView(JavaUI.ID_PACKAGES);
	
		// Bottom: Query Results
		IFolderLayout bottom = factory.createFolder("bottom", IPageLayout.BOTTOM, 0.75f, factory.getEditorArea());
		bottom.addView(QueryResultView.VIEW_ID);
		bottom.addView(FavoriteQueryView.VIEW_ID);
		
		// Top right: Outline View
		IFolderLayout topRight = factory.createFolder("topRight", IPageLayout.RIGHT, 0.75f, factory.getEditorArea());
		topRight.addView(IPageLayout.ID_OUTLINE);
		topRight.addView(InspectorView.VIEW_ID);
	}

	private void addActionSets() {
		// TODO Auto-generated method stub
		
	}

	private void addNewWizardShortcuts() {
		// TODO Auto-generated method stub
	}

	private void addPerspectiveShortcuts() {
		// TODO Auto-generated method stub
		
	}

	private void addViewShortcuts() {
		// TODO Auto-generated method stub
	}

}
