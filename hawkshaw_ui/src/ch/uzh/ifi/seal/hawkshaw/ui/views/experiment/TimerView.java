package ch.uzh.ifi.seal.hawkshaw.ui.views.experiment;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.ui.part.ViewPart;

import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class TimerView extends ViewPart {
	private List<Task> tasks;
	
	private Group progressGroup;
	private ProgressBar currentProgressBar;
	private Label statusLabel;

	private Label timeProgressLabel;
	private ProgressBar totalProgressBar;
	
	private Task activeTask;
	private int currentTaskIndex;
	
	private Button startStopButton;

	private Button resetButton;

	private Button nextButton;

	private IState state;
	
	public TimerView() {
		currentTaskIndex = 0;
	}

	@Override
	public void createPartControl(Composite parent) {
		GridLayout layout = new GridLayout(2, false);
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;
		parent.setLayout(layout);

		Group buttonGroup = new Group(parent, SWT.NONE);
		buttonGroup.setText("Experiment Controls:");
		layout = new GridLayout(4, false);
		buttonGroup.setLayout(layout);
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = false;
		gridData.grabExcessVerticalSpace = false;
		buttonGroup.setLayoutData(gridData);
		
		startStopButton = new Button(buttonGroup, SWT.NONE);
		startStopButton.setText("Start Experiment");
		startStopButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.PLAY));
		startStopButton.addListener(SWT.Selection, new StartStopAction());
		
		resetButton = new Button(buttonGroup, SWT.NONE);
		resetButton.setText("Reset");
		resetButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.RESET));
		resetButton.addListener(SWT.Selection, new ResetAction());
		resetButton.setEnabled(false);
		
		nextButton = new Button(buttonGroup, SWT.NONE);
		nextButton.setText("Complete active task");
		nextButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.NEXT));
		nextButton.addListener(SWT.Selection, new NextAction());
		nextButton.setEnabled(false);
		
		gridData = new GridData();
		gridData.horizontalIndent = 15;
		nextButton.setLayoutData(gridData);
		
		statusLabel = new Label(parent, SWT.NONE);
		statusLabel.setText("Press 'Start Experiment' to begin");
		
		gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.horizontalIndent = 50;
		statusLabel.setLayoutData(gridData);
		
		
		progressGroup = new Group(parent, SWT.NONE);
		progressGroup.setText("Progress: ");
		layout = new GridLayout(3, false);
		layout.verticalSpacing = 10;
		layout.marginWidth = 10;
		layout.marginHeight = 20;
		progressGroup.setLayout(layout);
		
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.verticalIndent = 10;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		progressGroup.setLayoutData(gridData);
		
		Label currentTaskProgressLabel = new Label(progressGroup, SWT.NONE);
		currentTaskProgressLabel.setText("Current:");
		
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		currentTaskProgressLabel.setLayoutData(gridData);
		
		currentProgressBar = new ProgressBar(progressGroup, SWT.NONE);
		
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		currentProgressBar.setLayoutData(gridData);
		
		timeProgressLabel = new Label(progressGroup, SWT.NONE);
		timeProgressLabel.setText("(00 min 00 sec left)");
		
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		timeProgressLabel.setLayoutData(gridData);
		
		Label totalProgressLabel = new Label(progressGroup, SWT.NONE);
		totalProgressLabel.setText("Total:");
		
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		totalProgressLabel.setLayoutData(gridData);
		
		totalProgressBar = new ProgressBar(progressGroup, SWT.NONE);
		
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		totalProgressBar.setLayoutData(gridData);
		
		state = new InitState();
		state.init();
	}
	
	@Override
	public void setFocus() {
		startStopButton.setFocus();
	}
	
	private void startNextTask() {
		if(currentTaskIndex < tasks.size()) {
			activeTask = tasks.get(currentTaskIndex++);
			
			setUpProgressBars(activeTask);
			
			new Thread(activeTask).start();
		}
	}
	
	private void setUpProgressBars(Task nextTask) {
		progressGroup.setText(nextTask.getLabel() + ": " + nextTask.getTitle());
		currentProgressBar.setMaximum(nextTask.getTotalTaskTimeInMillis());
	}
	
	private void setTotalWork(int work) {
		totalProgressBar.setMaximum(work);
	}
	
	private void worked(int workDone, int workRemaining) {
		workedTask(workDone, workRemaining);
		workedTotal(workDone);
	}
	
	private void workedTask(int workDone, int workRemaining) {
		currentProgressBar.setSelection(currentProgressBar.getSelection() + workDone);
		
		timeProgressLabel.setText('(' + toTimeString(workRemaining) + " left)");
	}
	
	private void workedTotal(int work) {
		totalProgressBar.setSelection(totalProgressBar.getSelection() + work);
	}
	
	private void resetTotalWorked() {
		totalProgressBar.setSelection(0);
	}
	
	private void resetTaskWorked() {
		currentProgressBar.setSelection(0);
	}
	
	private void resetCurrentTaskIndex() {
		currentTaskIndex = 0;
	}

	private int getTotalTaskDuration() {
		int total = 0;
		
		for(Task task : tasks) {
			total += task.getTotalTaskTimeInMillis();
		}
		
		return total;
	}
	
	private void collectTasks() {
		tasks = new ArrayList<>();
		
		IConfigurationElement[] config = Platform.getExtensionRegistry()
				 .getConfigurationElementsFor(QueryUI.EXTENSION_ID_EXPERIMENT);
	
		for(IConfigurationElement configElement : config) {
			String label  = configElement.getAttribute("label");
			String title  = configElement.getAttribute("title");
			int timeInMin = Integer.parseInt(configElement.getAttribute("time"));
			
			tasks.add(new Task(label, title, timeInMin));
		}
	}

	private static String toTimeString(int duration) {
		return String.format("%d min, %d sec", 
				TimeUnit.MILLISECONDS.toMinutes(duration),
				TimeUnit.MILLISECONDS.toSeconds(duration) - 
				TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
				);
	}

	private class StartStopAction implements Listener {		
		@Override
		public void handleEvent(Event event) {
			state.startStopClicked();
		}
	}
	
	private class ResetAction implements Listener {
		@Override
		public void handleEvent(Event event) {
			state.resetClicked();
		}
	}
	
	private class NextAction implements Listener {
		@Override
		public void handleEvent(Event event) {
			state.nextClicked();
		}
	}
	
	private class Task implements Runnable {
		private static final int SLEEP = 1000;
		
		private String label;
		private String title;
		private int totalTaskTimeInMillis;
		
		private int remainingTimeInMillis;
		
		private boolean dead;
		private boolean paused;
		
		public Task(String label, String title, int timeInMin){
			this.label = label;
			this.title = title;
			totalTaskTimeInMillis = timeInMin * 60 * 1000;
		}

		public String getLabel() {
			return label;
		}

		public String getTitle() {
			return title;
		}

		public int getTotalTaskTimeInMillis() {
			return totalTaskTimeInMillis;
		}

		public int getElapsed() {
			return totalTaskTimeInMillis - remainingTimeInMillis;
		}

		@Override
		public void run() {
			dead = false;
			paused  = false;
			remainingTimeInMillis = totalTaskTimeInMillis;
			
			while(true) {
				if(dead) {
					finish();
					break;
				}
				
				if(remainingTimeInMillis <= 0) {
					expire();
					break;
				}
				
				sleep();
				
				if(!paused) {
					internalWorked();
					
					remainingTimeInMillis -= SLEEP;
					
					if(remainingTimeInMillis < 5000) {
						Toolkit.getDefaultToolkit().beep();
					}
				}
			}
		}
		
		public void kill() {
			dead = true;
		}
		
		public void pause() {
			paused = true;
		}
		
		public void resume() {
			paused = false;
		}
		
		private void sleep() {
			try {
				Thread.sleep(SLEEP);
			} catch (InterruptedException e) {
				return;
			}
		}
		
		private void expire() {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					state.taskComplete();
					MessageDialog.openWarning(getSite().getShell(),"Time for " + label +" is up!", "Please stop working on the current task, quickly write down any answers you already have found, and then proceed to the next task! Do not forget to write down the amount of time you have used for the current task, even if you were not able to complete it.");
				}
			});
		}
		
		private void finish() {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					state.taskComplete();
				}
			});
		}
		
		private void internalWorked() {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					worked(1000, remainingTimeInMillis);
				}
			});
		}
	}
	
	private class InitState extends AbstractStateAdapter {
		@Override
		public void init() {
			collectTasks();
			setTotalWork(getTotalTaskDuration());
			resume();
		}
		
		@Override
		public void resume() {
			resetTaskWorked();
			resetTotalWorked();
			
			progressGroup.setText("Progress");
			timeProgressLabel.setText("(00 min 00 sec left)");
			
			startStopButton.setText("Start Experiment");
			startStopButton.setEnabled(true);
			resetButton.setEnabled(false);
			nextButton.setEnabled(false);
			
			statusLabel.setText("Press 'Start Experiment' to begin");
			
			resetCurrentTaskIndex();
		}
		
		
		@Override
		public void startStopClicked() {
			state = new RunningState();
			state.init();
		}
	}
	
	private class RunningState extends AbstractStateAdapter {
		
		@Override
		public void init() {
			resetTaskWorked();
			
			startNextTask();
			resume();
		}
		
		@Override
		public void resume() {
			activeTask.resume();
			
			startStopButton.setText("Pause");
			startStopButton.setEnabled(true);
			startStopButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.PAUSE));
			resetButton.setEnabled(false);
			nextButton.setText("Complete active task");
			nextButton.setEnabled(true);
			statusLabel.setText("Experiment in progress");
		}
		
		@Override
		public void startStopClicked() {
			state = new PausedState();
			state.init();
		}
		
		@Override
		public void nextClicked() {
			state = new TaskCompleteState();
			state.init();
		}

		@Override
		public void taskComplete() {
			state = new TaskCompleteState();
			state.init();
		}
	}
	
	private class PausedState extends AbstractStateAdapter {
		@Override
		public void init() {
			startStopButton.setText("Resume");
			startStopButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.PLAY));
			resetButton.setEnabled(true);
			nextButton.setEnabled(false);
			statusLabel.setText("Experiment paused. Please resume asap.");
			activeTask.pause();
		}
		
		@Override
		public void resume() {
			throw new RuntimeException("cannot resume paused state");
		}
		
		@Override
		public void startStopClicked() {
			state = new RunningState();
			state.resume(); // was previously the running state
		}

		@Override
		public void resetClicked() {
			activeTask.kill();
			state = new InitState();
			state.resume();
		}
	}
	
	private class TaskCompleteState extends AbstractStateAdapter {
		@Override
		public void init() {
			activeTask.kill();
			
			startStopButton.setEnabled(false);
			if(currentTaskIndex >= tasks.size()) {
				resetButton.setEnabled(true);
				nextButton.setEnabled(false);
				statusLabel.setText("No tasks left. Write down: " + activeTask.getLabel() + " -> " + toTimeString(activeTask.getElapsed()));
			} else {
				resetButton.setEnabled(false);
				nextButton.setEnabled(true);
				nextButton.setText("Proceed to next task");
				statusLabel.setText("Write down: " + activeTask.getLabel() + " -> " + toTimeString(activeTask.getElapsed()));
			}
			
		}
		
		@Override
		public void resume() {
			throw new RuntimeException("cannot resume task complete state");
		}
		
		@Override
		public void resetClicked() {
			state = new InitState();
			state.resume();
		}

		@Override
		public void nextClicked() {
			state = new RunningState();
			state.init();
		}
	}
	
	private abstract class AbstractStateAdapter implements IState {
		@Override
		public void init() {
			// default : do nothing
		}
		
		@Override
		public void resume() {
			// default : do nothing
		}
		
		@Override
		public void startStopClicked() {
			// default : do nothing
		}

		@Override
		public void resetClicked() {
			// default : do nothing
		}

		@Override
		public void nextClicked() {
			// default : do nothing
		}

		@Override
		public void taskComplete() {
			// default : do nothing
		}
	}

	private interface IState {
		void init();
		void resume();
		void startStopClicked();
		void resetClicked();
		void nextClicked();
		
		void taskComplete();
	}
}
