package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;

public class XQuerySystemInitializer implements IRunnableWithProgress {
	private static final int SLEEP = 200;
	
	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		monitor.beginTask("Waiting for query system to initialize...", IProgressMonitor.UNKNOWN);
		
		OInitializer oinit = new OInitializer();
		if(!OntologyCore.ready()) {
			oinit.start();
		}
		
		QInitializer qinit = new QInitializer();
		if(!OntologyCore.ready()) {
			qinit.start();
		}
		
		while(oinit.isAlive() || qinit.isAlive()) {
			monitor.worked(1);
			
			String message = (oinit.isAlive() ? "Waiting for Ontology Core." : "Ontology Core is already available.") +
							 ' ' +
							 (qinit.isAlive() ? "Waiting for Query Core." : "Query Core is already available.");
			monitor.setTaskName(message);
			
			Thread.sleep(SLEEP);
		}
		
		monitor.done();
	}
	
	
	private static final class OInitializer extends Thread {
		@Override
		public void run() {
			OntologyCore.waitForInit(null);
		}
	}
	
	private static final class QInitializer extends Thread {
		@Override
		public void run() {
			QueryCore.waitForInit(null);
		}
	}
}