package ch.uzh.ifi.seal.hawkshaw.ui.wizards.issues;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;

public class IssueImporterPage extends WizardPage {
	public static final String PAGE_ID = "ch.uzh.ifi.seal.hawkshaw.ui.wizards.issues.IssueImporterWizard.ImporterPage";
	private static final String DEFAULT_TRACKING_SYSTEM = "Atlassian Jira";
	
	private ComboViewer trackerComboViewer;
	private Text urlText;

	private Text keyText;

	private Spinner errorSpinner;

	private Spinner issueNumberSpinner;

	private Spinner issueSleepSpinner;
	
	private WizardValidator validator;
	
	protected IssueImporterPage() {
		super(PAGE_ID, "Import Knowledge from an Issue Tracker", IHawkshawImages.ISSUE_WIZ);
		setMessage("Hawkshaw can download information about bugs from various issue trackers. " +
				   "You can then query this data in natural language.");
		validator = new WizardValidator();
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = createComposite(parent);
		
		addTrackerTypeGroupTo(composite);
		addConnectionGroupTo(composite);
		addConfigGroupTo(composite);
		addIdleGroupTo(composite);
		
		setControl(composite);
	}

	public String getTrackerUrl() {
		return urlText.getText();
	}
	
	public String getIssueKey() {
		return keyText.getText();
	}
	
	public int getMaxErrors() {
		return errorSpinner.getSelection();
	}
	
	public int getSleepAfter() {
		return issueNumberSpinner.getSelection();
	}
	
	public long getSleepMillis() {
		return issueSleepSpinner.getSelection();
	}
	
	private Composite createComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(4, false);
		composite.setLayout(layout);
		return composite;
	}

	private void addTrackerTypeGroupTo(Composite composite) {
		Group trackerGroup = new Group(composite, SWT.NONE);
		trackerGroup.setText("Issue Tracking System:");
		trackerGroup.setLayout(new GridLayout());
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		trackerGroup.setLayoutData(gridData);
		
		
		addTrackerComboViewerTo(trackerGroup);
	}

	private void addTrackerComboViewerTo(Group trackerGroup) {
		Combo combo = new Combo(trackerGroup, SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.FILL_BOTH));
		combo.addListener(SWT.Selection, validator);
		
		trackerComboViewer = new ComboViewer(combo);
		trackerComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		trackerComboViewer.setLabelProvider(new DummyLabelPovider());
		trackerComboViewer.setInput(new String[] { DEFAULT_TRACKING_SYSTEM });
		trackerComboViewer.setSelection(new StructuredSelection(DEFAULT_TRACKING_SYSTEM));
	}

	private void addConnectionGroupTo(Composite composite) {
		Group connectionGroup = new Group(composite, SWT.NONE);
		connectionGroup.setText("Credentials:");
		connectionGroup.setLayout(new GridLayout(3, false));
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		connectionGroup.setLayoutData(gridData);
		
		addUrlLabelTo(connectionGroup);
		addKeyLabelTo(connectionGroup);
		addUrlTextFieldTo(connectionGroup);
		addKeyTextFieldTo(connectionGroup);
	}

	private void addKeyTextFieldTo(Group connectionGroup) {
		keyText = new Text(connectionGroup, SWT.SINGLE | SWT.BORDER);
		keyText.setText("IVY-");
		keyText.addListener(SWT.Modify, validator);
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		keyText.setLayoutData(gridData);
	}

	private void addUrlTextFieldTo(Group connectionGroup) {
		urlText = new Text(connectionGroup, SWT.SINGLE | SWT.BORDER);
		urlText.setText("https://issues.apache.org/jira");
		urlText.addListener(SWT.Modify, validator);
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		urlText.setLayoutData(gridData);
	}

	private void addKeyLabelTo(Group connectionGroup) {
		Label keyLabel = new Label(connectionGroup, SWT.NONE);
		keyLabel.setText("Issue Key:");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		keyLabel.setLayoutData(gridData);
	}

	private void addUrlLabelTo(Group connectionGroup) {
		Label urlLabel = new Label(connectionGroup, SWT.NONE);
		urlLabel.setText("Tracker URL:");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 20;
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		urlLabel.setLayoutData(gridData);
	}

	private void addConfigGroupTo(Composite composite) {
		Group configGroup = new Group(composite, SWT.NONE);
		configGroup.setText("Imported Range of Issues:");
		configGroup.setLayout(new GridLayout(3, false));
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		configGroup.setLayoutData(gridData);
		
		addMaxErrorLabelTo(configGroup);
		addErrorSpinnerTo(configGroup);
		addErrorsLabelTo(configGroup);
		}

	private void addErrorsLabelTo(Group configGroup) {
		Label errorLabel = new Label(configGroup, SWT.NONE);
		errorLabel.setText("errors");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
//		gridData.horizontalAlignment = SWT.LEFT;
		errorLabel.setLayoutData(gridData);
	}

	private void addErrorSpinnerTo(Group configGroup) {
		errorSpinner = new Spinner (configGroup, SWT.BORDER);
		errorSpinner.setMinimum(0);
		errorSpinner.setMaximum(1000);
		errorSpinner.setSelection(50);
		errorSpinner.setIncrement(5);
		errorSpinner.setPageIncrement(50);
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		errorSpinner.setLayoutData(gridData);
	}

	private void addMaxErrorLabelTo(Group configGroup) {
		Label maxErrorLabel = new Label(configGroup, SWT.NONE);
		maxErrorLabel.setText("Stop import after ");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		maxErrorLabel.setLayoutData(gridData);
	}

	private void addIdleGroupTo(Composite composite) {
		final Group idleGroup = new Group(composite, SWT.NONE);
		idleGroup.setText("IP Blacklisting Prevention:");
		idleGroup.setLayout(new GridLayout(5, false));
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		idleGroup.setLayoutData(gridData);
		
		addWait1LabelTo(idleGroup);
		addIssueNumberSpinnerTo(idleGroup);
		addWaitLabel2To(idleGroup);
		addIssueSleepSpinnerTo(idleGroup);
		addWaitLabel3To(idleGroup);
		addWarningTextLabelTo(idleGroup);
	}

	private void addWaitLabel3To(final Group idleGroup) {
		Label waitLabel3 = new Label(idleGroup, SWT.NONE);
		waitLabel3.setText(" ms");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		waitLabel3.setLayoutData(gridData);
	}

	private void addIssueSleepSpinnerTo(final Group idleGroup) {
		issueSleepSpinner = new Spinner(idleGroup, SWT.BORDER);
		issueSleepSpinner.setMinimum(0);
		issueSleepSpinner.setMaximum(60000);
		issueSleepSpinner.setSelection(5000);
		issueSleepSpinner.setIncrement(100);
		issueSleepSpinner.setPageIncrement(5000);
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		issueSleepSpinner.setLayoutData(gridData);
	}

	private void addWaitLabel2To(final Group idleGroup) {
		Label waitLabel2 = new Label(idleGroup, SWT.NONE);
		waitLabel2.setText(" issues for ");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		waitLabel2.setLayoutData(gridData);
	}

	private void addIssueNumberSpinnerTo(final Group idleGroup) {
		issueNumberSpinner = new Spinner(idleGroup, SWT.BORDER);
		issueNumberSpinner.setMinimum(0);
		issueNumberSpinner.setMaximum(1000);
		issueNumberSpinner.setSelection(25);
		issueNumberSpinner.setIncrement(5);
		issueNumberSpinner.setPageIncrement(10);
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		issueNumberSpinner.setLayoutData(gridData);
	}

	private void addWait1LabelTo(final Group idleGroup) {
		Label waitLabel1 = new Label(idleGroup, SWT.NONE);
		waitLabel1.setText("Wait every ");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		waitLabel1.setLayoutData(gridData);
	}

	private void addWarningTextLabelTo(final Group idleGroup) {
		Text warningTextLabel = new Text(idleGroup, SWT.MULTI | SWT.WRAP); // Labels don't wrap text correctly
		warningTextLabel.setText("Some issue trackers will automatically block your IP when they " +
								 "receive too many requests in a short amount of time. Suspending " +
								 "the import for a few seconds once in a while can trick them.");
		warningTextLabel.setEditable(false);
		warningTextLabel.setEnabled(false);
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 5;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		warningTextLabel.setLayoutData(gridData);
	}

	private class WizardValidator implements Listener {
		@Override
		public void handleEvent(Event event) {
			ISelection selection = trackerComboViewer.getSelection();
			boolean trackerSelected = !selection.isEmpty();
			
			String urlString = urlText.getText();
			boolean urlValid = urlString != null && urlString.startsWith("http"); // TODO proper validation
			
			String keyString = keyText.getText();
			boolean keyValid = keyString != null && keyString.length() > 0;
			
			boolean wizardValidates = trackerSelected && urlValid && keyValid;
			
			setPageComplete(wizardValidates);
		}
	}
	
	private static final class DummyLabelPovider extends LabelProvider {
		@Override
		public String getText(Object element) {
			return element.toString();
		}
	}
}
