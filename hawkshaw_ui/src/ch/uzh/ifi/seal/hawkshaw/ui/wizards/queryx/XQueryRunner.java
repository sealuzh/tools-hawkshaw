package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.operation.IRunnableWithProgress;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.core.exceptions.QueryExecutionException;
import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.core.query.Question;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryInterfaceDispatcher;

public class XQueryRunner implements IRunnableWithProgress {
	private Question question;
	private Answer answer;
	
	private IJavaProject javaProject;
	
	private IStatus status;
	
	XQueryRunner(Question question, IJavaProject javaProject) {
		this.question    = question;
		this.javaProject = javaProject;
	}

	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		SubMonitor progress = SubMonitor.convert(monitor, "Compiling and executing formal query. This may take a couple of seconds.", 100);
		
		QueryExecutionThread queryExecutor = new QueryExecutionThread();
		queryExecutor.start();
		
		while(queryExecutor.isAlive()) {
			progress.setWorkRemaining(100);
			progress.worked(1);
			Thread.sleep(100);
		}
		
		if(status.getSeverity() == IStatus.OK) {
			QueryInterfaceDispatcher.INSTANCE.showResult(answer, javaProject);
		} else {
			QueryInterfaceDispatcher.INSTANCE.showError(status);
		}
	}
	
	public IStatus getStatus() {
		return status;
	}
	
	private class QueryExecutionThread extends Thread {
		@Override
		public void run() {
			try {
				answer = question.ask();
				status = Status.OK_STATUS;
			} catch(QueryExecutionException qeex) {
				status = new Status(IStatus.ERROR, QueryCore.PLUGIN_ID, "Could not answer '" + question.toString() + "'. Please report a bug and provide the question you have entered, as well as the following query string:\n\n" + qeex.getQueryStatementString() + "\n\n", qeex);
			}
		}
	}
}
