package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Iterator;
import java.util.Set;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;

public class InspectorComparator extends ViewerComparator {	
	private static final int DESCENDING = 1;
	private int direction = DESCENDING;
	
	private String oldPropUri;
	public InspectorComparator() {
		direction = DESCENDING;
	}
	
	public int getDirection() {
		return direction == 1 ? SWT.DOWN : SWT.UP;
	}
	
	public void setColumn(TableColumn tableColumn) {
		String newPropUri = (String) tableColumn.getData("propUri");
		
		if(oldPropUri != null && oldPropUri.equals(newPropUri)){
			direction = 1 - direction;
		} else {
			oldPropUri = newPropUri;
			direction = DESCENDING;
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		QrItemWrapper b1 = (QrItemWrapper) e1;
		QrItemWrapper b2 = (QrItemWrapper) e2;
		
		int rc = 0;
		
		if(oldPropUri != null) {
			Set<Object> s1 = b1.getPropertyValues(oldPropUri);
			Set<Object> s2 = b2.getPropertyValues(oldPropUri);
			
			Iterator<Object> i1 = s1.iterator();
			Iterator<Object> i2 = s2.iterator();
			
			if(i1.hasNext() && i2.hasNext()) {
				Object o1 = i1.next();
				Object o2 = i2.next();
				
				if(o1 instanceof Comparable && o2 instanceof Comparable) {
					rc = ((Comparable)o1).compareTo((Comparable)o2);
				}
			}
		}
		
		if (direction == DESCENDING) {
			rc = -rc;
		}
		
		return rc;
	}
}
