package ch.uzh.ifi.seal.hawkshaw.ui.messages;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;

public enum HawkshawNotificationCenter {
	INSTANCE;
	
	private Growl growl;
	
	private HawkshawNotificationCenter() {
		if(Platform.getOS().equals(Platform.OS_MACOSX)) {
			growl = new Growl("Hawkshaw", new String[] { "System" }, new String[] { "System" });
			growl.init();
			growl.registerApplication();
		}
	}
	
	public void notifyUser(String title, String text, Image image) {
		if(growl != null && growl.isGrowlEnabled()) {
			showInGrowl(title, text);
		} else {
			showInTray(title, text, image);
		}
	}
	
	public void showInTray(String title, String text, Image image) {
		showInTray(title, text, image, SWT.ICON_INFORMATION);
	}

	public void showInTray(final String title, final String message, final Image image, final int type) {
		final Display display = Display.getDefault();
		display.asyncExec(new ShowInTrayRunnable(display, title, message, image, type));
	}
	
	public void showInGrowl(String message, String text) {
		if(growl != null) {
			growl.notify("System", message, text);
		}
	}

	private static final class ShowInTrayRunnable implements Runnable {
		private Display display;
		
		private String title;
		private String message;
		private Image image;
		
		private int type;
		
		public ShowInTrayRunnable(Display display, String title, String message, Image image, int type) {
			this.display = display;
			this.title = title;
			this.message = message;
			this.image = image;
			this.type = type;
		}
		
		@Override
		public void run() {
			Tray tray = display.getSystemTray();
			if (tray != null) {
				Shell shell = new Shell(display);
				ToolTip tip = new ToolTip(shell, SWT.BALLOON | type);
				tip.setMessage(message);
				tip.setText(title);
				
				TrayItem item = new TrayItem(tray, SWT.NONE);
				item.setImage(image);
				item.setToolTip(tip);
				
				tip.setVisible(true);
			}
		}
	}
}
