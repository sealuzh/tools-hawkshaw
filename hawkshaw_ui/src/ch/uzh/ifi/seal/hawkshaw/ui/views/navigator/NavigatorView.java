package ch.uzh.ifi.seal.hawkshaw.ui.views.navigator;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.awt.Frame;

import javax.swing.JApplet;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

// TODO own plug-in?
public class NavigatorView extends ViewPart {
	public static final String ID = "ch.uzh.ifi.seal.hawkshaw.ui.navigatorview";

	public NavigatorView() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(Composite parent) {
//		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		Frame frame = SWT_AWT.new_Frame(new Composite(parent, SWT.EMBEDDED));
		JApplet applet = new JApplet();
	    frame.add(applet);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
