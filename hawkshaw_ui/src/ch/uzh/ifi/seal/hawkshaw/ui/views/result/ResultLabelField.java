package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.graphics.Image;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.ImageMap;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.util.AbstractField;

public class ResultLabelField extends AbstractField {

	@Override
	public String getDescription() {
		return "A human-readable representation of the answer.";
	}
	
	@Override
	public String getColumnHeaderText() {
		return "Answer";
	}
	
	@Override
	public Image getColumnHeaderImage() {
		Image image = null;
		
		if(!Platform.getOS().equals(Platform.OS_MACOSX)) { // column images look weird on mac osx
			image = QueryUI.getDefault().getImage(IHawkshawImages.REPORT);
		}
		
		return image;
	}
	
	@Override
	public Image getImage(Object object) {
		Image image = null;
		
		if(object instanceof QrTreeItem) {
			QrTreeItem node = (QrTreeItem)object;
			String uri = node.getUri();
			
			if(uri != null) {
				IProject project = node.getAssociatedJavaProject().getProject();
				OntologyCore.fetchPersistentModelFor(project);
				
				image = ImageMap.getImage(OntologyCore.fetchPersistentModelFor(project), uri);
			}
		}
		
		return image;
	}
	
	@Override
	public String getToolTipText(Object object) {
		String result = null;
		
		if(object instanceof QrTreeItem) {
			QrTreeItem node = (QrTreeItem)object;
			String uri = node.getUri();
			
			if(uri != null) {
				int pos = uri.lastIndexOf('#');
				if(pos > -1) {
					result = UriUtil.decode(uri.substring(pos + 1));
				} else {
					result = uri;
				}
			}
		} else if(object instanceof AnswerTreeItem) {
			AnswerTreeItem item = (AnswerTreeItem) object;
			Answer answer = item.getItem();
			int size = answer.size();
			
			result = answer.getQuestionPhrase() + " (" + answer.size() + (size == 1 ? " result)" : " results)");
		}
		
		return result;
	}

	@Override
	public Image getToolTipImage(Object object) {
		Image image = getImage(object);
		
		if(image == null && object instanceof AnswerTreeItem) {
			image = QueryUI.getDefault().getImage(IHawkshawImages.ANSWER);
		}
		
		return image;
	}

	@Override
	public int getPreferredWidth() {
		return 300;
	}
}
