package ch.uzh.ifi.seal.hawkshaw.ui.util;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.swt.graphics.Image;

public abstract class AbstractField implements IField {
	private boolean visible;
	
	public AbstractField() {
		visible = true;
	}

	public boolean isShowing() {
		return visible;
	}

	public void setShowing(boolean showing) {
		visible = showing;
	}
	
	@Override
	public String getValue(Object object) {
		return object.toString();
	}
	
	@Override
	public String getToolTipText(Object element) {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public Image getDescriptionImage() {
		return null;
	}

	@Override
	public Image getColumnHeaderImage() {
		return null;
	}

	@Override
	public Image getImage(Object object) {
		return null;
	}

	@Override
	public Image getToolTipImage(Object object) {
		return null;
	}

	@Override
	public int compare(Object object1, Object object2) {
		return 0;
	}

	@Override
	public int getDefaultDirection() {
		return 0;
	}
}
