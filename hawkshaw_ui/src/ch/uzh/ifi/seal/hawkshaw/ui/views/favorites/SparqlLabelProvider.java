package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jface.viewers.OwnerDrawLabelProvider;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Event;

public class SparqlLabelProvider extends OwnerDrawLabelProvider {
	private static final int TEXT_MARGIN_TOP = 5;
	private static final int TEXT_MARGIN_BOTTOM = 10;
	private static final int TEXT_MARGIN_LEFT = 5;
	
	@Override
	protected void measure(Event event, Object element) {
		SparqlQueryItem item = (SparqlQueryItem) element;
		String text = item.format();
		
		Point size = event.gc.textExtent(text);
		event.width = size.x + 2 * TEXT_MARGIN_LEFT;
		event.height = Math.max(event.height, size.y + TEXT_MARGIN_TOP + TEXT_MARGIN_BOTTOM);
	}

	@Override
	protected void paint(Event event, Object element) {
		SparqlQueryItem item = (SparqlQueryItem) element;
		String text = item.format();
	
		event.gc.drawText(text, event.x + TEXT_MARGIN_LEFT, event.y + TEXT_MARGIN_TOP, true);
	}
}
