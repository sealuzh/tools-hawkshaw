package ch.uzh.ifi.seal.hawkshaw.ui;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary.IHawkshawIssueOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.owl.vocabulary.IHawkshawHistory;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IHawkshawOntology;

public final class ImageMap {
	private ImageMap() {
		super();
	}
	
	public static Image getImage(PersistentJenaModel model, String uri) {
		Image result = null;
		if(model.hasOntClass(uri, IHawkshawHistory.FILE_UNDER_VERSION_CONTROL)) {
			result = QueryUI.getDefault().getImage(IHawkshawImages.MULTI_FILE);
		} else if (model.hasOntClass(uri, IHawkshawOntology.FILE) || model.hasOntClass(uri, IHawkshawHistory.VERSION)) {
			result = PlatformUI.getWorkbench().getSharedImages().getImage(org.eclipse.ui.ISharedImages.IMG_OBJ_FILE);
			
		} else if(model.hasOntClass(uri, IHawkshawJavaOntology.PACKAGE)) {
			result = JavaUI.getSharedImages().getImage(org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_PACKAGE);
		} else if(model.hasOntClass(uri, IHawkshawJavaOntology.CLASS)) {
			result = JavaUI.getSharedImages().getImage(org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_CLASS);
		} else if(model.hasOntClass(uri, IHawkshawJavaOntology.INTERFACE)) {
			result = JavaUI.getSharedImages().getImage(org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_INTERFACE);
		} else if(model.hasOntClass(uri, IHawkshawJavaOntology.FIELD)) {
			result = JavaUI.getSharedImages().getImage(org.eclipse.jdt.ui.ISharedImages.IMG_FIELD_PRIVATE); // TODO correct visibility modifiers
		} else if(model.hasOntClass(uri, IHawkshawJavaOntology.METHOD) || model.hasOntClass(uri, IHawkshawJavaOntology.CONSTRUCTOR)) { // TODO 
			result = JavaUI.getSharedImages().getImage(org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_PUBLIC);
			
		} else if(model.hasOntClass(uri, IHawkshawIssueOntology.ISSUE)) {
			result = QueryUI.getDefault().getImage(IHawkshawImages.ISSUE);
		} else if(model.hasOntClass(uri, IHawkshawIssueOntology.COMMENT)) {
			result = QueryUI.getDefault().getImage(IHawkshawImages.COMMENT);
		} else if(model.hasOntClass(uri, IHawkshawOntology.DEVELOPER) || model.hasOntClass(uri, IHawkshawHistory.COMMITTER) || model.hasOntClass(uri, IHawkshawIssueOntology.ASSIGNEE)) {
			result = QueryUI.getDefault().getImage(IHawkshawImages.DEVELOPER);
		} else if(model.hasOntClass(uri, IHawkshawIssueOntology.REPORTER)) {
			result = QueryUI.getDefault().getImage(IHawkshawImages.REPORTER);
		} else if(model.hasOntClass(uri, IHawkshawOntology.STAKEHOLDER)) {
			result = QueryUI.getDefault().getImage(IHawkshawImages.STAKEHOLDER);
		}else {
			result = QueryUI.getDefault().getImage(IHawkshawImages.RDF_TINY);
		}
		
		
		return result;
	}
}
