package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.IJavaProject;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.ResultType;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IHawkshawOntology;
import ch.uzh.ifi.seal.hawkshaw.ui.views.shared.IQrItemContainer;

public class AnswerTreeItem implements ITreeItem<Answer>, IQrItemContainer {
	private ResultTreeItem parent;
	private Answer answer;
	
	private Set<QrTreeItem> children;
	
	public AnswerTreeItem(ResultTreeItem parent, Answer answer) {
		this.parent = parent;
		this.answer = answer;
		
		children = new HashSet<>();
		
		buildTree();
	}

	@Override
	public Answer getItem() {
		return answer;
	}


	@Override
	public IJavaProject getAssociatedJavaProject() {
		return parent.getAssociatedJavaProject();
	}

	@Override
	public ResultTreeItem getParent() {
		return parent;
	}

	public boolean hasChildren() {
		return children.size() > 0;
	}
	
	public QrTreeItem[] getChildren() {
		return children.toArray(new QrTreeItem[children.size()]);
	}

	@Override
	public QrItem[] getItems() {
		List<QrItem> uris = new ArrayList<>();
		
		for(QrItem item : answer.getNodes()) {
			uris.add(item);
		}
		
		return uris.toArray(new QrItem[uris.size()]);
	}

	@Override
	public String toString() {
		return answer.getQuestionPhrase();
	}
	
	private void buildTree() {
		Map<String, QrTreeItem> treeItemCache = new Hashtable<>();
		
		PersistentJenaModel model = OntologyCore.fetchPersistentModelFor(getAssociatedJavaProject().getProject());
		
		if(answer.isEnumQuestion()) {
			QrTreeItem defaultItem = cacheTreeItem(new QrItem(model, String.valueOf(answer.size()), ResultType.LITERAL), treeItemCache);
			defaultItem.setParent(this);
			children.add(defaultItem);
		} else {
			for(QrItem node : answer.getNodes()) {
				switch(node.getType()) {
				case INDIVIDUAL:
					String individualUri = node.getUri();
					String parentUri = model.getObject(individualUri, IHawkshawOntology.HAS_PARENT);
					QrTreeItem lastItem = cacheTreeItem(node, treeItemCache);
					
					while(parentUri != null) {
						QrItem tmp = new QrItem(model, model.labelOf(parentUri), ResultType.INDIVIDUAL);
						tmp.setUri(parentUri);
						
						QrTreeItem currentItem = cacheTreeItem(tmp, treeItemCache);
						lastItem.setParent(currentItem);
						currentItem.addChild(lastItem);
						
						parentUri = model.getObject(parentUri, IHawkshawOntology.HAS_PARENT);
						lastItem = currentItem;
					}
					
					lastItem.setParent(this);
					children.add(lastItem);
					
					break;
				case CLASS:
					QrTreeItem item = new QrTreeItem(node);
					item.setParent(this);
					children.add(item);
					break;
				default:
					QrTreeItem defaultItem = cacheTreeItem(node, treeItemCache);
					defaultItem.setParent(this);
					children.add(defaultItem);
					break;
				}
			}
		}
	}

	private static QrTreeItem cacheTreeItem(QrItem node, Map<String, QrTreeItem> treeItemCache) {
		String key = node.getUri() != null ? node.getUri() : String.valueOf(node.asReadable().hashCode());
		
		QrTreeItem result = treeItemCache.get(key);
		
		if(result == null) {
			result = new QrTreeItem(node);
			treeItemCache.put(key, result);
		}
		
		return result;
	}
}
