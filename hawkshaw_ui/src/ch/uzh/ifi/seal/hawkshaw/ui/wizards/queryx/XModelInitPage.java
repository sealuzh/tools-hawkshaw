package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.InvocationTargetException;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.facade.BuildStatus;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.selections.IResourceSelection;

public class XModelInitPage extends AbstractXQueryPage {
	public static final String PAGE_ID = "ch.uzh.ifi.seal.hawkshaw.ui.wizards.query.XQueryWizard.XModelInitPage";
	
	private Label projectNameLabel;

	private Label natureStatusLabel;

	private Label modelStatusLabel;

	private Label reasoningStatusLabel;

	private IJavaProject javaProject;

	private boolean needsNature;
	private boolean needsModel;
	private boolean needsReasoning;

	public XModelInitPage() {
		super(PAGE_ID, "Create a Model", IHawkshawImages.INSP_WIZ);
		setMessage("The currently selected project does not have an up-to-date ontology model associated.");
	}

	public void init(IResourceSelection selection, BuildStatus status) {
		this.javaProject = selection.getJavaProject();
		this.needsNature = status.lacksStatus(BuildStatus.HAS_NATURE);
		this.needsModel  = status.lacksStatus(BuildStatus.HAS_MODEL);
		this.needsReasoning = status.lacksStatus(BuildStatus.IS_CLEAN);
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);
		
		Label projectIconLabel = new Label(composite, SWT.NONE);
		projectIconLabel.setImage(PlatformUI.getWorkbench()
											.getSharedImages()
											.getImage(IDE.SharedImages.IMG_OBJ_PROJECT)
								 );
		projectIconLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		
		projectNameLabel = new Label(composite, SWT.NONE);
		projectNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Label placeholder = new Label(composite, SWT.NONE);
		placeholder.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		natureStatusLabel = new Label(composite, SWT.NONE);
		natureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		
		modelStatusLabel = new Label(composite, SWT.NONE);
		modelStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		
		reasoningStatusLabel = new Label(composite, SWT.NONE);
		reasoningStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		
		setControl(composite);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if(visible) {
			projectNameLabel.setText(javaProject.getElementName() + ":");
			
			if(needsNature) {
				natureStatusLabel.setText("- Nature will be added");
			}
			
			if(needsModel) {
				modelStatusLabel.setText("- Full build will be performed");
			}
			
			if(needsReasoning) {
				reasoningStatusLabel.setText("- Reasoning will be done");
			}
		}
		
		super.setVisible(visible);
	}
	
	@Override
	public boolean hasNextPage() {
		return true;
	}
	
	@Override
	public void finish() {
		IRunnableWithProgress initializer = new XModelInitializer(javaProject, needsNature, needsModel);
		
		try {
			getContainer().run(true, true, initializer);
		} catch (InvocationTargetException e) {
			throw new HawkshawException(QueryUI.getDefault(), "Unexpected error while building ontology model.", e);
		} catch (InterruptedException e) {
			// should not happen
			throw new HawkshawException(QueryUI.getDefault(), "Interrupted while building ontology model.", e);
		}
	}
}
