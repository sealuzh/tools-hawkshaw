package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class ColumnSortOnSelection extends SelectionAdapter {
	private TableViewerColumn column;
	
	public ColumnSortOnSelection(TableViewerColumn column) {
		this.column = column;
	}
	
	@Override
	public void widgetSelected(SelectionEvent e) {
		TableViewer viewer = (TableViewer) column.getViewer();
		Table table = viewer.getTable();
		TableColumn tableColumn = column.getColumn();
		
		InspectorComparator comparator = (InspectorComparator) viewer.getComparator();
		comparator.setColumn(tableColumn);
		table.setSortDirection(comparator.getDirection());
		table.setSortColumn(tableColumn);
		
		viewer.refresh();
	}
}