package ch.uzh.ifi.seal.hawkshaw.ui.actions.export;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class ExportModelAction implements IObjectActionDelegate {

	private IProject project;

	@Override
	public void run(IAction action) {
		if(project != null) {
			FileDialog dialog = new FileDialog(Display.getDefault().getActiveShell(), SWT.SAVE);
			dialog.setFilterNames(new String[] { "OWL Files", "All Files (*.*)" });
			dialog.setFilterExtensions(new String[] { "*.owl", "*.*" });
			dialog.setFilterPath(System.getProperty("user.dir"));
			dialog.setFileName(project.getName() + ".owl");
			
			String exportPath = dialog.open();
			
			IWorkbench wb = PlatformUI.getWorkbench();
			IProgressService ps = wb.getProgressService();
			try {
				ps.busyCursorWhile(new ExportRunnable(project, exportPath));
			} catch (InvocationTargetException e) {
				throw new HawkshawException(QueryUI.getDefault(), "Error while invoking exporter.", e);
			} catch (InterruptedException e) {
				throw new HawkshawException(QueryUI.getDefault(), "Export interrupted.", e);
			}
			
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			for (Iterator<?> it = ((IStructuredSelection) selection).iterator(); it
					.hasNext();) {
				Object element = it.next();
				IProject project = null;
				if (element instanceof IProject) {
					project = (IProject) element;
				} else if (element instanceof IAdaptable) {
					project = (IProject) ((IAdaptable) element)
							.getAdapter(IProject.class);
				}
				if (project != null) {
					this.project = project;
				}
			}
		}
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

	private static final class ExportRunnable implements IRunnableWithProgress {
			private IProject project;
			private String path;
			
			public ExportRunnable(IProject project, String path) {
				this.project = project;
				this.path = path;
			}
			
			@Override
			public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				monitor.beginTask("Exporting OWL Model", IProgressMonitor.UNKNOWN);
				
				OntologyCore.waitForInit(null);
				PersistentJenaModel model = OntologyCore.fetchPersistentModelFor(project);
				
				try {
					model.export(path);
				} catch (FileNotFoundException e) {
					throw new HawkshawException(QueryUI.getDefault(), "File not found: " + path, e);
				}
			}
		}

}
