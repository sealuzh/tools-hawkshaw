package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.TableColumn;

import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IRDFS;
import ch.uzh.ifi.seal.hawkshaw.support.misc.WordUtil;
import ch.uzh.ifi.seal.hawkshaw.ui.ImageMap;

public class InspectorColumnLabelProvider extends ColumnLabelProvider {
	private TableViewerColumn viewerColumn;
	
	public InspectorColumnLabelProvider(TableViewerColumn viewerColumn) {
		this.viewerColumn = viewerColumn;
	}
	
	@Override
	public String getText(Object element) {
		TableColumn tableColumn = viewerColumn.getColumn();
		String propUri = (String) tableColumn.getData("propUri");
		
		return ((QrItemWrapper) element).getPropertyValuesAsString(propUri);
	}

	@Override
	public Image getImage(Object element) {
		Image image = null;
		
		TableColumn tableColumn = viewerColumn.getColumn();
		String propUri = (String) tableColumn.getData("propUri");
		
		if(IRDFS.LABEL.equals(propUri) || "#TYPE".equals(propUri)) {
			QrItem node = ((QrItemWrapper) element).getItem();
			image = ImageMap.getImage(node.getModel(), node.getUri());
		}
		
		return image;
	}

	@Override
	public String getToolTipText(Object element) {
		QrItemWrapper bean = ((QrItemWrapper) element);
		TableColumn tableColumn = viewerColumn.getColumn();
		String propUri = (String) tableColumn.getData("propUri");
		
		return WordUtil.wrap(bean.getPropertyValuesAsString(propUri), 80, "\n", false);
	}
}