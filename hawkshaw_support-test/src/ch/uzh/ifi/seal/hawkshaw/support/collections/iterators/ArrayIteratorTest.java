package ch.uzh.ifi.seal.hawkshaw.support.collections.iterators;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.support.collections.iterators.ArrayIterator;

public class ArrayIteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIterator() {
		String[] strings = {"first", "second", "third"};
		ArrayIterator<String> iter = new ArrayIterator<>(strings);
		assertTrue(iter.hasNext());
		assertEquals(0, iter.nextIndex());
		assertFalse(iter.hasPrevious());
		assertEquals("first", iter.next());
		assertEquals(1, iter.nextIndex());
		assertEquals("second", iter.next());
		assertTrue(iter.hasPrevious());
		assertEquals(0, iter.previousIndex());
		assertEquals(2, iter.nextIndex());
		assertEquals("third", iter.next());
		assertFalse(iter.hasNext());
		assertEquals(1, iter.previousIndex());
		
		assertEquals("second", iter.previous());
		assertEquals("first", iter.previous());
		assertFalse(iter.hasPrevious());
		assertEquals(-1, iter.previousIndex());
	}
}
