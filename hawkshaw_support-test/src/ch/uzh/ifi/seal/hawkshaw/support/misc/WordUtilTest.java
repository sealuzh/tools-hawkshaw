package ch.uzh.ifi.seal.hawkshaw.support.misc;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.*;

import org.junit.Test;

public class WordUtilTest {

	@Test
	public void testASCIIConversion() {
		assertEquals("wursch", WordUtil.convertNonAscii("w�rsch"));
		assertEquals("Lavenee", WordUtil.convertNonAscii("Laven�e"));
		assertEquals("item", WordUtil.convertNonAscii("�tem"));
	}
	
	@Test
	public void testWrap() {
		assertEquals("1\n2\n3", WordUtil.wrap("123", 0, "\n", true));
		assertEquals("1\n2\n3", WordUtil.wrap("123", 0, "\n", true));
		assertEquals("longstring\nthatwraps", WordUtil.wrap("longstringthatwraps", 10, "\n", true));
		assertEquals("longstringthatdoesnotwrap\nshortstring", WordUtil.wrap("longstringthatdoesnotwrap shortstring", 10, "\n", false));
	}
	
	@Test
	public void testCamelCase() {
		assertEquals("camelCaseWord", WordUtil.toCamelCase("camel case wOrd"));
	}
}
