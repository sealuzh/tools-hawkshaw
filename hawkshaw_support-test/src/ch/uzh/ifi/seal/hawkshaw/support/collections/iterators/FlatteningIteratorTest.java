package ch.uzh.ifi.seal.hawkshaw.support.collections.iterators;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.support.collections.iterators.FlatteningIterator;

public class FlatteningIteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = NoSuchElementException.class)
	public void testIterate() {
		/*
		 List topLevel
		 |
		 |-- String "a"
		 |
		 |-- List secondLevel1
		 |	 |
		 |	 |- Integer "10"
		 |
		 |-- List secondLevel2
		 |	 |
		 |	 |- String "hi"
		 |	 |
		 |	 |- boolean "you"
		 */
		
		
		List<Object> topLevel = new ArrayList<>();
		topLevel.add(new String("a"));
		
		List<Integer> secondLevel1 = new ArrayList<>();
		secondLevel1.add(new Integer(10));
		topLevel.add(secondLevel1);
		
		List<String> secondLevel2 = new LinkedList<>();
		secondLevel2.add("hi");
		secondLevel2.add("you");
		topLevel.add(secondLevel2);
		
		FlatteningIterator iter = new FlatteningIterator(topLevel, "b");
		assertEquals("a", iter.next());
		assertEquals(new Integer(10), iter.next());
		assertEquals("hi", iter.next());
		assertEquals("you", iter.next());
		assertEquals("b", iter.next());
		assertFalse(iter.hasNext());
		
		iter.next(); // should throw a NoSuchElementException
	}
}
